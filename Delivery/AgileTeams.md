# Agile Teams

## Table of Contents

 - [Introduction](#markdown-header-introduction)
 - [Scrum Master vs Project Manager](#markdown-header-scrum-master-vs-project-manager)
 - [Tickets and Stories](#markdown-header-tickets-and-stories)
 - [Story Estimation](#markdown-header-story-estimation)
 - [Iteration Planning](#markdown-header-iteration-planning)
 - [Stand-up](#markdown-header-stand-up)
 - [Tech Huddles](#markdown-header-tech-huddles)
 - [Retrospective Process](#markdown-header-retrospective-process)
 - [References and Resources](#markdown-header-references-and-resources)


## Introduction

'Agile' refers to a way of approaching work. It can be conceptually contrasted with different methods of organising and delivering work, such as the 'Waterfall' approach to project management.

Waterfall project management maps out a project into distinct, sequential phases, with each new phase beginning only when the previous one has been completed. The Waterfall system is the most traditional method for managing a project, with team members working linearly towards a set end goal. Each participant has a clearly defined role, and none of the phases or goals are expected to change.

Traditional methods of work estimation and delivery such as Waterfall, where milestones and timeframes are estimated well in advance, are not necessarily suited to an industry where the fast changing nature of the subject matter itself can mean constant re-definition and movement of goal posts.

Agile is very popular within the tech industry because it allows rapid prototyping, iterative design, and continuous deployment. The time estimation is generally not as important as the quality of work and ultimate goals being reached (which can change). It is flexible, and delegates much responsibility to the team itself to design and deliver in small chunks. 

Agile isn't defined by a set of ceremonies or specific development techniques. Rather, agile is a group of methodologies that demonstrate a commitment to tight feedback cycles and continuous improvement. 

The key to doing agile right is embracing a mindset of continuous improvement. Although the project lead or product owner typically prioritises the work to be delivered, the team takes the lead on deciding how the work will get done. Experiment with different practices and have open, honest discussions about them with your team. Keep the ones that work, and throw out the ones that don't.

These are some of the ceremonies or techniques that are typical at ARQ Group. 

However, make sure to confirm your particular work requirements with your tech lead or Project Manager (PM) /scrum master.

## Scrum Master vs Project Manager

A traditional PM may be required to ensure the team delivers pre-defined products at pre-defined times. Agile teams may have a PM, but if the project is managed in a traditional way it can conflict with the Agile methodology - for example, it could result in a 'tick-box' mentality where the project is regarded as having succeeded even if the deliverables have become redundant or changed.

Agile teams may instead have a 'scrum master' (or a PM who functions like a scrum master) whose primarily role is to remove blockers for the team to achieve what they have defined is needed. This may be done in short 'sprints' (often 2 weeks), several of which are bundled together to deliver one chunk of functionality in an 'epic' (made up of however many sprints are needed).

## Tickets and Stories

'Tickets' contain "stories" describing small portions of functionality and design from the user perspective. They are usually created by the Business Analyst or 'BA'. They are intended to be focused and small enough such that they could be implemented by one developer within the sprint. 

They often are drafted using phrases 'As a user', 'when' 'X' happens, the user should expect 'Y'. For example:

*Scenario 1*

- As a user
- I am on the `Manage My Account Screen`
- **When** I click on the `Edit Details` button
- I am navigated to the `Edit Your Details screen` 

For more on completing work on these stories and how to create pull requests so that they are approved to be merged into the codebase, see [Pull Requests](../Development/PullRequests.md)

## Story Estimation

There are many ways to estimate time based on the full amount of the work contained in these tickets, but one common way in Agile teams is to estimate "story points" for each ticket, and then add them all up. Story point estimation is another topic in itself - one popular way at ARQ is to use the Fibonacci sequence from 1 to 13. 

While the Fibonacci sequence continues past 13, anything estimated to be 13 is generally considered too complex and should be split into smaller stories. 

Here as some examples of what type of work could be assigned to each number in the sequence.

- 1 - no logic, very simple and quick (e.g. updating a string, on one screen)
- 2 - add or update some simple logic, on one screen
- 3 - add or change more complex logic, navigation between screens
- 5 - add or change complex logic, probably multiple screens or changing UI (e.g. based on API responses)
- 8 -  add or change very complex logic, multiple screens and/or changing UI, has impact in other areas (e.g. adding or changing re-used components) AND/OR implementation of third party dependency 
- 13 - multiple scenarios and combinations of the above - split this up if you can!

## Iteration Planning

Iteration planning is an opportunity for the entire team to agree on the stories from the product backlog they plan to complete in the upcoming iteration. They come to this agreement based on the team capacity, the length of the iteration, and the business value expected at the end of the iteration by the product owner/client.

### When is the Iteration Planning held?
It is held in the 1 - 3 days leading up to the next iteration.

### Who should attend?
All roles must be represented.

### Before the meeting
- BA must author the story cards and run them through story review.  
- Any assumptions related to a story card have been validated.  
- Story cards are ready for development.  
- PM/ Scrum Master must schedule iteration planning meeting.  
- Product Owner must think about the business value expected from the iteration.  
- Tech lead should be prepared with the tech spikes / tech cards that need to be played in that iteration.  

### Process
- BA will quickly outline the story cards that are ready for development.  
- The team must decide a goal for the iteration.  
- For example: "To complete the Restaurant Search functionality."  
- The team must calculate their capacity based on historical velocity.  
- The team must decide on which story cards will be played in the iteration based on the velocity and the iteration goal.  
- The stories should align with the sprint goal and add up to something usable.  
- Do not be tempted to change story estimates to squeeze the story into the iteration.  
- Tech Lead and Developers must consider if it makes logical sense to play the selected cards in one iteration.  
- Tech Lead must assess whether any technical card, spike, or debt must be completed before playing the selected cards.  - 
- Testers must assess if the selected cards can be tested in unison.  
- The cards should be arranged in the JIRA board and the physical wall based on the priority of the cards.  
- Priority of, and trade-offs between story cards is made by the Product Owner.  
- Large cards should be played at the start of the iteration to allow the team to complete it on time.  
- The team must work out the dependency between cards while prioritising.  
- The team must consider defects to be fixed in the iteration so that defects do not pile up in the backlog.  

## Kick-offs and Walk-throughs

When a ticket/ story card is created, the BA may "kick-off" by taking the team through what is required and answering any questions, or making any updates needed. Some BAs integrated this with ticket estimation, and do many at once.

When a ticket/ story card is completed, whoever completed it (e.g. the developer/s or platform) may "walk-through" the changes with the rest of the team and/or client. 

Some teams do walk-throughs for each ticket, or at intervals for chunks of functionality. They may do internal walk-throughs getting into technical details, and/or may do demonstrations for clients that are aimed at the end product. Some clients like to do their own walk-throughs and not have the developers present at all.

Whether and how your team does this is up to your team and project specifications.

## Stand-up

Once work has commenced, teams should have a daily check-in meeting to keep things moving. This is generally referred to as a "stand-up" because when physically in the same place it should be so short that you can do it standing up around a table or while having coffee etc.

- **What**: The daily stand-up is an opportunity to ensure communication across the team (Developers, QA, BA, UX, PM).

- **What NOT**: The stand-up is not intended as a status update to management or to other stakeholders.  The stand-up is not a problem-solving or issue resolution meeting. The stand-up is not an opportunity to move any cards on the physical wall - the card must be updated on the wall and on JIRA before the stand up.

- **When**: Ideally in the morning, on each day of a sprint, at the same time and same location.  It is time-boxed to 15 minutes.

- **Who**: Tech Lead, Developers, BA, QA, Scrum Master or PM, Product Owner.  All team members should attend, but the meetings are not postponed if some of the team members are not present. The Scrum Master or PM facilitates the stand-up, each participant addresses their update to the whole team.

**Process**: During the daily stand-up, each team member answers the following three questions:

- (1) What did you do yesterday?
- (2) What will you do today?
- (3) Are there any impediments in your way?  

Issues that are raised are taken offline and usually dealt with by the relevant subgroup immediately after the meeting.  At the end of the stand-up go through the list of blockers. This is to ensure that a team-member has an action on each of those cards and validate whether it is still blocked.

### Tips for running an effective stand-up
- Form a habit. Run the stand-up everyday. Run the stand-up on time.
- Stand-ups should be fast. Give a quick update. If you have no updates, move on to the next person.
- Do not go into technical detail.
- Talk about the functionality in terms that are understandable to everyone attending the standup. For e.g.: Instead of "Yesterday, I worked on story card 99.", say "Yesterday, I worked on the search functionality on the restaurants screen.".
- Do not talk over the team-member giving an update or go into side-discussions. Facilitator must intervene if this happens.
- Do not go into a discussion about issues. Take these offline with the relevant subgroup. Facilitator must intervene if this happens.
- Do not delay raising issues until the next standup. Issues must be raised and resolved as soon as possible. During the standup, broadcast that an issue is being worked on.
- As each person answers the question "What did I do since we met last", everyone should be thinking to themselves "Is there anything that person could help me with based on what they just completed?"
- As each person answers the question "What will I do before we meet again?-  everyone should be thinking to themselves "Is there anything I know which could help that person with their new item?", "Will their work block me or affect my current work?"

### Advantages
- By focusing on what was accomplished yesterday and will be accomplished today, the team gains an excellent understanding of what work has been done and what work remains.  
- Knowing that other team-members are working on the same area encourages collaboration.
- By mentioning what will be accomplished today, each team member makes a commitment to their team; This has the wonderful effect of helping a team realize the significance of these commitments.  

## Tech Huddles

Talking directly with other developers can help solve complex technical problems or areas you are blocked.

- **Who**: Typically speaking, all developers (including tech lead) of a given platform from a scrum team should attend.

- **When**: Tech huddles should be performed daily (e.g. iOS or Android devs meeting). You should also do ad-hoc tech huddle any time you need help or a decision will affect the entire team.

- **What**: Tech huddles are a way for developers to get together and help each other out. There is no right or wrong way to do your tech huddles, do what works for your team - this may involve coffee!

Tech huddles are a time when you can:

1. Get help from your peers on a tech problem or bug.
2. Talk about any blockers you may have.
3. Share design decisions.
    - Explain why you made certain design decisions.
    - Ask for help with design decisions.
    - If you're about to start a new story, there may be some design decisions you need to discuss with the team.
    - Take advantage of the experience and different viewpoints of your team mates.
4. Share interesting tips, tricks or tools you've found.
5. Get updates from the tech lead regarding the project.


## Retrospective Process

- **What**: A Retrospective (Retro) is an opportunity for the entire team to reflect on the past iteration and continuously look for opportunities to improve the way the team operates.  The retrospective is a safe place to express any issues. Team members should feel encouraged to raise concerns without fear of retribution. Another opportunity for coffee...

- **Who**: The whole team. The facilitator can nominate a note-taker to record actions and take notes. Team members may take turns being the facilitator.

- **When**: The retro is held at the end of every iteration. Time-box the retro to one hour. It is acceptable for the retro to take less time, but not more.  

**Process**: The format below is not the only way to hold a Retrospective. The team may try different styles of retrospectives and choose the one that works best for the team.  
     
- (1) The team should review the backlog of actions and team members should provide an update on actions assigned to them. 
- (2) The facilitator should allocate the first 10 minutes for team members to write/ put sticky notes under these three headings:
    - What worked well? 
    - What did not work well?
    - What can be improved?  
- (3) Discussion.
    - The team acknowledges the things that did work well.
    - The team discusses the things that did not work well and identifies solutions.
    - If there are too many things that did not work well or can be improved, the facilitator will ask each team member to vote for 3 issues.
    - The team should discuss the issues in the order of most votes to least votes.
    - The facilitator should create actions and assign them to team members.  


## References and Resources
- [Agile Estimation and Planning - Mike Cohn](https://eva.fing.edu.uy/pluginfile.php/248476/mod_label/intro/%282005%29%20Cohn%20-%20Agile%20Estimating%20and%20Planning.pdf?time=1558526842604/)
- [What is Agile? - Atlassian Agile Coach](https://www.atlassian.com/agile)
- [What is Waterfall Project Management?](https://www.wrike.com/project-management-guide/faq/what-is-waterfall-project-management/)
