# Test Driven Development

### Test Driven Development
Unit tests are commonly automated tests that are created before the code is written [Test Driven Development (TDD)](https://en.wikipedia.org/wiki/Test-driven_development).

#### Frameworks
- [XCTest](https://developer.apple.com/documentation/xctest)

#### Setup
By ticking the checkbox "Include tests" when creating a new XCode project, tests using XCTest will be included. The tests then can be added to or expanded as the development progresses. To add unit tests to an existing project where the project has not been configured for unit testing, follow [these steps](https://medium.com/@CoderLyn/setting-up-a-new-test-folder-205aec029455) instead.

For more, see [Testing](./Development/iOS/Concepts/Testing.md) in this playbook and check out the links below:

## References & Resources
- [The Three Rules of TDD](http://www.butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)
- [Test Driven Development Tutorial for iOS: Getting Started](https://www.raywenderlich.com/5522-test-driven-development-tutorial-for-ios-getting-started)
- [Example of XCTesting](https://www.raywenderlich.com/21020457-ios-unit-testing-and-ui-testing-tutorial)
- [Test Driven Development: By Example](https://www.amazon.com/gp/product/0321146530/ref=as_li_ss_tl?ie=UTF8&linkCode=sl1&tag=&linkId=6d76ed79b7f498d7930b370ad4c059ac)
