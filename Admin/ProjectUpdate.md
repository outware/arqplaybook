# Project Update

## Status

### Sprint #1 (28/09/2021 - 8/10/2021) complete:

- Development/iOS agreed as immediate focus
- Draft markdowns added to Development/iOS 
- Content of existing topics updated and expanded
- Additional topics added where needed
- All draft markdowns peer reviewed

### Sprint #2 (11/10/2021 - 22/10/2021) first week complete:

- Input from senior developers on some draft markdowns in Development/iOS 
- Android and Web notified of project
- Android agreed to start work based on iOS approach and Web to do in future
- Style-guide in Admin folder added and reviewed

### Sprint #2 second week not complete: 

* all developers put on client work
- Some remaining complex draft markdowns  in Development/iOS need senior developer input

- Content page not reviewed

### Sprint #3 (1/11/2021 - 14/11/2021)
coordinator: Audrey

- Gotten senior input for most of the highlighted markdowns
    - testing
    - swift overview 
    - multithreading
    - userNotifications
- Some Markdowns have been updated
    - testing
    - swift overview 
    - multithreading 
    - userNotifications


### Sprint #4 (15/11/2021 - 28/11/2021)
coordinator: Brooke

**iOS**
- still integrating senior dev input,
- creating markdowns and structure for general documents 
            - e.g. source control, testing
**android**
- started to create code concept and general markdowns in line with iOS

Next steps
iOS and Android expect to continue with above in sprint 5
Blockers
not being able to add senior devs to review PRs in repo

### Sprint #5 (29/11/2021 - 12/12/2021)
coordinator: Jess

- Had a restrospective (board can be found [here](https://miro.com/app/board/uXjVObUtK14=/))
- Updated Simon on restrospective findings
- Created a plan of action for the next year

### Sprint #6 (13/12/2021 - 24/12/2021)
coordinator: Audrey

- Responding to Simon's feedback on the playbook so far
- Fixing broken links and the appendix
- Fixing some of the "ways of working" files

### Sprint #7 (31/01/2022 - 18/02/2022)
coordinator: Brooke

- Brooke halfway through accessibility
- Decided to extend sprints and carry over goals into next sprint

## Next Steps

### Sprint #8 (21/02/2022 -18/03/2022)
coordinator: Jess
- carry over goals from last sprint

## Key things to note

ALL markdowns need to be formally reviewed by Simon or a senior developer.

All draft markdowns in Development/iOS need to be formally reviewed

## Contacts

Currrent:
(1) iOS
?

(2) Android
- Sam Cheney

(3) Web
- Amrit Kaur
- Thaisa Stelmo

Previous:
(1) iOS
- Jess Bommer
- Brooke Van Eerden
- Audrey Patricia

(2) Android
- Henry Roth
 
