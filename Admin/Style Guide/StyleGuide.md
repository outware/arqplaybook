# Style Guide

## Markdown Files

### General

Use simple language or "plain English" to the extent possible, but not slang.

Imagine you are explaining something to a complete novice, or even primary school age child. If there is a shorter or clearer word, use it.

Break up paragraphs with short sentences. 

Include line-breaks for ease of reading.

Add diagrams or pictures where helpful.

### File Names

Use either:
- plain English (e.g. 'DesignPatterns', 'LocationAndMaps'), or where that is not possible
- exact name (e.g. 'UIKit', 'Swift')

in camel-case, but with a capital at the start as well.

### Content - Headings and Sub-headings

Use capital-letters at the start of each word, except for conjunctives such as 'and', 'or', 'in' etc.

Where a word is hyphenated, only capitalise the first letter not that of the hyphenated word.

e.g. Content - Headings and Sub-headings

### Content - Code

Where giving code examples using single words or phrases within a text block, use `back-ticks` (place the word inside a set of ` to get this effect).

Where giving code examples using code blocks, use either a set of triple back-ticks or tab:

    class MyClass {
        //
    }

### Content - Spelling

Use Australian English. Australian English prefers British over American spelling. 

For example, use 's' not 'z':

e.g. initialisation (not initialization)

For example, use 'ou' not 'o':

e.g. 'colour' (not 'color')

EXCEPT in code examples where one or the other is required.

### Content - Links

Embed external links to common proper nouns where an explanation seems sensible, such as names of design patterns, and objects in the Apple API that have a reliable source such as an official Apple documentation page.

Embed internal links where there are references that are documented internally, including key concepts and Apple API objects.

Links are embedded using 

    [name](http or internal link)
    
Links to images are embedded using

    ![name](http or internal link)

See below for links in References and Resources.

### Content - References and Resources

Include an embedded link to formal (e.g. Apple) documentation at the top of the file if general, or directly under the sub-heading if it relates directly to that section.

Includes a 'References and Resources' section at the bottom of the page with the full urls in bullet point list:

    ## References and Resources:
    - https://www.hackingwithswift.com/example-code/language/what-is-mvvm
    - https://cocoacasts.com/swift-and-model-view-viewmodel-in-practice
    - https://www.freecodecamp.org/ 
    - https://www.developer-experts.net/en/ 

If the link is ambiguous, prepend it with a title or short explanation:

- [MVVM Diagram - https://www.developer-experts.net/en/](https://www.developer-experts.net/en/)

### Content - Images

Use the Asset folder provided to save and link to images.

In the ARQPlaybook repository's Development folder, the iOS, Android and Web folders each have their own Asset folder - please use your own.

Make sure you references the images that are in your markdown in your 'References and Resources' section (see above).
