# Meeting 1

FILENOTE
MEETING RE updating Arq playbook
Date: 27 Sep 2021, 3pm
Attendees: Audrey Patricia (AP), Brooke Van Eerden (BVE), Jess Bommer (JB)
_______________
General impressions
-    Links inaccessible or broken
-    Some files just had links or not much content or anything – streamline?
-    Repo hard to follow, Website easier to look at
-    Things that could maybe be joined:
-    Process flows e.g. agile
-    Some onboarding/induction info e.g. group by type of person, capability etc
Priorities
-    iOS folder just needs updating and some content added, and we would learn something about iOS
-    Android could based structure on iOS folder if Simon happy with it
Jess notes
-    There should be one intro document for iOS
-    Onboarding could be higher level, or for each area e.g. iOS (copy and paste of same basic intro, short summary of iOS etc)
-    * reference to checking source control tools and conventions -> link to different folder/page (could include instructions for different projects)
-    Code conventions and style guide should be after onboarding
-    After this, section with core concepts related to iOS development -> 
•    Basics and advanced shouldn’t be separate, should just have different sections with summary and more links to resources
•    Not sure about the obj-c stuff, there are links to Apple dev archived docs. Some concepts still seem relevant
IOS folder structure example
-    Onboarding
-    Code conventions and style guide
-    Source control – could go on higher level
-    Core concepts to master
-    Project 1 (tableView, blah)
-    Project 2 (collectionView, coreData, Decodable and Codable, API blah)
-    Project 3 etc (log-in page, NSUserDefaults blah)

Agreed:
-    IOS folder is priority 
-    Timelines: update documents by next week – drafts/ PRs by Mon 2 Oct 2021
Actions
-    Audrey create folder for repo today
-    Flag approach with Simon – Jess to send invite to Simon and team for Wed


