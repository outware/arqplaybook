# Meeting 1

FILENOTE
RE updating Arq playbook
Date: 8 October 2021, 9.30am
Attendees: 
- [iOS] Audrey Patricia (AP), Brooke Van Eerden (BVE), Jess Bommer (JB)
- [Android] Henry Roth (HR)
- [Web] Amrit Kaur (AK), Thaisa Stelmo (TS)
_______________
JB: Thanks for coming, as you may know we are working with Simon Smiley-Andrews on updating the existing digital Playbook and moving it into an ARQ-wide 'ARQPlaybook'. 

This meeting is just an initial check in with Android (HR) and we have invited Web (AK and TS) just to flag it is happening and identify a contact that can be brought in later.

BVE: [sharing ARQPlaybook README screen] This is some background to the project (shows Simon's summary in the repo and talks through it).  

[sharing file structure screen] This is our proposed file structure. We are approaching it from the iOS perspective (we're not focusing on HR stuff for example), but expect to involve Android and then Web building on our work so that we can update this Develop folder. 

JB:  Yes - for example, we are creating a 'Swift language and iOS' overview document, and we have a 'Design Patterns' documents so Android may want some similar documents.

Also our Admin folder contains project management (meeting, sprint details) and style guide documentation - we can talk about moving that up to the 'Develop' level and using that as a common folder so we have standard style and some shared project management docs. This will be deleted once the project is complete, but as BVE and I could be pulled off onto PTV at any moment, we want to make sure there's a place someone could look at to pick up the project and know what the status is if needed. It could also be useful to have a shared Admin folder so the source control etc is coordinated between iOS, Android and Web.

BVE: [sharing screen] This is what the original playbook looked like (shows both repo and website version) and an example of some updated iOS files in the new ARQPlaybook repo.

HR: So are you for example updating it to UIKit because that's new?

JB: No, UIKit isn't new (SwiftUI is the 'new' one) but it exists in current projects and certainly ones BVE and I may be on. We are expanding/ updating including the existing UIKit document. Also we are updating from the old language Objective-C to Swift as Simon asked. 

HR: Great, I think Android already has alot of useful stuff, it just needs to be updated.

BVE: It is really intended as a repository/ webpage where a new-starter could go and - having no experience in iOS - find all the basics. So we have included instructions, examples, pictures, links to tutorials etc where useful too.

JB: We have just completed the first 2-week sprint, which was pretty much adding all these 'core concepts' files that Brooke has just showed you (updating/expanding on the old repo).

Our second 2-week sprint will be speaking to senior engineers and asking them for input and potentially formally review (clone and create Pull Request to add comments and changes). As part of our second sprint we also want to start getting Android involved using our work as a template/ foundation. 

The next stage after that would be Simon discussing it with other managers and senior engineers, and we'd get further direction. As I said however, BVE and I could be on PTV by then, so it could be someone else doing the iOS part then.

Eventually Web could be involved. We're not expecting Web to be involved in this next sprint, just to confirm an appropriate contact. 

Maybe eventually some of the ARQPlaybook will be published, but that would be something for the future. Marketing etc would probably be involved, and Simon would be giving direction and reviewing well before that.

Does anyone have any other questions?

AK: So this is basically just documentation?

BVE: Yes.

JB: I think the important outcomes for us is to agree with Android to have a meeting next week to further discuss, and to confirm with Web an appropriate contact for going forward. Does that sound ok?

HR: Yes, I'm happy to meet next week.

AK: It sounds like I'm the best contact if it is to be involved from a project perspective as TS just joined.

BVE: We are all new to iOS, it was actually a very useful learning experience to research and write these documents.

JB: That's a good point - it could be good for Thai to be involved from that perspective, but it sounds like Amrit you would like to be the contact and you can coordinate that.

AK: Yes

**Agreed**:
-   iOS to discuss with Android further next week
-   AK is the key contact for Web for future
**Actions**:
-   iOS to send invite to Android 
-   iOS to send meeting notes to all and update repo with meeting notes


