#  Sprint 3
coordinator: Audrey

- Week 1 to mid-Week 2 Todos
    * Receiving feedback from senior devs on the following markdowns 
        - Delegates (Jenny)(Brooke)
        - Multi-threading (Yang) (Audrey)
        - ExternalAPICall (Yannian) (Audrey)
        - Combine (Abdullah) (Jess)
        - UserNotification (Cyril)(Audrey)
        - NSNotificationCentre (in terms of Publisher/Subscriber model and Combine)(Abdullah)(Jess)
        - Testing (Hadi) (Jess)
        - Swift overview (Barton) (Brooke)
- Wendesdays are update catchups while Fridays are knowledge sharing catchups 
    * Jess
        * testing.md - Hadi has reviewed and made PR, mostly replacing a few sentences and deleting large chunks of content - Barton will make PR next week, so I think leave Hadi’s for now and incorporate after reviewing and merging Barton’s in case he updates the text that Hadi deleted
        * Combine.md - Abdullah made himself available for a call but I’ve rescheduled to next Mon, and asked if he could review/make PR in meantime
        * NSNotificationCentre.md - I’ll discuss with Abdullah next week as part of Combine meeting
    * Brooke 
        * swift overview.md - caught up with Barton to discuss updates to file to discuss (to be finalised Friday): 
            * to discuss: table of contents (making it work in bitbucket - linking to headings? if not possible - get rid) - **possible**
            * move 2 links (apple & swift documentation) links under Swift header or under "recommended reading" - **DECISION = put it under the heading** 
            * suggestion to stick with the same markdown syntax (* or - for dotpoints etc. ) - going with **-**
        * delegates.md - in conversation with Jenni (will be reviewed when Jenny gets time and catchup with her)
        * Notes from Jenny
            * add markdown about git
            * add markdown about SOLID programming
            * add markdown about how to write commit messages
            * markdown about how to make PRs
            * fix broken links
    * Audrey
        * multithreading.md - got feedback from Yang, will add content based on his feedback
        * UserNotification.md - catching up with Cyril for feedback on Thurs
        * externalAPICall.md - Shared it with Yannian and waiting for her feedback
    
- Mid-week 2 Todos
    - discuss possibilities of grouping some concepts together
    0 e.g. have one overall file called "asynchronous/ synchronous" with 3 files (combine, asyn/await, multithreading GCD) underneath it
    
- Audrey to get an update for
    - progress on the android team 
    - check-in with Simon on when the web team should be involved - **holding off on this, web team are busy!**
    
- Suggestions of markdowns to be added
    - Protocols & protocol oriented programming 
    - Error handling 
    - Encoding & Decoding Types 

