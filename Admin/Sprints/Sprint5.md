#  Sprint 5
coordinator: Jess

**iOS**

Aim to finalise -

*Senior dev input*

- Combine/ NSNotifications [Jess]
- Delegates  [Brooke]
- Generics[Audrey]

**General**

- Testing  [Jess]
- Pull Requests [Brooke]
- SOLID [Audrey]

**android**

- 2 topics (per week) from old repo per person to be updated and moved into new repo 
- Coding Conventions [Henry] 
- Activity and Fragments [Sam] 

**ideas**

- using'Project update' file for talking points for Simon update with following headings (or similar):

1. Status
2. Next Steps
3. Blockers
4. Contacts
