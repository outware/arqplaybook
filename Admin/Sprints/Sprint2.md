# Sprint 2

## Week 1

-    Meet with identified people to get input/ address Qs where needed
-    Update repo with input for own tasks – add person to PR to review after checking with them (or post to Slack channel if they prefer)
-    Create an Admin folder/ md in repo -> meeting notes -> project management -> status of ios topics/ additional topics to-do?
-    Code Conventions and Style Guide Review and update?

## Week 2
-    Add images/ Assets so local folder and make sure sized correctly
-    Update references to make sure all consistent style and we acknowledge where appropriate
-    Update Develop/iOS folder structure to ensure makes sense
-    Ask Simon to review the Develop/iOS folder and contents

## Task List

### Reviewing markdowns task-list

Jess:

- ViewControllers
- UIKit
- Testing
- Delegates
- AudioPlayback
- VideoPlayback
- ExternalAPICall
- UserNotifications

Audrey:

- SynchronousAsynchronous
- Contacts
- LocationAndMaps
- DesignPatterns
- Properties
- Extensions

Brooke:

- Swift
- Singletons
- Closures
- DataStorage
- Multi-threading
- NSNotificationCentre

### Files to add:

- High-level "what is swift" (Jess) - DONE 12.10.21
- Coding conventions (Pull across - check with someone) (Audrey)
- Getting started - technical getting started (pull across) (Brooke)
- maybe CodableDecodable but that could be part of ExternalAPICall (Jess)
 
