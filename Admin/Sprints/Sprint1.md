# Sprint 1

## Week 1

-    Review playbook individually
-    Meet to discuss playbook and agree on overall approach
-    Meet with Simon and get approval for approach
-    Plan 14 day sprint (objectives, priorities, timelines)
o    Agree on source control approach
o    Allocate tasks within between team members (see table below)
-    Create Develop/iOS folder
-    Start completing items on tasklist individually

## Week 2
-    Complete all drafts on tasklist (complete = merged into remote repo)
-    Note outstanding questions/ issues and their status  under ‘outstanding questions + status’
-    Note people/ resources within ARQ that could address outstanding Qs & issues under ‘Who can we ask?’ and ‘Other resources?’
-    Allocate Qs needing senior input to Sprint #2
-    Contact relevant people to get 15 mins in diary for n Sprint #2
-    Line people up for other folders in structure based on iOS (can provide ours as templates – and same horizontals e.g. design patterns, etc) 


## Task List

Add table

### Folder Structure



 

