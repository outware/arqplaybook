#  Sprint 6
coordinator: Audrey
17/01/2022 - 31/01/2022

**iOS**

- [] Make a repo READ.ME [**decision**: do it in the end when we are about to open the playbook to others]
- [✓] Update sourceControl.md and pullRequest.md files (based on Simon's feedback)
- [✓] Adding Fork into the sourceControl.md and to the gettingStarted file 
- [✓] Get input for sourceControl.md 
- [ ] Solid, generics (todo), delegate (todo) finalise markdowns (senior dev input) [SOLID + ExternalAPI feedback incorporated]
- [✓] Fix links on the appendix page and change list to a numeric list 

**Android**

- Look through source control and pullRequest.md 
- Getting started file refresh
- Reviewing core documents 
- [Henry] merge existing PRs
- Talk to Luan to find topics to cover

**Some notes/ planning** 

- Mid-march - update Simon on:  
    - Propose/ plan to start bringing associates to test out the playbook (make a plan)
    - Show draft of playbook 
    - Complete draft README
    - Ask for recorded sessions of Simon sharing topics e.g. Xcode shortcuts, delegates intro, ask for link and add them to playbook? 
