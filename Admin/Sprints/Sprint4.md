#  Sprint 4 
coordinator: Brooke


**iOS**

- fixing/ checking links [Audrey]
    - fix table of content links - completed
    - image links - completed
    - changing all dotpoints "-" - completed
    - adding "swift" syntax to all code blocks - completed 
- adding in "ways of working" markdown
    - SOLID [Audrey] - completed - waiting to be approved/merged
    - how to make PRs [Brooke] - completed - waiting to be approved/merged
    - How to use Bitbucket/ Git [Jess] - in progress
    - Look at old outware and find other things like this to migrate [Brooke]
- finalising the feedback from senior devs 
    - NotificationCenter [Jess] - in progress
    - Combine [Jess] - in progress
    - Testing [Jess] - merged
    - ExternalAPICall [Audrey] - in progress
    - Delegates [Brooke] - to follow up
- Updating markdowns from received feedback

**android**

- 2 topics (per week) from old repo per person to be updated and moved into new repo 
- [Henry] Coding Conventions
- [Sam] Activity and Fragments

ideas
- having videos for things like using Sourcetree (things better explained through walkthroughs)
