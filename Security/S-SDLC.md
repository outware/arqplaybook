# iOS S-SDLC #

This Security Software Devleopement Lifecycle document is a living document to align with the cutting-egde security guideline we recommend to our development team when working on mobile application projects. Security is more important now than ever and so the following document should be a starting point of the considerations you should take into account when developing secure mobile applications.

Before we get into specific security situation and implementations it is worth noting down a number of considerations we must have during the development process

### Considerations

1. Ensure sensitive data is protected at-rest and in transit
2. Consider intentional and unintentional data leak risks across the application
3. Ensure only an authorized application can access sensitive information
4. Ensure that authorized applications can only access their required informations

The following considerations naturally move to the following Principles to be following throughout development and the entire coding lifecycle

### Principles

1. Applications storing or accessing sensitive information should be developed with security in mind from the start
2. Utilize platform specific features and techniques where possible
3. During development do not log sensitive information - to log files, console etc

The following document will go through the high level concepts we want to be considering on both the iOS and Android platforms. In each example we can then link to some platform specific code snippets to dive deeper into considertations we need when actually implementing this functionality. 


## Local Authentication

Local authentication is the process of the user proving that they are the genuine users who should be getting access to this information. Many applications automatically log the user in based on storing a login token - rather than needing to use login details each time. 

This, a good user experience and very convenient, is a security risk that we need to be aware of. 

#### Example
Giving your friend your phone to look at some photos. Your banking application is backgrounded and logged in in the background and they are able to access your bank balance or other sensitive information. 

Both the iOS and Android platforms provide multiple specific login autnentication methods:

- Face ID
- Touch ID
- Login with username and password

<TODO: Write up documentation around FaceID and Touch ID>
<TODO: Don't worry about authentication flow at this login flow is already in most apps>

When developing for the mobile platforms we should protect user's sensitive information behind these local authentication methods, allowing a seamless user experience but also a secure one.      
## Data Protection

An application needs to be designed and architected to protect sensitive information. Both sensitive user information as well as sensitive application information. 

We are all aware of what sensitive user information is - but sensitive application information might not be quite so obvious. 

When an application stores data it stores it in its flash memory. This means that we need to be aware of copywrited audio, video, photos and other documents (for example health reports) that might be stored and considered sensitive.

To handle this we can use [Data Protection](https://developer.apple.com/documentation/uikit/protecting_the_user_s_privacy/encrypting_your_app_s_files) for iOS and [Find out] for Android devices.   

Great - we don't have to worry about anything right? Well no, as a developer you need to know your application, its data usage and the needs of the business. It is up to you to define the necessary protection level for different pieces of data in your application. 

More on Data protection can be found here.

[Write new document and update with different data protection cases.](https://codeburst.io/security-guidelines-for-ios-app-sdlc-52908ec0e008)

## Password/Token management

Almost all applications in the market need to store small pieces of sensitive information, such as passwords, keys and login tokens. Both iOS and Android utilize the keychain to provide a secure way to store these items.
 
An application should always utilize all the possible platform build in safety features based on the need of the business to give protection to sensitive data without compromising the functionality.

[More information can be found here](https://codeburst.io/security-guidelines-for-ios-app-sdlc-52908ec0e008)


## Persistant storage

When storing small pieces of sensitive information the device keychain is a perfect solution to securely storing this information. But what happens when we want to store a significant amount of information - the keychain is not the right tool for this - so we have to understand how and when to store different pieces of information on our devices. 

In general when storing information in the form of a database we will use Coredata (for iOS), Realm or SQLite. These by default are not secure - but can be secured by using protection classes to encrypt the sandbox in the device when the device is locked. 

This though is is compromised if a user has a jailbroken phone or their information is auto-uploaded to the iCloud store. 

This means that encrypting locally stored data is very important - even though the iOS sandbox is not accessible by third parties.

#### Device not passcode protected

It is worth noting that we cannot assume a user is utilising a passcode or biometrics for their device. This means that their data might be compromised even if it is encrypted. 

There are a number of ways of dealing with this:

1. Don't store sensitive information locally if the user doesn't have a passcode set. This is something that the banking team at ANZ have 
2. Consider using encryption local storage like Realm or SQLCipher.

## Network Security

User's sensitive data is at risk when in transit as well as when at rest. We need to ensure both are secured and considered.

1. App Transport Security

This is by defaul enabled in iOS and enforces that connections must be HTTPS. Developers can bypass this for specific API instances and third party frameworks. 

2. Certificate Pinning

When an application establishes an HTTPS connection sensitive data might still be at risk. User devices may still connect to insecure wireless networks which leaves data vunerable to man-in-the-middle attacks - snatching the sensitive data we want to protect.

[SSL Pinning](https://www.guardsquare.com/blog/iOS-SSL-certificate-pinning-bypassing) ensures that the app will only communicate with designated servers. The SSL certificate is generally saved to the local app bundle.

Due to this we need to be aware of whenever the servers SSL key is changed as we will need to update the app with a new certificate.

But keeping a certificate in a local app bundle is not the right approach to ensure our architectural design, maintainability and security needs. 

A better approach is for every certificate has its own public key. That can persists in the renewal process. Google.com is the best example of this.

Create a hash (sha256) of the public key and store them in the application. It has several advantages like, easy to manage due to a small size and it allows shipping an application with hash which is irreversible. 

[Public key approach](https://www.bugsee.com/blog/ssl-certificate-pinning-in-mobile-applications/)

3. Avoid caching HTTPS requests/responses  

In the iOS NSURLSession framework, the default session configuration has a cache policy enabled. Hence, it creates the Cache.DB file to persist the request/response information till it does not expire.


1. Developers should use the ephermeralSessionConfiguration which doesn’t have persistent storage for caches, cookies or credentials. (Private Browsing)

2. Remove shared cache as and when required.

URLCache.shared.removeAllCachedResponses()

3. Disable global cache

let cache = URLCache(memoryCapacity: 0,diskCapacity: 0,diskPath:nil)
URLCache.shared= cache

Caching helps network performance by reducing the number of requests, turning it off for any data you think is highly sensitive can be a good idea.

## Application level

Within the application layer there are many different things that the application does without us necessarily being aware of this taking place. Knowing what the phone does automatically is a must to ensure that we can turn it off for when we are dealing with sensitive information. 

A. User Input — Disable Caching
iOS keyboard caches the data for auto-correction. Auto-correction should be turned off for fields used for sensitive user data input.
[Check android]

UITextFieldInstance.autocorrectionType = UITextAutocorrectionTypeNo

B. Prevent system snapshots
The system takes a snapshot of the currently visible screen before moving an application to the background. This is a risk as a snapshot might be taken and stored of sensitive information in an unsecure location.

When an application is entering in the background state, a developer should take appropriate steps to secure sensitive information.

a. Hide or obscure passwords including other sensitive information which might get captured as part of the snapshot.
b. Cover the visible screen with splash image, blur the current view or present the blank screen.

C. Detect Screenshot
Users can capture a screen with the combination of a device hardware keys at any point-in-time.

APIs are available which triggers the notification on screenshot event.

NotificationCenter.default.addObserver(self, selector: #selector(screenshotTaken), name: UIApplication.userDidTakeScreenshotNotification, object: nil)

However, APIs are not available to prevent a user from taking a screenshot. As a developer, we can capture these events to take an appropriate action based on the business requirement.

D. Detect Screen Recording
A screen can be recorded using the iOS platform feature. An application can’t stop screen recording. However, it can detect a recording status and take relevant action as per the business requirement.

Approaches,

- Overlay with blur view.
- Exit gracefully


E. Screen Mirroring
The devices can connect to external displays. It will bypass all the above record prevention techniques in place. The screen captured from the mirrored screen.

How to incur it?

APIs are available which notify whenever an external display is attached to the device. The UIScreenDidConnectNotification will notify if an external display is connected to the iPhone/iPad/iPod when our application is in running state.

What if an external display connected before launching an application?

The UIScreen framework has a method called screens which let you know the currently connected screens.

F. Logging mechanism

Logging information is one of the crucial tasks in development. Developers always prefer to log all sort of information on the console. Intentionally or unintentionally it can log the sensitive information on the console. Hence, owning a logging strategy is a crucial task in the SDLC.

Apple has introduced the unified logging system provides a single, efficient, performant APIs for capturing messaging across all levels of the system.

See the key has been replaced by <private>

Secrete key is replaced with <private>
To know more on logging read Unified Logging and watch this WWDC video.

G. Jailbroken Device Check

iOS jailbreak removes software restrictions imposed by Apple. It provides a root level access to the users to make the entire device and data vulnerable.

The application should verify if it is running on the secured device else it should erase all the sensitive information under app context and exit immediately.

Here’re a couple of ways to check if an application is currently running in a jailbroken environment:

The existence of files that are common for jailbroken devices.
Reading and writing in system directories (sandbox violation).
Detect JailBreak Device in Swift 5 (iOS) programatically

Avoid Attackers to intrude in your application by all means possible in a single page| Supported to Swift (world first…
sabatsachin.medium.com

H. App Sandboxing
Sandboxing is a great way to protect users by limiting the privileges of an app to its intended functionality, increasing the difficulty for malicious software to compromise the users’ system. Must read App Backup Best Practices.

App sandbox directory structure

I. Obfuscation

An application uses many API access keys from 3rdparty services, passphrases, passcodes, secrets, for encryptions as plain hardcoded strings in the code.

Which we might not intend to be exposed?

This hard-coded sensitive information has to be obfuscated and turned them into a byte array.

/*Original - XYZ123ABC*/
static let GOOGLE_API_KEY: [UInt8] = [24, 34, 86, 66, 12, 345, 4, 81, 45, 49, 32]
There are many open source approaches available as listed below.

obfuscateapi: Obfuscate/defuscate strings in AES128 format
SwiftSHIELD: protects iOS apps against reverse engineering attacks.
Obfuscator: Blog to understand obfuscation with way to implement.

## TWO-FACTOR AUTHENTICATION

Two-factor authentication (2FA) adds an extra layer of security for users that are accessing sensitive information.

Ways to implement multifactor authentication in a Mobile app.

Time Based OTP with authentication provider (Google Authenticator, Microsoft Authenticator etc.)
- SMS
- Email
- Push Notifications

## AUDITS AND REVIEW

In SDLC, the expectations from the team are they should follow all the security guidelines to make the application & its data secure.

A team has two eyes for ensuring the safety of the application and data.

1st eye: Developers
2nd eye: QA

It’s not enough though. A team needs an outside person the 3rd eye. They will stamp on the safety aspects of the application.

- Regular code review
- Security Audits

These points look interesting, exciting, and so on. However, regular code review, security audits looks cumbersome to me.
