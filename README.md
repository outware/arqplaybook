# README #

The goal of the ARQ Playbook

1. To update the current playbook resource we have
2. To sort the current playbook into a more convenient order / folder structure
3. Eventually to move the current playbook online - open source - to show our knowledge in the space

Steps to understand the playbook

1. Spend some time going through the current playbook


a. The repo: https://bitbucket.org/outware/playbook/src/master/
b. The site: https://playbook.outware.io/

2. Understand the structure of the information to identify the new folder and architecture structure for the new playbook
3. Create a contents page on the readMe (the contents page should link to specific pages listing specific page locations)
4. Create a complete glossary page - that links to every different page - link this to the contents page
5. Start moving across pages from the old playbook and updating them - we need to 

- Check whether the information is still relevant
- Update Objective-C to swift
- Group information that is useful
- Identify missing areas to make a complete playbook.


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact