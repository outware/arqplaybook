# Proxyman Proxy Tool

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Downloading Proxyman](#markdown-header-downloading-proxyman)
- [Features that Facilitate the Debugging Process](#markdown-header-features-that-facilitate-the-debugging-process)
- [Getting Started](#markdown-header-getting-started)
- [Basic Features](#markdown-header-basic-features)
- [Advanced Features](#markdown-header-advanced-features)

## Introduction

Almost all applications communicate over HTTP. Examples of these include getting data from an API or posting data to an API. 

Sometimes problems can arise, requiring us to dive deeper into the network layer and figure out what is causing the problem. In these situations proxy tools can be useful and act as the middle-man between our code and the API, allowing us to see what happens to the data between the API and our code. 

In general, we can use proxy tools for things like: 

- Observing the HTTP requests/ responses
- Modifying the HTTP requests/ responses (which can be useful in testing our code's error handling capability)
- Network Throttling (e.g. simulating different situations that users might encounter)

While there are many tools, here we will focus on two popular tools used at Arq, Charles and Proxyman. 

Both Charles and Proxyman have paid licenses and free access. While Arq provides a few licenses for these tools, the free version of these tools are usually enough as the limits are usually on the number of map functions, breakpoints, composed requests etc. Which can easily be cleared out when they are no longer needed. 

## Downloading Proxyman 

[Proxyman](https://proxyman.io/)

## Features that Facilitate the Debugging Process

Basic features include: 

- Intercepting and analysing HTTP/HTTPS network requests and responses
- Observe requests and responses on mobile devices/ simulators 
- Repeat, Compose requests/ responses (allowing you to send composed responses on a click of a button on the tools)
- Export requests/ responses to common files (e.g. exporting a log file)

Advanced features include: 

- Local Map: find matching requests (according to rules we set) and replace responses with local files we provide 
- Breakpoint: setting breakpoints on matched requests/ responses and be able to manipulate them before continuing the network request/response
- Whitelist/ Blacklist: allow/ block certain requests 
- Rewrite: define changes to one or more calls in advance and have these rules apply during runtime 
- Network Throttling: simulate various types of network connections

## Getting Started 

To get started with Proxyman, head over to this [resource](https://proxyman.io/posts/2019-06-14-getting-started-with-proxyman)

The image below will be useful as we look into the powerful features Proxyman provides.

![](./iOS/Assets/ProxyTools/ProxymanLayout.png)

When using proxy tools to debug API calls on a simulator/ physical test device, these devices need to first be set up to capture the HTTP/HTTPS messages

Proxyman:

- [iOS setup guide look here](https://docs.proxyman.io/debug-devices/ios-device)
- [Android setup guide look here](https://docs.proxyman.io/debug-devices/android-device)

Note (for Proxyman): 

![](./iOS/Assets/ProxyTools/ProxymanDeviceSetup.png)

The Proxyman application also provides a step by step guide on how to set devices for proxying, this can be found under *Certificate > install certificate on iOS > Physical devices.../ Simulators...*

An equivalent guide can be found for Android devices under *Certificate > install certificate on Android > Physical devices.../ Emulators...*

## Basic Features 

### 1. Intercepting and analysing HTTP/HTTPS network requests and responses

You can intercept and analyse requests/ responses through breakpoints. 

A breakpoint will pause the request/ response and allow you to view/ change it's content before resuming it. 

![](./iOS/Assets/ProxyTools/ProxymanBreakpoint.png)

To set a breakpoint, navigate to Tools > Breakpoint > Rules
You will then see a window pop up like the image above after clicking on the "+" button on the bottom left of the screen.

Setting a breakpoint rule tells the proxy tool to pause at the specified request. To create a rule, you will need to give it a name, so that it is easily identified in the breakpoint rules table. 
Then add in the API URL into the matching rule text field. Here you can also use wildcard to specify breakpoints on all subpaths of the URL 

e.g. `http://numbersapi.com/random/trivia` - using this sets a breakpoint for this specific endpoint of the API
but this `http://numbersapi.com/*` - specifies that you want a breakpoint to be set for all subpaths and endpoints of the numbersApi

You can also set rules of what type of request you want breakpoints to be added, e.g. just for GET/POST requests, or ANY for all types 

Lastly, you can select to put breakpoints on either both the request or response, selecting one will only pause the process for that instance. 

Once your rule is ready, click on the "add" button to add this breakpoint rule into your list of rules.

![](./iOS/Assets/ProxyTools/ProxymanBreakpointRules.png)

Breakpoints can easily be enabled and disabled through the break point rule table shown above.

![](./iOS/Assets/ProxyTools/ProxymanBreakpointExample.png)

When a breakpoint is triggered, you will receive a new window pop up with the details of the request/ response, you can also change the body of the request/ response under the "Body" tab.

When you are ready, you can choose to execute (continue) the request/ response, or cancel/ abort the response. 

TUrning off a breakpoint when you don't need to debug a network call is as simple as disabling (unchecking) the breakpoint rule on the breakpoint rules table.


### 2. Observing requests and responses

![](./iOS/Assets/ProxyTools/ProxymanRequestOverview.png)

You can view your requests and responses for certain URLs. This is easier if you use the filter function on the bottom left.

It may even be helpful to right-click on the URL on and pin it, so that it will be easier to access next time if it is something you will need to proxy a lot for. 

After selecting the correct URL on the source list, you will see requests pop up in your request list panel. On selecting one of these, you are then able to view details on it's request (bottom left side) and response (on the bottom right side).   


### 3. Composing requests

![](./iOS/Assets/ProxyTools/ProxymanCompose.png)

You can compose requests/responses to be sent to your targeted API and view it's response through the request list panel and see the details of the request/ response. This can be useful in testing how your API reacts to certain requests/ responses. 

(A) You can change the HTTP method through this drop down (e.g. make a GET/POST request) 
(B) This where you put your targeted HTTP/HTTPS API address 
(C) Fill this section with the JSON body of your request/ response 
(D) You may add in any necessary headers e.g. `x-api-key` by clicking on this header button and filling in the header's key-value pairs

Once you have filled these out, click send to process the request and you should be able to see the details of these one your request list, on clicking the instance of the request you just sent, you will see it's details on the content panel.

### 4. Exporting responses/ requests 

![](./iOS/Assets/ProxyTools/ProxymanExport.png)

Proxyman support exporting logs to multiple file types. 

To export, click on request/ response on the request list panel, then right click and select Export.

File types: 

- Proxyman Log: A built in log that contains all requests and responses information
- HAR: Suitable for transferring HTTP requests and response to other apps 
- CSV: Exports selected requests to a CSV file 
- Postman: Exports as a Postman Collection

## Advanced Features 

### 1. Local Map

The Local Map Tool can be used to quickly manipulate the response of API calls. You can use the content of local files as a response to your requests, these are done by setting matching rules.
This can be useful in testing several edge cases without explicitly changing/ updating the data in your servers. This helps us test the app's behaviour with different responses. 

You can find this tool under Tools > Map Local 

To create a new matching rule select the "+" button on the bottom left. 

![](./iOS/Assets/ProxyTools/ProxymanMapLocal.png)

Add in the URL and indicate whether you want to MAP for specific type of responses e.g. only GET/ only POST or ANY. Wildcards are also supported for matching rules to map to all subdomains of a specific domain. 

You can then map your response to a local file by selecting "Select Local File" on the bottom left and using finder to locate the location of your local file with the stubbed response.

Once your matching rule is set, make sure that your map local rule is enabled and when your app hits this matching rule Proxyman will intercept and send the stubbed response from the local file you have assigned to the rule.

### 2. Breakpoints

The breakpoints feature can be found under Tools > Breakpoint. Under This menu there are 2 items, Rules and Breakpoints.

The breakpoint item stores all the current breakpoints Proxyman has triggered, breakpoints will stay there until you have executed (continue the request/ response) or cancelled them.
 
The rules item are where you set rules for breakpoints, e.g. adding a matching website link for the proxy tool to break at.
To create a breakpoint rule navigate to Tools > Breakpoint > Rules and select the "+" button on the bottom left.

![](./iOS/Assets/ProxyTools/ProxymanBreakpointRules2.png)

On the popup, give the breakpoint a name so it can be easily identifiable in the list of breakpoint rules you have created. Then fill in the matching rule textfield with the link of the breakpoint you want to set. 
You can also change the matching rule to only include specific type of requests e.g. ANY, GET, POST. It is also possible to add a wildcard to match the different subdomains of a specific site, e.g. we could use the matching rule `http://numbersapi.com/*` to match all the API endpoints of the NumbersAPI and cause a breakpoint to happen every time they are called. 
You can also set whether the breakpoint is enabled for a Request or a Response. Once your rule is created, click on the "Add" button on the popup to save this rule.

![](./iOS/Assets/ProxyTools/ProxymanBreakpointTable.png)

The breakpoint rules window will save all your created rules, you can easily enable and disable these by checking/ unchecking the checkbox on the left of each rule. 


### 3. Whitelist/ Blacklist

Whitelist/ Allow list is a tool you can use to define what domains you allow to appear on the Proxyman app, unmatched requests will not be shown on Proxyman. 
This helps reduce the number of unnecessary requests being shown on Proxyman, allowing you to focus on what you need to track. 

To use this tool, you can find it under Tools > Allow List. To create a new matching rule tap on the "+" button on the bottom left and fill in the popup to create a matching rule.

Blacklist/ Block List is useful when you want to block/ hide certain domains in your debugging session. All enabled rules on the block list will drop the connection to those specific sites/ calls.

To use block lists, you can find it under Tools > Block List. Similar to Allow lists, create a new matching rule and ensure it is enabled. 

### 4. Rewrite

Proxyman provides a scripting feature that you can use to write JS code to manipulate Request/ Response in a flexible way. 

Some benefits for this include: 

- Implement Map Local / Map Remote / Breakpoint by JS Code. 100x Faster
- Change the Request Content, includes Domain, Host, Scheme, Port, Path, HTTP Method, HTTP Headers, Query, Body (Encoded-Form, JSON, plain-text)
- Change the Response Content, includes HTTP Status Code, HTTP Headers, Body 
- Provide plenty of built-in add ons and libraries for common tasks, such as Hashing, Encode/Decode, JSON-Text transformer, Beautify etc.
- Able to write your own JS Add ons or Libraries

To find out more about how this tool can be used and setup, refer the Proxyman's documentation [here on scripting](https://docs.proxyman.io/scripting/script)

![](./iOS/Assets/ProxyTools/ProxymanNetworkEnvironments.png)

### 5. Network Throttling

This feature allows developers to simulate poor networking environments, Proxyman provides a variety of different environments. 

You can also apply these environments to the whole system or certain domains only. 

You can use these tools by navigating to Tools > Network Conditions. Create your network condition environment by tapping on the "+" button on the bottom left and make sure to enable it. 

![](./iOS/Assets/ProxyTools/ProxymanNetworkCondition.png)
