# Source Control

Source control refers to the process of managing and recording changes in the code base. 

It generally consists of a *remote* codebase (e.g. one stored in the cloud, that everyone can access and work on at the same time) and a *local* copy. To make changes to the main codebase, "branches" are created - both remotely, and then locally on your own computer to make changes through "commits" which are then pushed to the remote branch.

When people are creating multiple branches from one code base, and making changes simultaneously, this has the potential to get very messy. There may be "conflicts" between various changes.

Source control tools and patterns help to manage this, and create a lasting record of the changes.

## Git

Git is a distributed version control system (DVCS) with an emphasis on speed, data integrity, and support for distributed, non-linear workflows.

In conjunction with Git, we typically use a cloud-based hosting service to allow our code changes to be seen remotely and by the entire team. These include Github, GitLab and Bitbucket, all of which we interact with using Git.

At ARQ, we use Atlassian's Bitbucket git cloud service to support our software development process and adopt the [Gitflow Workflow convention](http://nvie.com/posts/a-successful-git-branching-model/), which defines a strict branching model designed around the project release.

## GitFlow

One methodology to keep the use of Git consistent across the team is Gitflow. This enables a developer to easily maintain and manage the non-linear workflow that Git provides. It is likely different clients and projects you work on will have different Git strategies, all of which will have their own strengths and weaknesses. For example, one client of ARQ use Trunk Development rather than GitFlow.

### What is GitFlow?
GitFlow is a branching model for Git, which defines a strict branching model designed around project release. It assigns very specific roles to different branches and defines how and when they should interact.

Previous Version Control Systems (such as Subversion and CVS) tracked the whole history of a repository using a single master or trunk branch. Instead of this, GitFlow uses two branches to record the history of the project.

![](processes/gitflow.svg "Gitflow")

### Branch structure and use
|Branch name|Branch Type|Description|Branch from|Merge into|
|:----------|:---------:|:----------|:---------:|:--------:|
|master|Historical|This branch stores the official release history, where the HEAD always reflects a production-ready state. It only ever contains *production delivered* code only. Master branch commits *must be* tagged with the release version. eg: 1.0.0|-|-|
|develop|Historical|This branch serves as an integration branch for features, where the HEAD always reflects a state with the latest delivered development changes for the next release. This branch should *always* compile and is where Jenkins Development and OMQA builds are built from.|master|-|
|feature/*`<JIRA-Number>`[-`<optional-card-title-or-description>`]*|Feature|Each new feature should reside in its own branch, which can be pushed to the central repository for backup/collaboration. But instead of branching off from master, feature branches use develop as their parent branch. When a feature is complete, it gets merged back into develop. Features should never interact directly with master.In addition to the JIRA-Number, although not mandatory, it is advisable to add the title of the card being developed. Words should be separated using hyphens.|develop|develop|
|release/*`<version>`*|Release|Once develop has acquired enough features for a release (or a predetermined release date is approaching), a release branch is forked off of develop. Creating this branch starts the next release cycle, so no new features can be added after this point -- only bug fixes, documentation generation, and other release-oriented tasks. Once it is ready to ship, the release gets merged into master (where it is tagged with a version number), and develop (which may have progressed since the release was initiated).Using a dedicated branch to prepare releases makes it possible for one team to polish the current release while another team continues working on features for the next release. It also creates well-defined phases of development (eg: it's easy to say, "this week we're preparing for version 4.0" and to actually see it in the structure of the repository.This branch is used to build Jenkins DropClient builds, which are released to the client for UAT and/or the AppStore/PlayStore (Production).|develop|develop, master|
|hotfix/*`<JIRA-Number>`[-`<optional-card-title-or-description>`]*|Maintenance|This is used to quickly patch production releases. This is the only branch that should fork directly off of master. As soon as the fix is complete, it should be merged into both master and develop, and if there is an active release branch. The master branch should be tagged with an updated version number, usually a minor increment in the version. Having a dedicated line of development for bug fixes lets your team address issues without interrupting the rest of the workflow of waiting for the next release cycle. You can think of maintenance branches as ad hoc release branches that work directly with master. In addition to the JIRA-Number, although not mandatory, it is advisable to add the title of the card being developed. Words should be separated using hyphens.|master|develop, master, release|
|source/*`<version>`*|Source|This is only used when the code needs to be delivered to the client.|develop|-|

## Commands
Git flow commands follow a very simple structure, which is easy to remember and follow.

| |action type|action|branch name|
|:---:|:---:|:---:|:---:|
|git flow|init| - | - |
|git flow|feature, release, hotfix|list| - |
|git flow|feature, release, hotfix|start, finish, publish, pull|name|


|Git flow commands|Description|Respective git commands|
|:----------------|:----------|:----------------------|
|**`git flow init`**|Initialises the git flow environment on the current repository||
|**`git flow feature list`**|Lists all the feature branches that exist locally||
|**`git flow feature start <feature name>`**|Creates a new feature branch named feature/`<feature name>` and switches to it. Note that there is no need to enter the *feature/* prefix.|`git branch feature/<feature name>`, `git checkout feature/<feature name>`|
|**`git flow feature publish <feature name>`**|Pushes the branch to the origin so other developers can access it.|`git push origin <feature name>`, `git config branch.<feature name>.remote origin`, `git config branch.<feature name>.merge refs/heads/<feature name>`, `git checkout <feature name>`|
|**`git flow feature pull [alias] <feature name>`**|Pulls the latest code from the origin. You can optionally add the origin's alias (especially if working with multiple origins for the same repository.|**First time feature pull:**, `git fetch -q origin<feature name>`, `git branch no-track <feature name> FETCH_HEAD`, `git checkout -q <feature name>`  **Subsequent feature pull** `git pull -q origin<feature name>`|
|**`git flow feature finish <feature name>`**|Finishes the feature and merges the code back into develop.|`git checkout develop`, `git merge --no-ff <feature name>`, `git branch -d <feature name>`|

The *feature* action type can be replaced with *release* and *hotfix*, which will execute the action as described, with the branches branching and merging as per the diagram above.


## Conventions

### Commit messages

Commit messages are how developers describe *why* a change has been made and potentially *what* has been changed, as opposed to *how* the change works.

There are two parts to a git commit message: the subject line and the body.

The subject line gives a high-level desicription of what the change is about, and is written in the following way:

* It is limited to 50 characters
* Is written in sentence-case (capital first letter, then as needed)
* Does not contain a full-stop at the end of the line
* Is written in the imperative mood (is read like a command, eg: Fixes alignment on Dashboard screen)

The body provides details about the change, explaining what was changed and why it was changed. It shouldn't describe how it works, as the code should be self-explanatory.

### Merging vs Rebasing
There are two ways in which to merge feature branches back to the main branch (eg: develop): *merging* and *rebasing*.

Merging provides the ability to keep the full history of all the commits on the branch.

Rebasing, on the other hand, changes the commits by taking the feature branch, and physically putting it at the HEAD of the main branch. It does this by making sure that all the commits that have happened on the branch since the last base/rebase have been applied. Then all the commits from the branch will be submitted to the main branch.

Note that rebasing should never be used on public branches, ie: develop or main, as it can result in very messy situations where Git thinks your main branch history has diverged from everyone else's.

The rule of thumb is if **anyone else is looking at the branch** do not use rebase. Consider a non-destructive way to make your changes (e.g., the git revert command) .

See [Merging vs Rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing/conceptual-overview) for more information.

## GUI apps for git
All git operations can be performed through the command line. If you are very comfortable with the command line you may prefer to stick with that.

However, many developers use a Graphical User Interface (GUI). This does the same thing as the command line under the hood, but can be easier to work with. For example, it can help to group commits under one theme, type well-formatted and spelt titles and comments, and - importantly - view the history of the code "tree" in a user-friendly design.

SourceTree is the most commonly used git GUI at ARQ, however Fork and Git Kraken are also popular and perfectly fine. It is ultimately about making your job easier and faster, so choose the one that is best for you! 

### SourceTree
A free git tool built by Atlassian. [Download SourceTree](https://www.sourcetreeapp.com/)

### Fork
A free git tool (you can ignore the 'please purchase' message unless you want to contribute). It is very similar to SourceTree, but has slightly quicker updating of changes and some other UI advantages.  [Download Fork](https://git-fork.com/)

### Git Kraken
A free git tool with a retro look about the UI. [Download GitKraken](https://www.gitkraken.com/)

## Tricks

### What should you do when SourceTree goes unresponsive

Sometimes, SourceTree becomes unresponsive. 

First, try restarting your computer (after you stash or commit any outstanding changes!).

If this doesn't work, here is another trick you can try:

On Mac:

* If you're using an OS X version before Mavericks: Remove ~/Library/Preferences/com.torusknot.SourceTreeNotMAS.plist
* If you're using Mavericks or later, run the following in the terminal

        defaults delete com.torusknot.SourceTreeNotMAS
        

### What should you do if Git keeps asking for your passphrase

If you are using Git with command lines, Git may keep asking your passphrase on every pull, push or fetch (this issue usually happens on macOS Sierre 10.12). There is a trick you can try to solve this problem:

On Mac:

* Edit the ssh config file and enable the `UseKeychain` option

        vim ~/.ssh/config

* 

        Host *
        UseKeychain yes
        
* Then add your passphrase to the keychain

        ssh-add -K

## References & Resources
- [Git commit messages](http://chris.beams.io/posts/git-commit/)
- [Getting Started -- git flow](http://yakiloo.com/getting-started-git-flow/)
- [Atlassian Tutorials](https://www.atlassian.com/git/tutorials/advanced-overview)
