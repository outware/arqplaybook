#  Charles Proxy Tool

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Downloading Charles](#markdown-header-downloading-charles)
- [Features that Facilitate the Debugging Process](#markdown-header-features-that-facilitate-the-debugging-process)
- [Getting Started](#markdown-header-getting-started)
- [Basic Features](#markdown-header-basic-features)
- [Advanced Features](#markdown-header-advanced-features)


## Introduction

Almost all applications communicate over HTTP. Examples of these include getting data from an API or posting data to an API. 

Sometimes problems can arise, requiring us to dive deeper into the network layer and figure out what is causing the problem. In these situations proxy tools can be useful and act as the middle-man between our code and the API, allowing us to see what happens to the data between the API and our code. 

In general, we can use proxy tools for things like: 

- Observing the HTTP requests/ responses
- Modifying the HTTP requests/ responses (which can be useful in testing our code's error handling capability)
- Network Throttling (e.g. simulating different situations that users might encounter)

While there are many tools, here we will focus on two popular tools used at Arq, Charles and Proxyman. 

Both Charles and Proxyman have paid licenses and free access. While Arq provides a few licenses for these tools, the free version of these tools are usually enough as the limits are usually on the number of map functions, breakpoints, composed requests etc. Which can easily be cleared out when they are no longer needed. 

## Downloading Charles 

[Charles](https://www.charlesproxy.com/download/latest-release/)

## Features that Facilitate the Debugging Process

Basic features include: 

- Observe requests and responses on mobile devices/ simulators 
- Repeat, Compose requests/ responses (allowing you to send composed responses on a click of a button on the tools)
- Export requests/ responses to common files (e.g. exporting a log file)

Advanced features include: 

- Local Map: find matching requests (according to rules we set) and replace responses with local files we provide 
- Breakpoint: setting breakpoints on matched requests/ responses and be able to manipulate them before continuing the network request/response
- Whitelist/ Blacklist: allow/ block certain requestes 
- Rewrite: define chanegs to one ormore calls in advance and have these rules apply during runtime 
- Network Throttling: simulate various types of network connections

## Getting Started 

To get started with Charles, head over to this [resource](https://www.charlesproxy.com/documentation/ios/getting-started-1/)

When using proxy tools to debug API calls on a simulator/ physical test device, these devices need to first be set up to capture the HTTP/HTTPS messages

Charles:

- [iOS device setup guide](https://www.detroitlabs.com/blog/2018/05/01/how-to-set-up-charles-proxy-for-an-ios-simulator/)
- [Android device setup guide](https://hackupstate.medium.com/using-charles-proxy-to-debug-android-ssl-traffic-e61fc38760f7)

## Basic Features

### 1. Observing requests/ responses 

To observe a specific request, it may be useful to use the filter feature on the bottom left of Charles. 

Here you can filter for a specific API/ website. E.g. in the image below we have filtered for the "numbers" (numbersAPI) call.

![](./iOS/Assets/ProxyTools/CharlesRequestOverview.png)

Clicking on the request on the left side panel opens up the overview of the request, where we can see detailed information of the URL, Status, Response, Timing etc. 

To view the received response, select on the "Contents" tab. 

![](./iOS/Assets/ProxyTools/CharlesResponse.png)

Here you can see the response from the API call and can even view the Query String or the Raw data if needed. 

### 2. Composing requests/ responses

To compose a request, choose "Compose" from the tools menu on Charles. 

![](./iOS/Assets/ProxyTools/CharlesCompose.png)

A screen like the image above will pop up. 

To compose a requests: 

A) Select a HTTP method you would like to use (e.g. GET/ POST)

B) Place in the URL of your targetted API
 
C) If there are any custom headers you need to put in to the API call, select the "Headers" tab and add them in
 
D) If you need to send a JSON body, add it in under the "Text" tab
 
E) Clicking "Add" adds the composed request to your list of composed requests
 
F) When ready click on "Execute" to fire the composed request and Charles will immediately show the details fo this request

### 3. Export requests/ responses

![](./iOS/Assets/ProxyTools/CharlesExport.png)

To export data from a session, choose Export from the file menu.

Charles supports exporting data to several different formats. 

File types: 

- CSV: this will contain summary data about the timing and size of requests and responses
- Trace File Format: a plain file text format specific to Charles
- XML: designed for exchanging sessions recordings with 3rd part software

## Advanced Features

### 1. Local Map

![](./iOS/Assets/ProxyTools/CharlesLocalMap.png)

To enable local map, navigate to `Tools > Map Local` and enable this tool. Once enabled, you can add a new location and rule to map for. 

![](./iOS/Assets/ProxyTools/CharlesAddLocalMapRule.png)

On the popup fill in the API addresses' details in the Map from column. Then under "Map to" add the path to the local file you would like to map to the response of the previously sepcified API call.

Once your rule is added, ensure it is checked to enable it. When hitting this API address, the response should be mapped to the local file you have indicated on the rule. 


### 2. Breakpoint

To easily set breakpoints to specific network calls, find the API call you are targetting on the left hand side of Charles (it might also be helpful to use the filter feature), then left click on that specific request and select "Breakpoint". This will set a breakpoint on that specific request. 

![](./iOS/Assets/ProxyTools/CharlesBreakpoints1.png)

You can also set breakpoints by navigating to `Proxy > Breakpoint Settings`. Here check "enable Breakpoints" and add the specific address of where you want a breakpoint to be triggered

![](./iOS/Assets/ProxyTools/CharlesBreakpoints2.png)


### 3. Whitelist/ Blacklist

Add addresses to your proxy tool block list by going to `Tools > Block List`.

![](./iOS/Assets/ProxyTools/CharlesBlockList.png)

On the popup you can add addresses to block and check them to block calls to these addressses.

Similarly, to add to you whitelisted addresses, navigate to `Tools > Allow List`. 

On the popup you can add addresses to allow and check them to allow only calls to these addressses.


### 4. Rewrite

To access the rewrite tool, go to `Tools > Rewrite`. 

![](./iOS/Assets/ProxyTools/CharlesRewrite.png)

On the left-hand side you can add new rewrite sets, tap on the "Add" button and create a new set of rewrite rules. 

Then tap on your newly created rewrite set and on the top right add the location addrress of the call you want to rewrite to by tapping on "Add" and filling in the details of the API call.

Lastly, set the rewrite rule on the bottom right by tapping on "Add". On the popup specify the type you rewriting for, enter a "match" for Charles to find and a "replace" which Charles will use to replace the matched object. 

![](./iOS/Assets/ProxyTools/CharlesRewriteRule.png)

For example, here we are rewriting the Response status of the API call. We are asking Charles to match for all 200 - 299 response statuses returned on that specific API address and replace it with a `503 Service Unavailable` response instead. 

To apply your rewrite rules make sure your rewrite set, locations and rules are checked, this ensures they are enabled. 

### 5. Network Throttling

To enable global throttling, you can go to `Proxy > Start Throttling`. This will throttle all network requests. 

You can further configure how Charles throttles, you can change the conditions or set sepcific hosts only to throttle by navigating to `Proxy > Throttle Settings` 

![](./iOS/Assets/ProxyTools/CharlesNetworkThrottling.png)

You will see a popup window where you can enable netwrok throttling and enable throttling for selected hosts only by adding those host addresses and checking them. 
Furthermore, you can configure the throttled network environment e.g. configure bandwidth, stability. 
