# iOS Getting Started Guide

## Table of Contents

- [iOS Developer Account](#markdown-header-ios-developer-account)
- [Computer Setup](#markdown-header-computer-setup)
- [Source Control](#markdown-header-source-control)
- [Certificates and Provisioning Profiles](#markdown-header-certificates-and-provisioning-profiles)
- [Understanding the Language](#markdown-header-understand-the-language-frameworks-and-patterns)
- [Coding Conventions](#markdown-header-coding-conventions)
- [Exercise(s)](#markdown-header-exercise)



## iOS Developer Account

1. Using your Outware email address, [create an Apple ID](https://appleid.apple.com/).
1. Approach an iOS Team Lead and ask them to invite your new Apple ID to Outware's Apple Developer account.
    
**Note:** You'll need an iOS developer account before you can do anything else iOS related.

## Computer Setup

1. Download Xcode, the IDE used by many of our iOS developers, from [Apple's developer portal](https://developer.apple.com/download/) or the [Mac App Store](https://itunes.apple.com/au/app/xcode/id497799835?mt=12).
1. Ensure that `Xcode.app` is located in `/Applications`, so that macOS is able to find the developer toolkit.
    * This is also important for the command line tools to work as well.
1. Launch Xcode and install the Command Line Tools when prompted.
1. We also use various QuickLook plug-ins to help with quickly viewing files.
    * [Provisioning](https://docs.google.com/a/outware.com.au/document/d/1oTN-A5zrzyQ--6K7oX175vTdu-f9OkrjJTk2c__dHpU/edit)    
1. **Optional**: Install a run-time visual debugger. Speak to an iOS Team Lead for a license.
    * [Reveal App](https://revealapp.com/download/)
    
## Source Control

1. Download a source control software to your device, we recommend [SourceTree](https://www.sourcetreeapp.com/)

1. Also sign up to/have a look at [Bitbucket](https://bitbucket.org/), you will likely be given access to our repos soon if you havnt already.

## Certificates and Provisioning Profiles

1. In Xcode, request an Apple Certificate.
    * `Xcode -> Preferences -> Accounts -> View Details -> Signing Identities (Create)`
    * **Alternatively**: you can create you Certificate in the [Apple Developer Portal](https://developer.apple.com/account/ios/certificate/development) but it is more involved and less convenient.
    * Ensure that an iOS Team Lead updates the Provisioning Profile to include your new Apple ID.
    * If you don't know who the team leads are then feel free to ask your mentor. 
1. Download the Outware Wildcard iOS Team Provisioning Profile.

## Understand the Language, Frameworks and Patterns

We have a [document](./iOS/001_StartWithSwift.md), which lists some fundamentals that we believe will help you with iOS development. Check it out, and be sure to ask your mentor and fellow developers if there is anything that you are unsure of.

### Coding Conventions

We have some coding conventions for ARQ iOS Developers, have a look at them [here](./CodingConventions.md), these should be followed on all new projects. 

## Exercise

In the coming days, you may be approached by your mentor about working together on a sample application.

The application will likely have you work with fundamental iOS building blocks such as [Tab Bars](https://developer.apple.com/ios/human-interface-guidelines/ui-bars/tab-bars/), [Tables](https://developer.apple.com/ios/human-interface-guidelines/ui-views/tables/) and [Navigation Bars](https://developer.apple.com/ios/human-interface-guidelines/ui-bars/navigation-bars/).

Please feel welcome to ask your mentor and your fellow iOS developers any questions; _most_ of us don't bite.

Welcome to the team!
