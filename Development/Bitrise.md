#  Bitrise


## Introduction

Bitrise is a CI/CD platform. CI/CD stands for Continuous Integration / Continuous Development. 

Think of Bitrise as an automation tool that makes our lives easier by automatically creating the artefacts and distributing them, so our QA team can test them.

These artefacts are `.ipa` files for iOS and `.apk` files for Android.

Without Bitrise, every time you complete a ticket you would need to ensure that all unit tests pass, manually export these artefacts from your computer and send them to our QA team. This would be a slow and very error-prone process.

When the Bitrise pipelines run, they complete all these steps automatically. They run unit tests, ensure that code signing is in order, install dependencies, archive and export the artefacts and upload them to AppCenter or TestFlight.

In case an error occurs, Bitrise also tells us what exactly went wrong so we can correct the mistakes before a faulty artefact is tested by our QA team.


## Getting started

To login to Bitrise, [click here](https://app.bitrise.io/users/auth/saml/dd9982f66bbbd91c/login). Your credentials are the same as for any ARQ or ARQForce account.

Select your project on the right hand side.

![image](Assets/bitrise_select_project.png)


## Project Dashboard

Now you are on the project's dashboard:

![image](Assets/bitrise_project_dashboard.png)

If you need to, you can manually start a new build by clicking the `Start/Schedule a build` button.
For branch, simply add the branch name, without `origin/`. For example, if you'd like to run a build on your feature branch called `feature/my-feature`, just add that.

For workflow select the one the applies to your needs. The following section explains each in detail.

![image](Assets/bitrise_start_build.png)


## Workflows

Workflows are essentially pipelines that consist of multiple steps. The workflows are standardised across our projects, however some steps might be different based on the project's needs.

In general, these are the workflows we are using:  


Workflow name  | Runs automatically | Usage | Artefact outputs
------------- | ------------- | ------------- | -------------
PR Validation  | When a PR is raised on any branch | Archives the project and runs tests | No outputs. Only ensures that project still builds and unit tests are passing
Deploy-qa-build | When code is pushed to `develop` branch | Used to pass on builds to QA for testing | Produces a `.QA` artefact. 
Deploy-dropclient-build | When code is pushed to a `release` branch | Used to pass on release builds to QA for testing | Produces 3 artefacts: `.UAT`, `.Release`, `.store-release` (only on iOS)

### Artefact Outputs explained

Artefact Output name  | Usage 
------------- | ------------- 
`.QA`  | Designed to allow testing of individual cards/tickets by QA.
`.UAT` | The UAT build is designed to provide a release-like codebase to facilitate:<br>1) QA Sanity Testing of the release build prior to sharing with the customer <br>2) User Acceptance Testing by our customers
`.Release` | The Release build is designed to be a release-ready codebase to facilitate: <br>1) QA final sanity testing prior to release <br>2) On Android, this build is used for Play Store submission
`.store-release` (iOS only) | This build is the same as the `.Release`, however it is signed with Production keys to allow release through the App Store.


## View a build

Click on one of the builds on the dashboard to see details about it. You can read the build log and see the steps that Bitrise executed.

If the build was successful it will show up in `green`, if there was an error, it will be in `red`.

Bitrise is pretty good at giving useful error messages that point you in the right direction. If the error is related to your changes (a failing unit test for example) then you can action this in the code base, commit, push and run a new build. 

However, if the error is related to something else, then it's usually the Managed Services Team who take care of it. They have the access rights to edit the workflow.

Once the workflow has successfully finished, you'll see the output appear on AppCenter or TestFlight.

![image](Assets/bitrise_build_number.png)
![image](Assets/bitrise_appcenter_build_number.png)
