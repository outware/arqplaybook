# Xcode Shortcuts

## Table of Contents
 - [Introduction](#markdown-header-introduction)
 - [Basic Shortcuts](#markdown-header-basic-shortcuts)
 - [Editor](#markdown-header-editor)
 - [Cleanup Your Code](#markdown-header-cleanup-your-code)
 - [Breakpoints](#markdown-header-breakpoints)
 - [Helpful for Debugging](#markdown-header-helpful-for-debugging)
 - [References and Resources](#markdown-header-references-and-resources)
 

## Introduction
Some handy shortcuts and hints so that you can use xcode more effectively
This does not include your standard computer shortcuts like `copy` and `paste` etc, which will work as normal within Xcode.

## Basic Shortcuts
Basic Shortcuts
Here’s a list of the most common shortcuts used in Xcode:

- Build
    - ⌘ + B - `( Command + B )`

- Run
    - ⌘ + R - `( Command + R )`

- Test
    - ⌘ + U - `( Command + U )`

- Stop
    - ⌘ + . - `( Command + . )`

- Clean
    - ⌘ + ⇧ + K - `( Command + Shift + K )`

- Clean the build folder
    - ⌘ + ⇧ + ⌥ + K - `( Command + Shift + Option + K )`

- Open a file quickly
    - ⇧ + ⌘ + O - `( Shift + Command + O )`

- Close current tab
    - ⌘ + W - `( Command + W )`

- Close Other Tabs
    - ⌘ + ⌥ + W - `( Command + Option + W )`

- Code completion
    - ⌃ + Space - `( Control + Space )`

- Clean up indenting
    - ⌘ + I - ` ( Command + I )`

- Fix-it All
    - ⌃ + ⌥ + ⌘ + F - `( Control + Option + Command + F )` 

## Editor
Adding a secondary editor
The following shortcut lets you add secondary editor(s). The new editors aren’t reflected on your screen if you’re in focus mode.

- ⌃ + ⌘ + T - `( Control + Command + T )`

By hovering over the “Add New Editor” Button and holding the Option Key, we can toggle the position of the new editor to the right or bottom.

To hide all but the current editor, use the following set of keys:

- ⇧ + ⌃ + ⌘ + ↩ - `( Shift + Control + Command + Enter )` 

- Highlight Editor
    - ⌘ + J - `( Command + J )`
 
Once the current editor gets highlighted you can navigate between the assistant editors with the arrow keys and press Return on the new editor to make it the active one.


- Open and close left panel 
    - ⌘ + 0 - `( Command + 0 )`
    
- Select Tabs within left panel
    - ⌘ + 1-9 - `( Command + 1-9 )`
    
- Open and close right panel
    - ⌘ + ⌥ + 0 = `( Command + Option + 0 )`
    
- Select Tabs within right panel
    - ⌘ + ⌥ + 1-9 - `( Command + Option + 1-9 )`

### Minimap Shortcuts

Xcode 11 gave us minimaps. A much-needed outline view of your code on the right-hand side of the IDE. By hovering over it you can navigate to any part of your code.
Among the important shortcuts, one lets you toggle the minimap view while the other shows an outline of all the properties, methods, classes and blocks of code in the file:

- Toggle Minimaps
    - ⇧ + ⌃ + ⌘ + F - `( Shift + Control + Command + F )`

- Minimap Outline
    - ⇧ + ⌃ + ⌘ - `( Shift + Control + Command )`

### Toggle canvas

If you’re looking to quickly build a prototype in code without letting the live previews distract you until its done, this is a handy shortcut.

- ⌥ + ⌘ + ↩ - `( Option + Command + Enter )`

Resume automatic previews
Often the automatic preview pauses and requires us to manually resume it. Xcode 11 gladly has a shortcut for this.

- ⌥ + ⌘ + P - `( Option + Command + P )`

### Change Schemes

- Choose Scheme
    - ⌃ + O  - `( Control + O )`

- Select Next Scheme
    -  ⌃ + ⌘ + ] - `( Control + Command + ] )`

- Select Previous Scheme
    - ⌃ + ⌘ + [ - `( Control + Command + [ )`

- Edit Scheme  
    - ⌘ + <  - `( Command + < )`

## Cleanup Your Code

Multiple Cursors on Multiple Selections
Often, there’s a need to use multiple cursors to avoid typing/copying the same thing on different lines.
 
 This places multiple cursors on the words and allows us to edit them together.
 
-  ⌥ + ⌘ + E - `( Option + Command + E )`

To select the previous occurrence

- ⇧ + ⌥ + ⌘ + E - `( Shift + Option + Command + E )`
 
Refactor All In Scope
Refactoring is inevitable. That makes editing variables and methods in the scope a crucial tool. 

The following shortcut lets us edit all in scope together at the same time:

- ⌃ + ⌘ + E - `( Control + Command + E )`

Fold and Unfold Methods
When your file sizes are going out of bounds (which ideally they shouldn’t) there’s a handy shortcut that lets you do code folding and collapse all methods/selective methods.
It puts a code ribbon on each enclosing block. Following are the shortcuts for the different cases:

- Fold All
    - ⇧ + ⌥ + ⌘ + ← - `( Shift + Option + Command + Left Arrow )`

- Unfold All 
    - ⇧ + ⌥ + ⌘ + → - `( Shift + Option + Command + Right Arrow )`

- Fold Current Block
    - ⌥ + ⌘ + ← - `( Option + Command + Left Arrow )`
    
- UnFold Current Block
    - ⌥ + ⌘ + → - `( Option + Command + Right Arrow )`



### Reorder Statements

To change the order of statements and to move them over one another the following shortcut is used:

-  ⌘ + ⌥ + ( ] or [ ) - `( Command + Option + Square Brackets )`

### Global Search And Or Replace

Xcode IDE has the ability to do a quick global search and even has the ability to replace the symbols everywhere (handle with care).

- Search in the whole project
    -   ⇧ + ⌘ + F - `( Shift + Command + F )`

- Search & Replace in the whole project
    -   ⇧ + ⌥ + ⌘ + F - `( Shift + Command + Option + F )`


## Breakpoints

- Create a breakpoint at current line
    -  ⌘ + \ - `( Command + \ )`
    
- Create a Symbolic Breakpoint
    - ⌥ + ⌘ + \ - `( Option + Command + \ )`
    
- Deactivate Breakpoints
    - ⌘ + Y - `( Command + Y )`

### When using breakpoints and the debugger

- To pause the debugger 
    - ⌃ + ⌘ + Y - `( Control + Command + Y )`
    
- Continue to the current line 
    - ⌃ + ⌘ + C - `( Control + Command + C )`
    
- Step Over
    - F6
    
- Step Into
    - F7
    
- Step Out
    - F8
    
- Step Over Instruction
    - ⌃ + F6 - `( Control + F6 )`
    
- Step Over Thread
    - ⌃ + ⇧ + F6 - `( Control + Shift + F6 )`
    
- Step Into Instruction
    - ⌃ + F7 - `( Control + F7 )`
    
- Step Into Thread
    - ⌃ + ⇧ + F7 - `( Control + Shift + F7 )`
    
## Helpful for Debugging

Jump to Definition
Since Xcode 9, command+click doesn’t directly take you to the definition. Instead, it shows you a pop-up hint with a list of options. In order to directly jump to the definition without seeing the pop-up use the following shortcut:

- ⌃ + ⌘ + J - `( Control + Command + J )`

Find Call Hierarchy
To quickly find the call hierarchy of the selected symbol, be it a method or an instance, just use the following shortcut. It opens the call hierarchy in the project navigator.

- ⇧ + ⌃ + ⌘ + H - `( Shift + Control + Command + H )`

- Find open file in folder structure
    - ⌘ + ⇧ + J - `( Command + Shift + J )`

## References and Resources
 
https://betterprogramming.pub/13-xcode-shortcuts-to-boost-your-productivity-329c90512309
