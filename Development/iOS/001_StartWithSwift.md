# Learn About Swift

#### Welcome to Swift to get yourself started on learning to use Swift, may we recommend reading;

- [Swift](Concepts/Swift.md)

- [UIKit](Concepts/UIKit.md)

- [Closures](Concepts/Closures.md)

- [Delegates](Concepts/Delegates.md)

- [Design Patterns](Concepts/DesignPatterns.md)

- [View Controllers](Concepts/ViewControllers.md)

- [Extensions](Concepts/Extensions.md)

- [SOLID Principles](Concepts/SOLID.md)

#### Looking to make an API call?

- [External API Call](Concepts/ExternalAPICall.md)

- [Multi-threading](Concepts/Multi-threading.md)

- [Synchronous / Asynchronous](Concepts/SynchronousAsynchronous.md)

- [Combine](Concepts/Combine.md)


#### Looking for Something More Specific?

- [Data Storage](Concepts/DataStorage.md)

- [Audio Playback](Concepts/AudioPlayback.md)

- [User Notifications](Concepts/UserNotifications.md)

- [NSNotificationCenter](Concepts/NSNotificationCenter.md)

- [Video Playback](Concepts/VideoPlayback.md)

- [Contacts](Concepts/Contacts.md)

- [Location and Maps](Concepts/LocationAndMaps.md)

- [Testing](Concepts/Testing.md)

- [Generics](Concepts/Generics.md)



## Full list of Markdowns in Alphabetical Order

[Audio Playback](Concepts/AudioPlayback.md)

[Closures](Concepts/Closures.md)

[Combine](Concepts/Combine.md)

[Contacts](Concepts/Contacts.md)

[Data Storage](Concepts/DataStorage.md)

[Delegates](Concepts/Delegates.md)

[Design Patterns](Concepts/DesignPatterns.md)

[Extensions](Concepts/Extensions.md)

[External API Call](Concepts/ExternalAPICall.md)

[Generics](Concepts/Generics.md)

[Location and Maps](Concepts/LocationAndMaps.md)

[Multi-threading](Concepts/Multi-threading.md)

[NSNotificationCenter](Concepts/NSNotificationCenter.md)

[Properties](Concepts/Properties.md)

[Singletons](Concepts/Singletons.md)

[SOLID Principles](Concepts/SOLID.md)

[Swift](Concepts/Swift.md)

[Synchronous / Asynchronous](Concepts/SynchronousAsynchronous.md)

[Testing](Concepts/Testing.md)

[UIKit](Concepts/UIKit.md)

[User Notifications](Concepts/UserNotifications.md)

[Video Playback](Concepts/VideoPlayback.md)

[View Controllers](Concepts/ViewControllers.md)


