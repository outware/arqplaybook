# Singletons


[Apple Documentation](
https://developer.apple.com/documentation/swift/cocoa_design_patterns/managing_a_shared_resource_using_a_singleton)

## Table of Contents

 - [Introduction](#markdown-header-introduction)
 - [When to Use](#markdown-header-when-to-use)
 - [Benefits](#markdown-header-benefits)
 - [Downsides](#markdown-header-downsides)
 - [Common Singletons](#markdown-header-common-singletons)
 - [Creating your own Singleton](#markdown-header-creating-your-own-singleton)
 - [References and Resources](#markdown-header-references-and-resources)
 
## Introduction
 
A singleton is defined as a shared instance of a class that is globally accessible.

The singleton pattern is more generally referred to as one that **restricts instantiation of a class to one**. 

Normally in Object-Oriented Programming (OOP) languages such as Swift and Objective-C, classes can have more than one instance (that is often their purpose, as they serve as a template for objects with similar structures and/or behaviours to avoid duplicated code). 

Singletons should, as a general rule, be configured so that they only have one instance. This is done via a static property which is actually a self-initialisation (here named 'shared'):

```swift
class Singleton {
    static let sharedInstance = Singleton()
}
```

This is guaranteed to be [lazily](./Properties.md#lazy) initialised only once, even when accessed across multiple threads simultaneously.

However, it is also common to have implementations of 'singletons' where there is not strictly one instance, and/or the singular instances are restricted to sub-classes.

For example, if additional set-up is needed, the result of a closure invocation can instead be assigned to the global constant as below (don't forget the parentheses after the [closure](./Closures.md): 

```swift
class Singleton {
    static let sharedInstance: Singleton = {
        let instance = Singleton()
        // setup code
        return instance
    }()
}
```

Implementations of "singletons" are in reality on a spectrum, where some of the looser implementations can cause real problems for developers and even become 'anti-pattern'.

## When to use

A singleton is like the [Elder Wand](https://www.youtube.com/watch?v=of5oHW9fkd4). It is very powerful, and should only be used in the right hands at the right time - otherwise, it can be equally as destructive.

Use the Singleton pattern when a class in your program should have just a single instance available to all clients; for example, a single database object shared by different parts of the program.

### Benefits

Singletons are a way to share logic between classes, which can make your code more DRY and therefore more readable and maintainable. 

This can make your data or data relationships more manageable, where the data is not overly complex.

In some cases, for example where the class initialisation is very expensive, they can be a more efficient choice in terms of memory.

Singletons are also clearly very convenient. Singletons can be created to provide a unified access point to a resource or service that�s shared across an app, like a network manager to make HTTP requests.

Most apps written for Apple platforms rely on APIs that are singleton based (see 'Common singletons' below).

### Downsides

However, singletons also create problems with code decoupling and testing. 

Singletons should not maintain state internally, or that will lead to situations where an application's functionality can be hard to reason about and test. 

Unfortunately, singletons are not always implemented in a disciplined way.

For that reason, they are a common source of bugs, where state ends up being shared and mutations not propagated properly throughout the system.

This risk increases in large, complex applications, where viewing all reference points at once and understanding how they relate becomes more difficult.

## Common singletons

Either way, iOS developers need to understand singletons, including how to identify them, when it is appropriate to use them and how to implement them optimally.

Apple's core APIs contain singletons that provide the basis for the most common app functionality. From UIScreen to UIApplication to NSBundle, static APIs are everywhere in Foundation, UIKit & AppKit.

Below are just a few examples of singletons (or singleton like objects) used in ARQ's iOS development in Swift:

- [UIApplication.shared](https://developer.apple.com/documentation/uikit/uiapplication)
- [UserDefaults.standard](https://developer.apple.com/documentation/foundation/userdefaults) 
- [FileManager.default](https://developer.apple.com/documentation/foundation/filemanager)
- [URLSession.shared](https://developer.apple.com/documentation/foundation/urlsession)
- [OperationQueue.main](https://developer.apple.com/documentation/foundation/operationqueue)

Note only the UIApplication is a "real" singleton, in the sense that instances of the others can be created without issue.

Objective-C (the language used before Swift, and still present in many applications) has very similarly named singletons:

- [UIApplication.sharedApplication](https://developer.apple.com/documentation/uikit/uiapplication?language=objc)
- [NSUserDefaults.standardUserDefaults](https://developer.apple.com/documentation/foundation/nsuserdefaults) 
- [NSFileManager.defaultManager](https://developer.apple.com/documentation/foundation/nsfilemanager?language=objc)
- [NSURLSession.sharedSession](https://developer.apple.com/documentation/foundation/nsurlsession?language=objc)
- [NSOperationQueue.mainQueue](https://developer.apple.com/documentation/foundation/nsoperationqueue?language=objc)

## Creating your own singleton

You can create your own singleton, but be warned of the above risks!

Create a class with a static constant 'shared' or 'sharedInstance' (or similar) that is an initialisation of the class itself:

```swift
class Dog {
    static let shared = Dog()
}
```

or for additional configuration/ to allow extension while maintaining only one instance of the subclass:

```swift
class Dog {
    static let shared: Dog = {
        let instance = Dog()
        return instance
        }()
}
```

Add an initialiser that is private to prevent direct construction calls with the 'new' operator:

```swift
class Dog {
    static let shared: Dog = {
        let instance = Dog()
        return instance
    }()
    private init(){} // set up
}
```

Finally add some business logic to be executed on the instance:

```swift
class Dog {
    static let shared: Dog = {
        let instance = Dog()
        return instance
    }()
    private init(){} // set up

    func makeNoise(){
        print("woof woof")
    }
}
```

The following code (accessing its method through the static "shared" property) should print "woof woof":

```swift
Dog.shared.makeNoise()
```

The following code (which tries to initialise another instance from outside the class) should produce an error:

```swift
let dog = Dog()
```

## References and resources:
- https://developer.apple.com/documentation/swift/cocoa_design_patterns/managing_a_shared_resource_using_a_singleton
- https://developer.apple.com/library/archive/documentation/General/Conceptual/DevPedia-CocoaCore/ObjectCreation.html#//apple_ref/doc/uid/TP40008195-CH39-SW1
- https://matteomanferdini.com/swift-singleton/
- https://medium.com/@nimjea/singleton-class-in-swift-17eef2d01d88
- https://www.swiftbysundell.com/articles/testing-swift-code-that-uses-system-singletons-in-3-easy-steps/

- [https://refactoring.guru/design-patterns/singleton](https://refactoring.guru/design-patterns/singleton)
