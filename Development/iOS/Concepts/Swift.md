# Swift

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Basic Operators](#markdown-header-basic-operators)
- [Types](#markdown-header-types)
- [Optionals](#markdown-header-optionals)
- [Control Flow](#markdown-header-control-flow)
- [Variables and Properties](#markdown-header-variables-and-properties)
- [Functions and Methods](#markdown-header-functions-and-methods)
- [Value vs References Types](#markdown-header-value-vs-references-types)
- [Other](#markdown-header-other)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

iOS development uses Swift and Objective-C, both of which are compiled, Object-Oriented Programming (OOP) languages.

Swift is the more recent and easier to use language. It was designed from the ground up in order to scale from small apps to large scale applications while also providing the performance, and low level access, needed to write safe modern operating systems. It can be used with both the UIKit and SwiftUI frameworks and has full access to the existing Objective-C API being able to interoperate with Objective-C's runtime.

SwiftUI being a new UI framework was created to enable development across all of Apple's platforms has been created for use with Swift only. Whereas UIKit, along with the other other platform specific kits, support both Objective-C and Swift as they are the older and more mature frameworks.

Recommended Reading:

- [Apple documentation](https://developer.apple.com/swift/)
- [Swift documentation](https://docs.swift.org/swift-book/)

### Protocol-Orientated Programming

While Swift is an Object-Orientated Programming language it supports multiple programming paradigms, however a key language tenant in Swift is Protocol Orientated Programming. Protocol Oriented Programming places emphasis on using Protocols, as opposed to inheritance, to encourage code reuse and to create common functionality across multiple unrelated objects. As this is a more advanced topic we are only providing a high level over here and will cover it in more depth separately, but it's useful know of its existence. 

For the enthusiastic some recommended links are:

- [WWDC Video on Protocol-Oriented Programming in Swift](https://developer.apple.com/videos/play/wwdc2015/408/)
- [RayWenderlich Protocol-Oriented Programming Tutorial](https://www.raywenderlich.com/6742901-protocol-oriented-programming-tutorial-in-swift-5-1-getting-started)

## Basic Operators

Swift supports all the standard, conventional operators found in C like languages, such as C, Java and Javascript. 

An outline of all basic operators can be viewed [here](https://docs.swift.org/swift-book/LanguageGuide/BasicOperators.html).

Arithmetic operators (`+`, `-`, `*`, `/`, `%` and so forth) detect and disallow value overflow, to avoid unexpected results when working with numbers that become larger or smaller than the allowed value range of the type that stores them. 

The assignment operator (`=`) doesn’t return a value, to prevent it from being mistakenly used when the equal to operator (`==`) is intended. 

Swift also provides range operators that aren’t found in C, such as` a..<b` and `a...b`, as a shortcut for expressing a range of values.

## Types

Swift is a strongly typed language, which means the compiler must know exactly what type of object (string, array, dictionary, custom object, etc.) needs to be stored in memory for a particular variable.

With Swift, the compiler can also automatically infer the type based on the objects assigned to a variable. This means a type must be included for properties or objects declared without a value:

```swift
let myVariable: String
```

but type inference can be used for those that have a value with an obvious type:

```swift
let myVariable = "A String"
```

although the type may be included anyway:

```swift
let myVariable: String = "A String"
```    

Note: our preferred style is to use type inference as it makes our code more concise, cleaner and easier to read.

Swift provides its own versions of the fundamental programming types: 

| Type         | Description  |
| ------------ | ------------ |
| **`Int`**    | Integer      |
| **`Double`** | Double Precision Floating Point Number |
| **`Float`**  | Floating Point Number |
| **`Bool`**   | Boolean      |
| **`String`** | Textual data |

and three primary [collection types](https://docs.swift.org/swift-book/LanguageGuide/CollectionTypes.html):

| Type                          | Description             |
| ----------------------------- | ----------------------- |
| **`Array<Element>`** or **`[Type]`** | An array                |
| **`Dictionary<Key, Value>`** or **`[Key: Value]`** | A dictionary, more genrally called a hash map |
| **`Set<Element>`**            | A Set, there is no equivalent shorthand with [ ] for sets |

## Optionals 

By default Swift objects can never be `nil`. This is a language safety feature and the compiler will raise a compile-time error if a variable is ever `nil`.

However, there are cases where `nil` is valid and appropriate. For these situations Swift has an innovative feature known as optionals. An optional may contain `nil`, or it may contain a value.

Swift requires the use of a question mark, `?`, after the type to indicate something an Optional type.

For example:

```swift    
let myVariable: String?
```

means that `myVariable` is an optional String (it may be nil, or it may have a String value).

By making optionals a language level feature it forces developers to consider whether a value will ever be `nil`, and if it is, how to handle those scenarios where it is `nil`. By doing so it removes the need to defensively check nil scenarios except when necessary so that it becomes the exception not the norm.

### Unwrapping Optionals

In order to use an optional variable in your code you will need to unwrap it and there are two common ways of unwrapping optionals:

1. Nil coalescing:

  ```swift
  let myString: String?
  ...
  let newString = myString ?? "" // This will assign a default value of "" if myString is nil
  ```

1. Forced unwrapping:

   ```swift
   let myString: String!
   ...
   let newString = myString // This will crash the app if myString is nil
   ```

Forced unwrapping is not to be used except in very specific scenarios as they will crash the application if a `nil` value is found. It is better to explicitly handle the `nil` scenario than crash the application. 

There are more ways to unwrap an optional that are used in conjuction with control flow, which we cover [below](#markdown-header-control-flow).

### Type Casting

To come back to types for a moment, now that we have discussed optionals we can discuss another feature of types: Type Casting. Type casting is the process of convering an object in Swift of one type to another type. 

There are two ways to do this:

1. the first is safely using an optional cast:

    ```swift
    let poodle = animal as? Dog
    let tabby = animal as? Cat
    ```

1. the second is a forced cast:

    ```swift
    let poodle = animal as! Dog
    let tabby = animal as! Cat
    ```

A forced cast will crash the application if it fails whereas an optional cast will present an optional value that can be unwrapped as covered above.

We prefer optional casting as we can control the failure scenarios without crashing the entire application. 

More info on type casting can be found [here](https://docs.swift.org/swift-book/LanguageGuide/TypeCasting.html).

## Control Flow

Swift provides a variety of control flow statements. 

These include:

- `while` loops to perform a task multiple times

- `for`-`in` loop to iterate over arrays, dictionaries, ranges, strings, and other sequences

- `if`, `guard`, and `switch` statements to execute different branches of code based on certain conditions

- statements such as `break` and `continue` to transfer the flow of execution to another point in your code

Swift’s switch statement is considerably more powerful than its counterpart in many C-like languages. Cases can match many different patterns, including interval matches, tuples, and casts to a specific type. 

Matched values in a switch case can be bound to temporary constants or variables for use within the case’s body, and complex matching conditions can be expressed with a `where` clause for each case.

Detailed information on control flow tools can be found [here](https://docs.swift.org/swift-book/LanguageGuide/ControlFlow.html).

## Variables and Properties

Properties and Variables are declared in the same manner, however properties are declared within and associated with an object.

### Variables

Variables can be local or global. Local variables can only be used within the function in which they are declared, whereas global variables are usually declared at the top of the file and can be accessed or changed in multiple locations and functions.

- Declaring a variable you don't plan on altering, i.e. a constant:

    ```swift
    let variableName = "empty string"
    ```

- Declaring a variable you **DO** plan on altering:

    ```swift
    var variableName = "empty string"
    ```

- Declaring a variable that is initially empty:

    ```swift
    var variableName: String
    ```

### Properties

Properties are a variable that belongs to an object, such as a class, enum or struct. More details on properties can be found [here.](./Properties.md)

## Functions and Methods

See the Swift documentation for an overview of [functions](https://docs.swift.org/swift-book/LanguageGuide/Functions.html), below is an example of how you might declare a basic function that when called `functionName()`, will print "hello world" to the console.

```swift
func functionName() {
    print("hello world")
}
```

[Methods](https://docs.swift.org/swift-book/LanguageGuide/Methods.html) are simply functions inside a particular type. To use a method simply append the method name after the variable name you wish to use the method upon.

```swift
let arrayName = [ 0, 1, 2, 3, 4]
print(arrayName.count)
```
    
The example above first declares an array of numbers. Then on the following line applies the array method of `count` to the array, this is wrapped inside a print statement and will result in `5` being printed to the console, as that is the total number of items within `arrayName`.

Classes, structures, and enumerations can all define instance methods, which encapsulate specific tasks and functionality for working with an instance of a given type. Classes, structures, and enumerations can also define type methods, which are associated with the type itself. 

The fact that structures and enumerations can define methods in Swift is a major difference from many C-like languages.

### Closures

Closures are a special type of function that are introduced with Swift but already present in multiple programming languages, including Javascript, and are an application of lambda calculus the maths world. They are self contained code block and what makes them a closure, and rather unique, are their ability to capture scope from their context, e.g. the parent function that's defining the closure. 

For more details on Closures we recommend reading the official [Swift docs](https://docs.swift.org/swift-book/LanguageGuide/Closures.html).

## Value vs References Types 

Reference types share a **single copy** of their data (passing around a memory reference, or pointer in C-like languages), while value types keep a **unique** copy of their data.

Examples of Swift reference types: `class`

Examples of Swift value types: `enum`, `struct`  

### Reference Types

Reference types are shared instances that can be passed around and referenced by multiple variables. 

```swift
class Dog {
  var wasFed = false
}
    
let dog = Dog()
```

The variable `dog` now points to a location in memory. If we then create a new `puppy` variable and pass `dog` as its value like such:

```swift
let puppy = dog
```
    
`puppy` will now **point to the exact same data** in memory, this is because `dog` itself is a reference to a memory address and puppy now points to the same memory address. 

Therefore, changing one named instance will affect the other instance that is pointing to the same memory reference. 

```swift
puppy.wasFed = true
print(dog.wasFed) // will print true
```

For example, in the code above when the `puppy`'s variable `.wasFed` was changed, we see that printing `dog.wasFed` returns the changed value `true` too.

### Value Types

Value types create a copy of the data, making it unique to the different variables.

Most Swift primitive types e.g. Int, String, Float, Double etc. are value types. 

This is why in the example below, we can expect `a` to equal 42 and `b` to equal 43.

```swift
var a = 42
var b = a
b += 1

a    // 42
b    // 43
```
    
The same is seen in structs, which are value types. 

```swift
struct Cat {
  var wasFed = false
}

var cat = Cat()
var kitty = cat
kitty.wasFed = true

cat.wasFed        // false
kitty.wasFed      // true
```

`kitty` contains a **unique copy** of the value of `cat` instead of a reference. 

Setting `kitty`'s `wasFed` property had no effect on `cat` because they are 2 unique copies.
 
### Mutability 

`var` and `let` function differenctly for reference types and value types. 

In the example above for **reference types**, `dog` and `puppy` instances were defined as constants with `let dog`, `let puppy`, but we were able to change the `.wasFed` property because inside the `Dog` class this property was defined as `var wasFed `. 

This is because, for reference types `let` means the reference to the instance (the object you have created in memory) must remain constant, but you can mutate the instance's properties itself.

```swift
let dog = Dog()
let miniDog = Dog()
dog = miniDog // not allowed, dog has been instantiated as a constant
```

On the other hand, for **value types**, `let` means the instance must remain constant, both it's reference and properties of that instance can never change. 

In the value type example of the `Cat` struct, if we declared kitty with `let` we will be able to change it's properties as such `kitty.wasFed = true`, then our kitty would starve all night!

```swift
let cat = Cat()
cat.wasFed = true // not allowed, cat is a constant, none of it properties can change
    
let cat2 = Cat()
cat = cat2 // not allowed, cat is a constant, cannot change the object it references
```

Even if the property `wasFed` was defined with `var`, if `kitty` was declared with `let` no properties will ever change.

```swift
let kitty = Cat()
kitty.wasFed = true // cannot change properties 

var kitty = Cat() 
kitty.wasFed = true // can change properties
```

### Mutating methods 

The `mutating` keyword is used in structs when a method changes the struct's internal properties (properties are not changed externally like with `person.name = Bob`). 

For example, when we call a method that is part of the struct which changes the struct's properties (look at the `goIcognito()` method below, mutating the `name` variable from within itself), the `mutating` keyword must be used as the function is declared.

```swift
struct Person {
    var name: String

    mutating func goIcognito() {
        name = "Anonymous"
    }
}
```
    
When a struct is created as a constant, properties also cannot be changed (as explained above)

However, when you create a struct (define it with `Struct myStruct {...}` ), Swift does not know whether you will use it with constants (`let`) or variables (`var`).

By default it takes a safe approach, it will not let you write methods that change properties (it defaults to thinking you are going to instantiate it as a constant) unless you specifically request it with the `mutating` keyword. 

```swift
var person = Person(name: "Bob")
person.goIncognito() // this will change person's name to "Bob"
```
    
Once again because the code above changes the property `name`, Swift will only allow that method to be called on a `Person` instance that is declared as `var`.

## Other

### Semi-colons

These are not needed at the end of code lines or blocks. While they can be used, best practice for Swift is to leave them out (yay!).

## References and Resources

- https://developer.apple.com/swift/
- https://docs.swift.org/swift-book/
- https://docs.swift.org/swift-book/LanguageGuide/BasicOperators.html
- https://docs.swift.org/swift-book/LanguageGuide/CollectionTypes.html
- https://docs.swift.org/swift-book/LanguageGuide/ControlFlow.html
- https://www.codecademy.com/learn/learn-swift/modules/learn-swift-hello-world/cheatsheet
- https://www.techrepublic.com/article/apples-swift-programming-language-the-smart-persons-guide/
- https://www.raywenderlich.com/9481-reference-vs-value-types-in-swift
- https://medium.com/good-morning-swift/difference-between-value-type-and-reference-type-in-swift-1f2bd9dd32a7
- https://www.hackingwithswift.com/sixty/7/5/mutating-methods
 

