#  Video Playback

[Apple documentation - VideoPlayer](https://developer.apple.com/documentation/avkit/videoplayer)

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [Play Video from Web](#markdown-header-play-video-from-web)
- [Play Video from Local Assets](#markdown-header-play-video-from-local-assets)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

To play a video within your application you must use `AVKit`. 

This framework creates user interfaces for media playback, complete with transport controls, chapter navigation, picture-in-picture support, and display of subtitles and closed captions.

It will give you access to the tools to make a video play, including `AVPlayerViewController`, a [view controller](./ViewControllers.md) which displays content from a player that users can pause, play, skip forward or backwards and turn sound on and off.


## Play Video from Web

- Create a new project and add a "play" button on the main storyboard.

- In your view controller file `import AVKit`:

```swift
import AVKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
}
```
            

- Create a function `playVideo`. 

- Inside it:

1. Create an instance of `AVPlayer`.

2. Create an instance of `AVPlayerViewController` and pass it as a reference to the player.

3. Modally present the `AVPlayerViewController` and call the `play()` method.

```swift
func playVideo(){
    guard let url = URL(string: "https://storage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4") else { return }

    // Create an AVPlayer, passing it the HTTP Live Streaming URL.
    let player = AVPlayer(url: url)

    // Create a new AVPlayerViewController and pass it a reference to the player.
    let controller = AVPlayerViewController()
    controller.player = player

    // Modally present the player and call the player's play() method when complete.
    present(controller, animated: true) {
            player.play()
    }
}
```
    
- Create an `@IBAction` function for the  `play` button that executes when it is pressed. 

- You can either place all the behaviour above inside this, or you can invoke the `playVideo()` function. The latter allows you to optionally print or log inside the`@IBAction` function to make sure the button itself works:

```swift
@IBAction func playButtonTapped(sender: UIButton) {
    print("video playing")
    playVideo()
}
```
            
## Play Video from Local Assets

First you must add the video into the project.

- To add the video start by clicking on the top most folder of your project

![Project Top Folder](../Assets/Project Top Level Folder.png)

- This will open a menu in the main screen, click on the tab `Build Phases` and find the section called `Copy Bundle Resources`

![Build Phases](../Assets/Build Phases.png)

![Copy Bundle Resources](../Assets/Copy Bundle Resources.png)

- Press the `+` button and a modal menu will appear, click on `Add Other` to find the desired video on your computer.

![Add Resource](../Assets/Add Resource Menu.png)

- Once you have found the video click `open` and in the next menu `finish`. You will have now added the video into the project.

To play a video that you have just added into your Copy Bundle Resources, follow the same steps as you would playing a video.

The only difference is that you should provide a file path instead of a web url. 

To do this:

- Inside the `playVideo()` function, provide a a file path to the `AVPlayer` using `URL(fileURLWithPath: path)` (instead of `URL(string: url)`). 

- The path is accessed and can be assigned to a 'path' variable using `Bundle.main.path(forResource: String, ofType: String)`:

```swift
func playVideo(){
    guard let path = Bundle.main.path(forResource: "TestVideo", ofType: "mp4") else {
        print("video not found")
        return
    }

        // Create an AVPlayer, passing it the path defined above.
    let player = AVPlayer(url: URL(fileURLWithPath: path))

        // Create a new AVPlayerViewController and pass it a reference to the player.
    let controller = AVPlayerViewController()
    controller.player = player

        // Modally present the player and call the player's play() method when complete.
    present(controller, animated: true) {
        player.play()
    }
}
```

NOTE: If the full name is `TestVideo.mp4` only `TestVideo` is passed to `forResource`;  `mp4` is passed to `ofType`. 

If you **do not** provide a string for your file name and instead specify `nil`, it will return the first resource file it finds that matches the remaining criteria.

## References and Resources
- [Apple Developer - AVFoundation](https://developer.apple.com/documentation/avfoundation)
- [Apple Developer - AVKit](https://developer.apple.com/documentation/avkit)
- [Apple Developer - path(forResource:ofType:inDirectory:)](https://developer.apple.com/documentation/foundation/bundle/1409670-path)
