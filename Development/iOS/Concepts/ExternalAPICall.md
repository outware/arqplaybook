# Making External API Calls (HTTP requests)

## Table of Contents

- [What is an External API](#markdown-header-what-is-an-external-api)
- [What is an HTTP Request](#markdown-header-what-is-an-http-request)
- [Making an HTTP Request in iOS](#markdown-header-making-an-http-request-in-ios)
- [Create Data Task](#markdown-header-create-data-task)
- [Handle Data Task Failure/ Success](#markdown-header-handle-data-task-failure-success)
- [Decode Data](#markdown-header-decode-data)
- [Completion Handlers](#markdown-header-completion-handlers)
- [Swift 'Result'](#markdown-header-swift-result)
- [References and Resources](#markdown-header-references-and-resources)

## What is an External API

External APIs allow you to create workflows that send HTTP requests to external web servers, API endpoints, and URLs.

This is usually done to retrieve data that can be used by the app.

This is often used to display something in the UI - for example, a weather app that retrieves up-to-date weather forecasts and displays them for the user.

## What is an HTTP Request

HTTP is an acronym for [Hyptertext Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol).

It is a client-server protocol for fetching resources such as HTML documents. Requests are initiated by the recipient or 'client', usually a web browser:

![HTTP request](../Assets/HTTPRequest.png)

The HTTP request is made by the client, to a named host, which is located on a server. The aim of the request is to access a resource on the server. 

To make the request, the client uses components of a URL (Uniform Resource Locator), which includes the information needed to access the resource.

## Making an HTTP Request in iOS

[Apple documentation - URLSession](https://developer.apple.com/documentation/foundation/urlsession)

In iOS, the simplest way to make an HTTP request is to use `URLSession`'s `.dataTask`, provided as part of Apple's API.

`URLSession` is the manager or coordinator of the requests your application performs. 
 
### Create Data Task

Since URLSession has a shared instance and uses a [singleton](./Singletons.md) pattern, we can use it to download/upload data to endpoints from/to the URLs we have specified, wthout creating a new instance of one.

`URLSession` has a singleton `shared` session (which does not have a configuration object - not customised) which can be used for basic requests, you will then use this shared data session to create multiple tasks if needed. 

A session creates one or more tasks to do the work of fetching, downloading or uploading data.

To access this shared session, you need to call the shared class object `URLSession.shared`. 

You will then need to create a `URLSessionTask` to handle the url request by accessing the [`shared`](https://developer.apple.com/documentation/foundation/urlsession/1409000-shared) singleton session object's method called `dataTask` which returns a `URLSessionDataTask`:

    let task = URLSession.shared.dataTask(with: URL(string: "http://numbersapi.com/random/trivia")!) { data, response, error in
        //...
    }

If you need to customise your `URLSession`, you can do this through `URLSessionConfiguration`, which provides three different configurations: 

1. **default** - similar to a shared session, but you can't onfigure it and assign delegates to obtaine data incrementally `let defaultSession = URLSession(configuration: .default)`
2. **ephemeral** - similar to shared sessions, but don't write caches, cookies or credentials to disk  `let defaultSession = URLSession(configuration: .ephemeral)`
3. **background** - lets you perform uploads and downloads of content in the background while your app is not running `let defaultSession = URLSession(configuration: .background)`

Similar to a shared session, the configured session will then need to create a `URLSessionDataTask`.

To pass in a url, you must use the method `URL(string: "HTTP string goes here")`. To make it more readable, you can assign it to a constant first:

    let url = URL(string: "HTTP string goes here")!
    
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
     //...
    }

`URL(string:)` returns as an optional URL type. 

We force unwrap it for the purpose of this example, but you could also safely unwrap it e.g. using `if let`. 

Make sure to use proper error handling so that the user is not stuck in a loading state if it is a bad url (such as "HTTP string goes here"...!).

### Handle Data Task Failure/ Success

A data task can either fail or succeed:

- Fail ->  `error` will have a value. 
- Succeed ->  `data` and `response` will have a value (`data` contains the data; `response` contains a `URLResponse` object - i.e. an http response code such as a 200 success, or 404 error).

If the data task is successful, you can unwrap `data` (of type `Data` ) and use it in your code. In most cases, HTTP requests will return JSON formatted data.

If the data task fails, you should handle the errors. As a placeholder, you can print or log the errors to the console until you have resolved how to handle them.

The .`dataTask` has an associated `.resume()` method that must be called in order to execute the defined task. 

It doesn't "wait" for a response. Rather, when a response is received it calls the callback for the task. This could be immediately, or it could be later (seconds, minutes etc).

Calling it causes a response to be sent back to the client, and terminates the HTTP request.

Full example below, with real api url:

    let url = URL(string: "http://numbersapi.com/random/trivia")!

    let task = URLSession.shared.dataTask(with: url) { data, response, error in
        if let data = data {
            // handle data
        } else if let error = error {
            print("HTTP Request Failed \(error)")
        }
    }
    
    task.resume()
    
### Decode Data 

In iOS, the JSON formatted data returned in the `data` object needs to be "decoded" before it can be properly used in our application.

It can be decoded into Swift using [`JSONDecoder()`](https://developer.apple.com/documentation/foundation/jsondecoder), available through Apple's API. 

For best practice, this is done in a single "APIHandler" Swift file. 

The ViewModel usually communicates with the APIHandler, instructing it to handle the request and return data for the app to use. 

We can use `Codable` protocol to easily decode and encode data. To decode the data, we decode the data by mapping the JSON data to our model. We also need to make these models conform to the `Codable` protocol so that the we can decode data to this model (e.g. decoding the JSON data below into `User` model).

For example, imagine getting this `user` JSON data from an API: 

```json
[
    {
        "userId": 1, 
        "username": "John",
    },
    {
        "userId": 2,
        "username": "Jane"
    }
]
```
To decode this data we need to create a model which maps this data as such:

```swift
struct User: Encodable {
    var userId: Int
    var username: String
}
```
The variable names used in the model need to be exactly the same as the keys on the json data, to allow the decoder to decode the data and map it to this `User` model.

You can create custom keys using `CodingKeys`. Then to decode the above data, pass it into the `JSONDecoder.decode(_:, from:)` function. 

```swift

do {
    let users = try JSONDecoder().decode([User].self, from: data) // `data` is the data successfully retrieved from a URLSession 
    // use data to updata UI etc.
}
catch {
    // handle error
}
```

In the code above, we wrap the `decode()` function in a do-catch block as it is failable. However if decoding is successful, it will return us an array of users (John and Jane).

It is important to note that data returned can be hierarchical and so might be more complex to decode. 

See these resources for more details and example on mapping out and decoding json data:

- [Understanding Swift Result Type](https://www.hackingwithswift.com/books/ios-swiftui/understanding-swifts-result-type)
- [JSON Parsing in swift explained with code examples](https://www.avanderlee.com/swift/json-parsing-decoding/)
- [JSON parsing in swift](https://betterprogramming.pub/json-parsing-in-swift-2498099b78f)
- [Working with hierarchical data](https://www.hackingwithswift.com/books/ios-swiftui/working-with-hierarchical-codable-data)


### Completion Handlers

"Completion handler" is a common name for a type of escaping [closure](../Closures.md) (which is itself simply a callback function). 

It is basically just a function that gets passed to another function as a parameter, and called when something is complete.

This is useful with [asynchronous](../SynchronousAsynchronous.md) tasks, where there's often no guarantee when it will end (which means you probably want to be notified, so you can make execution of subsequent tasks dependent on it completing and/or returning something).

Because of the asynchronous nature of HTTP requests, you may find yourself using completion handlers.

The completion handler allows us to handle the response of an HTTP request (e.g. updating our UI) whenever the task is complete.

Escaping closures (i.e. completion handlers) must be tagged with `@escaping`. 

This communicates to the program that it may be used outside of the current method, and asks its memory be kept alive until then. 


### Swift 'Result'

You can also use the Swift `Result` data type to handle the result of your HTTP request. It is implemented as an enumerator with two cases (i) success and (ii) failure.

If the HTTP request was unsuccessful, you can return it as a `.failure` case of `Result`. If the HTTP request was successful, pass the data back to the method that invokes `fetchData()` through a `.success` case of `Result`. The return values from the `Result` data type can be handled using a switch statement that handles both the `.success` and the `.failure` case.

In the example below, associated values `let data` and `let error` are set so that data returned from these case can be easily used within the case's logic: 

```swift
func getData() { 
    // viewModel method calling on the APIHandler
    apiHandler.fetchData(with: url, completion: { result in
        switch result {
        case .success(let data):
            DispatchQueue.main.async {
                // Dispatch it to the main queue to handle updates to the UI
                loadingAnimation.stop()
                myLabel.text = data.title // we handle data successfully donwloaded by updating our label UI
            }
        case .failure(let error):
            DispatchQueue.main.async {
                // Dispatch it to the main queue to handle updates to the UI
                loadingAnimation.stop()
                showAlert(with: error) // here we handle the error by showing an alert to users
            }
        }
    })
}

func fetchData(with url: URL, completion: @escaping (Result<MyDataType, Error>) -> Void) {
        
        // perform data task
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error: API call failed due to error - \(error)")
                completion(.failure(error))
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("Error : HTTP Response Code Error")
                completion(.failure(APIError.responseError(code: response.statusCode)))
                return
            }
            guard let data = data else {
                print("Error : No Data")
                completion(.failure(APIError.noData))
                // we call the completion handler here as well so that the loading animation will eventually be dismissed - the UI will respond to a failure
                return
            }
            // when code reaches here, it means data task returned successfully
            do {
                let decodedData = try JSONDecoder().decode(MyDataType.self, from: data)
                // here MyDataType is the type you want returned from this function
                // if you decide to decode in another function then MyDataType will be Data i.e. Result<Data, Error>
                completion(.success(decodedData))
            } 
            catch {
                completion(.failure(APIError.failedToDecodeData))
                return
            }
        }.resume()
}

// A custom error enum 
enum APIError: Error {
        case responseError(code: Int)
        case noData
        case failedToDecodeData
}
```


## References and Resources 
- https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://developer.mozilla.org/en-US/docs/Web/HTTP
- https://developer.apple.com/documentation/foundation/urlsession
- https://developer.apple.com/documentation/foundation/urlsession/1409000-shared
- [Making HTTP requests in Swift](https://cocoacasts.com/networking-fundamentals-how-to-make-an-http-request-in-swift)
- [Using singletons for data fetch in Swift](https://stackoverflow.com/questions/61704109/using-singletons-for-data-fetch-in-swift)
- [Networking in Swift](https://learnappmaking.com/urlsession-swift-networking-how-to/)
- [JSON Parsing in swift explained with code examples](https://www.avanderlee.com/swift/json-parsing-decoding/)
- [JSON parsing in swift](https://betterprogramming.pub/json-parsing-in-swift-2498099b78f)
- [Understanding Swift Result Type](https://www.hackingwithswift.com/books/ios-swiftui/understanding-swifts-result-type)
- [More about Throwing and Error Handling](https://docs.swift.org/swift-book/LanguageGuide/ErrorHandling.html)
