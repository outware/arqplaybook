# Location and maps

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [Core Location](#markdown-header-core-location)
- [Maps](#markdown-header-maps)
- [Indoor Mapping Data Format (IDMF)](#markdown-header-indoor-mapping-data-format-idmf)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

Location-based information consists of: 

- location services, and 
- maps 

Location services are provided by the Core Location framework, which defines interfaces for obtaining information about the user’s location and heading (the direction in which a device is pointing). 

Maps are provided by the Map Kit framework, which supports both the display and annotation of maps similar to those found in the Maps app.

## Core location

[Core Location](https://developer.apple.com/documentation/corelocation) provides services that determine a device’s geographic location, altitude, and orientation, or its position relative to a nearby iBeacon device. The framework gathers data using all available components on the device, including the Wi-Fi, GPS, Bluetooth, magnetometer, barometer, and cellular hardware.

Use instances of [CLLocationManager](https://developer.apple.com/documentation/corelocation/cllocationmanager) class to configure, start, and stop the Core Location services.

Events are received from an associated location manager object with [CLLocationManagerDelegate](https://developer.apple.com/documentation/corelocation/cllocationmanagerdelegate) methods.

Before your app is allowed to use the user's location data, the system requires your app to get the user's authorisation. 

To request authorisation, call either `requestWhenInUseAuthorization()` (only use location services when the app is in use) or `requestAlwaysAuthorization()` (use location services and receive events even when the app is not running).

To prepare your app to call one of the two request authorisation functions above, you will also need to add some keys to your Info.plist file.

In most cases, you will also need to provide a reason as to why your app needs the user’s location in the project's Info.plist file:

As of iOS 13, two properties are required (`NSLocationAlwaysAndWhenInUseUsageDescription` and `NSLocationWhenInUseUsageDescription`). 

To do this, go into the **Info.plist** file and add 2 key names

1. 'Privacy — Location Always and When In Use Usage Description'
2. 'Privacy — Location When In Use Usage Description'

and provide your reason. 

This will be shown to the user with an alert asking for permission to access location (provided the 'request authorisation' method is used - as it should be).

In your file, you will need to include `import CoreLocation` at the top and create the class with an instance of `CLLocationManager` inside. For example, in your ViewController:

```swift
import UIKit
import CoreLocation

class ViewController: UIViewController {
    
        var locationManager: CLLocationManager! 
        
        override func viewDidLoad(){
        super.viewDidLoad()
        locationManager = CLLocationManager()
    }
}
```
    
To check on your app's current authorisation status, call one the `authorizationStatus()` property on `CLLocationManager`.

For the purpose of this example, make the ViewController the delegate by creating an extension of ViewController that conforms to CLLocationManagerDelegate and setting the instance delegate method inside the classes `viewDidLoad` function to `self`:

```swift
extension ViewController: CLLocationManagerDelegate {
    
}  

class ViewController {
    
    var locationManager: CLLocationManager!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self
    }
}
```
    
You can then add in UI components to display and update location.  

Implement the `locationManager(_:didChangeAuthorization:)` method in your location manager delegate to be notified when your app's authorisation status changes.

See this [tutorial](https://levelup.gitconnected.com/core-location-setting-up-core-location-with-uikit-fc7980565b4f) with source code for setting up a simple app showing device location using UIKit and Swift.
    
## Maps

### Mapkit

Use [MapKit](https://developer.apple.com/documentation/mapkit/) to give your app a sense of place with maps and location information. 

You can use the MapKit framework to:

- Embed maps directly into your app’s windows and views.
- Add annotations and overlays to a map for calling out points of interest.
- Provide text completion to make it easy for users to search for a destination or point of interest.

If your app offers transit directions, you can make your directions available to Maps. 

You can also use Maps to supplement the directions that you provide in your app. 

For example, if your app only provides directions for subway travel, you can use Maps to provide walking directions to and from subway stations.

[**Geocoder**](https://developer.apple.com/documentation/mapkitjs/mapkit/geocoder)

Declaration:

```swift
interface mapkit.Geocoder
```

Geocoders convert human-readable addresses to geographic coordinates and vice versa.

The geocoder requires a network connection and handles calls asynchronously as geocoding is done on the server.

Providing context from these sources improves result accuracy/ relevance:

- The user's location in the browser's Geolocation API. The geocoder assigns higher relevancy to results near the user. Note that asking for the user's location requires the user's permission. If this option is enabled, the browser asks the user for permission (some browsers remember this setting for a day). By default, this option is disabled.

- Lookup options. Your website can provide context in the geocoder lookup options as a coordinate or region, so that for example if Australia is the region and the user types 'Brisbane' it will display Queensland's capital city - but if the San Francisco area is, then the nearby City of Brisbane will be displayed instead.

- The user's IP address. Of the three forms of context, this is the least accurate but the only one certain to be present. The user's IP is a granular IP location that is not logged or stored.

### Indoor Mapping Data Format (IDMF)

Use the [Indoor Mapping Data Format](https://developer.apple.com/documentation/mapkit/displaying_an_indoor_map) (IMDF) to show an indoor map with custom overlays and points of interest.

## References and Resources:
- https://developer.apple.com/documentation/corelocation
- https://developer.apple.com/documentation/corelocation/cllocationmanager
- https://developer.apple.com/documentation/corelocation/requesting_authorization_for_location_services
- https://developer.apple.com/documentation/corelocation/choosing_the_location_services_authorization_to_request
- https://developer.apple.com/documentation/corelocation/cllocationmanagerdelegate
- https://developer.apple.com/documentation/mapkit/
- https://developer.apple.com/documentation/mapkitjs/mapkit/geocoder
- https://developer.apple.com/documentation/mapkit/displaying_an_indoor_map
- https://developer.apple.com/maps/
- https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/LocationAwarenessPG/Introduction/Introduction.html#//apple_ref/doc/uid/TP40009497

