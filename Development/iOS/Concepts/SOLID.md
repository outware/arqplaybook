#  SOLID 

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [What is Object-Oriented Programming](#markdown-header-what-is-object-oriented-programming)
- [Single Responsibility Principle](#markdown-header-single-responsibility-principle)
- [Open/Closed Principle](#markdown-header-openclosed-principle)
- [Liskov Substitution Principle](#markdown-header-liskov-substitution-principle)
- [Interface Segregation](#markdown-header-interface-segregation-principle-isp)
- [Dependency Inversion](#markdown-header-dependecy-inversion)
- [References and Resources](#markdown-header-references-and-resources)


## Introduction

SOLID principles represent the five principles of object-oriented programming.

1. [**S**olid Responsibility Principle](#markdown-header-single-responsibility-principle)
2. [**O**pen/Closed Principle](#markdown-header-openclosed-principle)
3. [**L**iskov Substitution Principle](#markdown-header-liskov-substitution-principle) 
4. [**I**nterface Segregation](#markdown-header-interface-segregation-principle-isp )
5. [**D**ependency Inversion](#markdown-header-dependecy-inversion)

## What is Object-Oriented Programming

Object-Oriented Programming (OOP) is a programming model that organises software design around data/objects rather than functions/logic.

The internal implementation of an object's methods is invisible to the user, the object abstracts state changes.

> An object, in OOP is an abstract data type created during development, it can include multiple properties and methods. 

e.g. creating a User object with `name` as a property and `sign` as a method. 

## Single Responsibility Principle

This first principle states that
> Every module should have only one responsibility and one reason to change

This means that classes and/or objects should only be used for one thing. This principle will help you keep your classes lean and clean as possible.

An example of this is to create a separate class that handles external API calls, and even a separate class that takes on the responsibility of a router (provides the right URL for the needed API call), instead of making multiple API calls in different viewModel's.

Take a look at this `Handler` class below which handles 

- Making the network call
- Parsing the response
- Saving data into the database

```swift
class Handler { 
    
    func handle() {        
        let data = requestDataToAPI()        
        let array = parse(data: data)        
        saveToDatabase(array: array)    
    }   
  
    private func requestDataToAPI() -> Data {        
        // Network request and wait the response    
    } 
    
    private func parseResponse(data: Data) -> [String] {        
        // Parse the network response into array  
    }  
   
    private func saveToDatabase(array: [String]) {        
        // Save parsed response into database   
    }
}
```
You can split down these responsibilities into little classes like so

```swift
class Handler { 
    let apiHandler: APIHandler 
    let responseHandler: ResponseHandler 
    let databaseHandler: DatabaseHandler 
    
    init(apiHandler: APIHandler, responseHandler: ResponseHandler, databaseHandler: DatabaseHandler) { 
        self.apiHandler = apiHandler 
        self.responseHandler = responseHandler 
        self.dbHandler = dbHandler 
    } 
    func handle() { 
        let data = apiHandler.requestData() 
        let array = responseHandler.parse(data: data)
        databaseHandler.saveToDatabase(array)
    } 
} 

class APIHandler { 
    func requestData() -> Data { 
        // Network request and wait the response 
    } 
}
class ResponseHandler { 
    func parse(data: Data) -> [String] { 
        // Parse the network response into array 
    } 
}
class DatabaseHandler { 
    func saveToDatabase(array: [String]) { 
        // Save parsed response into database
    } 
}
```

In this modified implementation of the Handler class, each class `APIHandler`, `ResponseHandler` and `DatabaseHandler` have a focused and specialised role.

Following this principle will make your code easier to understand, easily extendable and reduces the number of bugs. 
Furthermore, if you need to make changes to a class, it is easier to do it for classes with a single responsibility.

## Open/Closed Principle

The second principle states that 
> Classes, modules and functions should be open for **extensions** but closed for modification

Let's break it down 

- "Open for extension" - you should be able to extend or change functionality of classes easily (as long as it relates to their role)
- "Closed for modification" - you must extend a class without changing its base implementation (what is done by the object to meet its responsibility) 

This allows you to add new functionality without changing the existing code. This will prevent situations in which a change to one of your classes also requires you to adapt all depending classes.

Take for example this `Logger` class which will iterate over `cars` and print details of each car

```swift
class Car {
    let name: String
    let color: String
    
    init(name: String, color: String) {
        self.name = name
        self.color = color
    }
    
   func printDetails() -> String {
        return "I have \(self.color) color \(self.name)."
    }
}

class Logger {
    func printData() {
        let cars = [ Car(name: "BMW", color: "Red"),
                     Car(name: "Audi", color: "Black")]
         cars.forEach { car in
             print(car.printDetails())
         }
     }
}
```

What if you want to add the possibility of printing the details of a new class, e.g. `Bicycle`, `Airplane`? With the code above we would need to change the implementation of `printData` everytime we want to log a new class.

```swift
class Logger {
    func printCarData() {
        let cars = [... array of car objects]
         cars.forEach { car in
             print(car.printDetails())
         }
     }
     
     func printBicycleData() {
        let bikes = [ ... array of bicycle objects]
        bikes.forEach { bike in
             print(bike.printDetails())
         }
    }
}
```
This is a modification and so will break the open/closed principle.

Another solution would be to modify the `Car` class to maybe be a `Vehicle` class

```
class Vehicle {
    let type: String 
    let name: String
    
    init(type: String, name: String) {
        self.type = type
        self.name = name
    }
    
    func printDetails() -> String {
        return "I have a \(self.name) \(self.type)."
    }
}
```
But this would once again require us to change the class to suit our needs - modification, breaking the open/closed principle.

Instead, we can solve this by creating a new protocol called `Printable`. 
We can then make our classes conform to this protocol and create a custom implementation of `printDetails()`.  

```swift
protocol Printable {
    func printDetails() -> String
}

class Car: Printable { 
    // same variables and init as above
    
    func printDetails() -> String {
        return "I have \(self.color) color \(self.name)."
    }
}

class Bike: Printable {
    // similar variables and init methof as Car above
    
    func printDetails() -> String {
        // custom implementation of printDetails function
        // Adding this function to our class allows us to conform to the Printable protocol
        return "I have \(self.name) bike of color \(self.color)."
    }
}

We can then use this Printable protocol as a type in our `Logger` class. Here we can have an array of `Printable` type data e.g. cars and bikes
and call on their respective custom printData() methods. 

class Logger {
    func printData() {
        let vehicles: [Printable] = [Car(name: "BMW", color: "Red"),
                            Bike(name: "Honda CBR", color: "Black")]
        vehicles.forEach { vehicle in
            print(vehicle.printDetails())
        }
    }
}
```

This makes our code easily expandable without modifying it. 

## Liskov Substitution Principle 

The third principle states that 
> Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program

If you replace one object with another that is a subclass, this replacement should not break the affected part.

This principle helps you use inheritance properly, a subclass should be able to replace its superclass without breaking the application or functionality of your code. 
For this to happen, all subclasses must behave in the same way the parent class does. 

Let us look at an example of code that breaks the Liskov Substitution Principle (LSP). 

```swift
class Rectangle {
    var width: Int
    var height: Int

    init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }

    func area() -> Int {
        return width * height
    }
}

class Square: Rectangle {
    override var width: Int {
        didSet {
            super.height = width
        }
    }

    override var height: Int {
        didSet {
            super.width = height
        }
    }
}
```

In the code snippit above, making `Square` a subclass of `Rectangle` breaks LSP because the square is changing the behaviour of Rectangle.
A rectangle should be able to independently set it's own `height` and `width`, unlike `Square` who is implementing a different behaviour compared to its parent `Rectangle` by always matching it's `height` and `width` when either is set. 

When this code is run: 

```swift
let square = Square(width: 10, height: 10)

let rectangle: Rectangle = square

rectangle.height = 7
rectangle.width = 5

print(rectangle.area()) 
```

While we expect the `rectangle`'s area to return `35` (7 x 5), we will get `25` (5 x 5). 
This is because on the line `let rectangle: Rectangle = square` we created a the rectangle which references `Square`. 
The result of this is that we have an object that should be a rectangle but is handled as a `Square` where sides (width and height) must be equal. 

Here, LSP is broken, because replacing the superclass `Rectangle` with a subclass `Square` breaks the functionality of the code, we are unable to use a subclass type without different behaviours.

How can we fix this code to respect the LSP? A solution would be to implement it differently and use a protocol that could be used for all types that are shapes. 

```swift
protocol Shape {
    func area() -> Int
}

class Rectangle: Shape {
    var width: Int
    var height: Int

    init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }

    func area() -> Int {
        return width * height
    }
}

class Square: Shape {
    var edge: Int

    init(edge: Int) {
        self.edge = edge
    }

    func area() -> Int {
        return edge * edge
    }
}
```

The `Shape` protocol allows us to customise the `area()` function, which each class that conforms to `Shape` must provide a custom implementation for.

In summary, if a class could have different behaviours from its parent class, it may be good to create and apply protocols for these different behaviours and making those classes instead conform and implement the speicifed variables/ methods through these protocols. 

## Interface Segregation Principle (ISP)

The fourth principle states that 
> Clients should not be forced to implement interfaces they don't use

Instead of having one fat interface (a protocol with a lot defined in it), it is better to have many small interfaces based on groups of methods.

This principle helps us avoid: 

- Large, packed interfaces
- Forcing classes to implement methods they can't or don't need 
- Polluting protocols with a lot of methods

These smaller protocols are known as "role protocols", which declare one or more methods for a specific behaviour. And so clients (classes) will only need to implement those role protocols whose methods are relevant to them. 

There are many examples of Swift APIs that follow this rule. A popular one would be `Codable`.
Codable is segregated into two smaller protocols `Decodable` and `Encodable`, if you only need one, then simply implement one of the protocols.
However if you do need to both decode and encode the data, then using `Codable` is preferred. 

Take for an example this fat `Machine` protocol.

```swift
protocol Machine {
    func convert(document: Document) -> PDF?
    func convert(document: Document) -> UIImage?
    func fax(document: Document)
}
```

If we have two classes `FaxMachine` and `Phone` implementing this protocol:

```swift
class FaxMachine: Machine {
    func convert(document: Document) -> PDF? {
        return nil // This is because a fax machine cannot do that
    }

    func convert(document: Document) -> UIImage? {
        return nil // This is because a fax machine cannot do that
    }

    func fax(document: Document) {
        // Implementation of the fax here
    }
}

class Phone: Machine {
    func convert(document: Document) -> PDF? {
        return PDF(document: document)
    }

    func convert(document: Document) -> UIImage? {
        return UIImage()
    }

    func fax(document: Document) { 
        return nil //empty because phones cannot fax documents
    }
}
```

As you can see above `FaxMachine` is forced to implement the two `convert(document:)` functions, despite not being able to use it, because fax machines cannot convert documents into PDFs or Images.
On the other hand, `Phone` is forces to implement the `fax()` function.

It might be better to have thinner grouped protocols. By having the the 2 `convert` functions inside one protocol called `Convertable` and the `fax` function is a `Faxable` protocol. 

## Dependency Inversion

The fifth principle states that 
> Depend upon abstractions not concretions

This principle encourages the use of protocols instead of using a concrete classes to connect different parts of your application. 

The other way around is also true, abstractions (your protocols) should not depend on details. Instead, your protocols should be like a contract, summing up the relationship of classes implementing it. 

Take a look at this code example below 

```swift
class FileSystemManager {
    func save(string: String) {
        // Open a file
        // Save the string in this file
        // Close the file
   }
}
class Handler {
    let fileManager = FileSystemManager()
    func handle(string: String) {
        fileManager.save(string: string)
    }
}
```

This code snippet shows that the `Handler` class is dependent on `FileSystemManager`. 
These two classes will be tightly coupled, although we may decide to use database instead of a file system manager. 
Tightly coupling these two classes makes it hard to reuse the `Handler` class. 

We can solve this problem by removing this dependency and using a protocol which we will call `Storage`.
This way, the `Handler` class can use this abstract protocol `Storage` wihtout caring what kind of storage is actually used, decoupling its dependency with any specific type of storage.
Any storage type e.g. file system manager, database, cloud can be used with `Handler` as long as it conforms to the `Storage` protocol. 

e.g. we can have several different ways of storing the data, but all classes are of the `Storage` type. 

```swift
class FileSystemManager: Storage {
    // implementation 
}

class DatabaseManager: Storage {
     // implementation
}

class CloudStorageManager: Storage {
    // implementation
}
```

The `Handler` class then does not need to worry about the system of storage being used, as long as it is of type `Storage`, making it more reusable. 

```swift 
class Handler {
    let storageManager: Storage
    
    init(storageManager: Storage) {
        self.storageManager = storageManager 
    }
}
```
We can initialise the Handler class with different types of storage. 

```swift 
let fileManager = FileSystemManager()

// create a handler with a file system storage type
Handler(storageManager: fileManager)

let databaseManager = DatabaseManager()

// create a handler with a database storage type
Handler(storageManager: databaseManager)


let cloudManager = CloudStorageManager()

// create a handler with a cloud storage type
Handler(storageManager: cloudManager)
```

The benefit of aligning to the dependency inversion principle is that software modules are decoupled, making them independent. 
This means we can change or replace one module without impacting dependent modules.
 
This makes it easy for the modules to be:

- reused
- tested independently by mocking dependencies (creating a mocked version of dependencies, e.g. creating a mocked core data stack to test the app's core data module)
- easily updatable and replaceable

## References and Resources

- [SOLID Principles in Swift](https://medium.com/@nishant.kumbhare4/solid-principles-in-swift-73b505d3c63f)
- [SOLID Principles for iOS Apps](https://www.raywenderlich.com/21503974-solid-principles-for-ios-apps)
- [Open/Closed Principle in Swift](https://medium.com/movile-tech/open-closed-principle-in-swift-6d666270953d)
- [Liskov Substitution Principle in Swift](https://medium.com/movile-tech/liskov-substitution-principle-96f15559e363)







