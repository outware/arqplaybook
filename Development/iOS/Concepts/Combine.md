# Combine

[Combine](https://developer.apple.com/documentation/combine) provides a Swift API for processing events, or changes in values over time. 

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Combine and MVVM](#markdown-header-combine-and-mvvm)
- [Publisher / Subscriber](#markdown-header-publisher-subscriber )
- [Connecting Publishers / Subscriber ](#markdown-header-connecting-a-publisher-to-a-subscriber)
- [Change the Output Type with Operators](#markdown-header-change-the-output-type-with-operators)
- [Customise Publishers with Operators](#markdown-header-customise-publishers-with-operators)
- [Chaining Publishers with Operators](#markdown-header-chaining-publishers-with-operators)
- ['flatMap'](#markdown-header-flatmap)
- ['switchToLatest'](#markdown-header-switchtolatest)
- [Cancel Publishing when Desired](#markdown-header-cancel-publishing-when-desired)
- [Example - Weather App](#markdown-header-example-weather-app)
- [Option #1 - Add City by Typing in Textfield](#markdown-header-option-1-add-city-by-typing-in-textfield)
- [Option #2 - Filter by Typing City in Textfield](#markdown-header-option-2-filter-by-typing-city-in-textfield)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

Combine is a reactive framework, which means that parts of the code (Subscribers/Observers) react upon emissions or receipt of data coming to other parts of the code (Publishers/Observables). 

For example, a Publisher can be created and fed with values coming from API calls, generated values or UI interactions. A Subscriber can be created and subscribe to new values coming from those Publishers. The code is considered to be reactive as it reacts to all new values coming through, and values coming through can be processed as per the business logic requirements.

### Combine and MVVM
The concept of a reactive framework doesn't conflict with MVVM architecture. It can still be used with Combine, in both UIKit and SwiftUI projects.
Combine with UIKit:
https://betterprogramming.pub/uikit-mvvm-combine-912c80c02262

Combine with SwiftUI:
https://iosapptemplates.com/blog/swiftui/mvvm-combine-swiftui

There are other third-party reactive frameworks such as CocoaReact, RxSwift and ReactiveSwift which are already widely used in iOS development. However, Combine is Apple's native reactive framework - this makes it more stable, less error prone and the best of all, it doesn't need to be introduced as a dependency, you just need to import Combine to your class.

Combine relies heavily on [Higher Order Functions](https://www.appcoda.com/higher-order-functions-swift/) -or could be called operators- which with it you can do [Publishers chaining](https://swiftwithmajid.com/2021/05/04/chaining-publishers-with-combine-framework-in-swift/).

Rather than implementing multiple delegate callbacks or completion handler closures, you can create a single processing chain for a given event source. Each part of the chain is a Combine operator that performs a distinct action on the elements received from the previous step.

For more about this see <a href="#Chaining-Publishers-with-Operators">Chaining Publishers with Operators</a>

## Publisher / Subscriber 

Combine's reactive framework uses a `Publisher` and `Subscriber` model. The Publishers essentially broadcast changes, and the Subscriber (which is subscribed to a Publisher) receives those changes and makes updates based on them. 

### What are they?

Combine declares Publishers to expose values that can change over time, and Subscribers to receive those values from the publishers.

- The `Publisher` protocol declares a type that can deliver a sequence of values over time. Publishers have operators to act on the values received from upstream publishers and republish them.

- At the end of a chain of publishers, a `Subscriber` acts on elements as it receives them. Publishers only emit values when explicitly requested to do so by Subscribers. This puts your Subscriber code in control of how fast it receives events from the publishers it’s connected to.

Note that the following Foundation types expose their functionality through publishers, including `Timer`, `NotificationCenter` and `URLSession`. 

Combine also provides a built-in publisher for any property that’s compliant with Key-Value Observing.

### Connecting a Publisher to a Subscriber

To receive the text field’s notifications with Combine, access the default instance of NotificationCenter and call its publisher(for:object:) method. This call takes the notification name and source object that you want notifications from, and returns a publisher that produces notification elements.

```swift
let pub = NotificationCenter.default
    .publisher(for: NSControl.textDidChangeNotification, object: filterField)
```

You use a Subscriber to receive elements from the publisher. The subscriber defines an associated type, Input, to declare the type that it receives. The publisher also defines a type, Output, to declare what it produces. The publisher and subscriber both define a type, Failure, to indicate the kind of error they produce or receive. To connect a subscriber to a producer, the Output must match the Input, and the Failure types must also match.

Combine provides two built-in subscribers, which automatically match the output and failure types of their attached publisher:

`sink(receiveCompletion:receiveValue:)` takes two closures. The first closure executes when it receives Subscribers.Completion, which is an enumeration that indicates whether the publisher finished normally or failed with an error. The second closure executes when it receives an element from the publisher.

`assign(to:on:)` immediately assigns every element it receives to a property of a given object, using a key path to indicate the property.

For example, you can use the sink subscriber to log when the publisher completes, and each time it receives an element:

```swift
let sub = NotificationCenter.default
    .publisher(for: NSControl.textDidChangeNotification, object: filterField)
    .sink(receiveCompletion: { print ($0) },
          receiveValue: { print ($0) })
```

Both the `sink(receiveCompletion:receiveValue:)` and `assign(to:on:)` subscribers request an unlimited number of elements from their publishers. To control the rate at which you receive elements, create your own subscriber by implementing the Subscriber protocol.

### Change the Output Type with Operators

The sink subscriber in the previous section performs all its work in the `receiveValue` closure. This could be burdensome if it needs to perform a lot of custom work with received elements or maintain state between invocations. The advantage of Combine comes from combining operators to customize event delivery.

For example, `NotificationCenter.Publisher.Output` isn’t a convenient type to receive in the callback if all you need is the text field’s string value. Since a publisher’s output is essentially a sequence of elements over time, Combine offers sequence-modifying operators like `map(_:)`, `flatMap(maxPublishers:_:)`, and `reduce(_:_:)`. The behavior of these operators is similar to their equivalents in the Swift standard library.

To change the output type of the publisher, you add a `map(_:)` operator whose closure returns a different type. In this case, you can get the notification’s object as an NSTextField, and then get the field’s stringValue.

```swift
let sub = NotificationCenter.default
    .publisher(for: NSControl.textDidChangeNotification, object: filterField)
    .map( { ($0.object as! NSTextField).stringValue } )
    .sink(receiveCompletion: { print ($0) },
          receiveValue: { print ($0) })
```

After the publisher chain produces the type you want, replace `sink(receiveCompletion:receiveValue:)` with `assign(to:on:)`. The following example takes the strings it receives from the publisher chain and assigns them to the filterString of a custom view model object:

```swift
let sub = NotificationCenter.default
    .publisher(for: NSControl.textDidChangeNotification, object: filterField)
    .map( { ($0.object as! NSTextField).stringValue } )
    .assign(to: \MyViewModel.filterString, on: myViewModel)
```

### Customise Publishers with Operators

You can extend the Publisher instance with an operator that performs actions that you’d otherwise need to code manually. Here are three ways you could use operators to improve this event-processing chain:

Rather than updating the view model with any string typed into the text field, you could use the `filter(_:)` operator to ignore input under a certain length or to reject non-alphanumeric characters.

If the filtering operation is expensive — for example, if it’s querying a large database — you might want to wait for the user to stop typing. For this, the `debounce(for:scheduler:options:)` operator lets you set a minimum period of time that must elapse before a publisher emits an event. The RunLoop class provides conveniences for specifying the time delay in seconds or milliseconds.

If the results update the UI, you can deliver callbacks to the main thread by calling the `receive(on:options:)` method. By specifying the `Scheduler` instance provided by the `RunLoop` class as the first parameter, you tell Combine to call your subscriber on the main run loop.

The resulting publisher declaration follows:

```swift
let sub = NotificationCenter.default
    .publisher(for: NSControl.textDidChangeNotification, object: filterField)
    .map( { ($0.object as! NSTextField).stringValue } )
    .filter( { $0.unicodeScalars.allSatisfy({CharacterSet.alphanumerics.contains($0)}) } )
    .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
    .receive(on: RunLoop.main)
    .assign(to:\MyViewModel.filterString, on: myViewModel)
```

### Chaining Publishers with Operators

Combine facilitates the handling of asynchronous, concurrent code by providing a way to "chain" asynchronous tasks. The two main operators are `flatMap` and `switchToLatest`.

#### `flatMap`

`Flatmap` chains two different publishers, passing the results of the first to the second a bit like passing a baton.

In the `SearchViewModel` class initialiser, it is used to pass the value of the first ("query") publisher, and create a second "search" publisher using the query:

```
final class GithubService {
    private let session: URLSession
    private let decoder: JSONDecoder

    init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }

    func searchPublisher(matching query: String) -> AnyPublisher<[Repo], Error> {
        // some code here
    }
}

final class SearchViewModel: ObservableObject {
    @Published var query: String = ""
    @Published private(set) var repos: [Repo] = []

    private let service = GithubService()

    init() {
        $query
        // flatMap operator
            .flatMap { 
                self.service.searchPublisher(matching: $0)
                    .replaceError(with: []) 
            }
            .receive(on: DispatchQueue.main)
            .assign(to: &$repos)
    }
}
```
The `assign` operator is used at the end of the `SearchViewModel` initialiser to save the result of the search operation in its `repos` property.

Add `debounce` before the `flatMap` operator to block the chain for a time interval, such as if you are delaying making the API request until the user has had some time to type something in a linked UI text field:

```
final class SearchViewModel: ObservableObject {
    @Published var query: String = ""
    @Published private(set) var repos: [Repo] = []

    private let service = GithubService()

    init() {
        $query
            // debounce operator
            .debounce(for: 0.5, scheduler: DispatchQueue.main)
            // flatMap operator
            .flatMap { 
                self.service.searchPublisher(matching: $0)
                    .replaceError(with: []) 
            }
            .receive(on: DispatchQueue.main)
            .assign(to: &$repos)
    }
}
```

#### `switchToLatest`

This facilitates the use of only the latest result - for example, if the second network request of two returns earlier than the first. It wouldn't make sense for the UI to update with the second request, then update to the first request (which is older) later. This could happen if only using `flatMap`.

The `switchToLatest` operator is designed to work with a publisher that emits other publishers. Use `map` to transform the publisher into a publisher that emits "search" publishers, then use `switchToLatest` to flatten the "stream" of publishers and deliver only the results from the latest one:

```
final class SearchViewModel: ObservableObject {
    @Published var query: String = ""
    @Published private(set) var repos: [Repo] = []

    private let service = GithubService()

    init() {
        $query
            // debounce operator
            .debounce(for: 0.5, scheduler: DispatchQueue.main)
            // map operator instead of flatMap
            .map { 
                self.service.searchPublisher(matching: $0)
                    .replaceError(with: [])
            }
            // switchToLatest operator
            .switchToLatest()
            .receive(on: DispatchQueue.main)
            .assign(to: &$repos)
    }
}
```

For more information see ['Chaining publishers with Combine in Swift'](https://swiftwithmajid.com/2021/05/04/chaining-publishers-with-combine-framework-in-swift/)

### Cancel Publishing when Desired

A publisher continues to emit elements until it completes normally or fails. If you no longer want to subscribe to the publisher, you can cancel the subscription. The subscriber types created by `sink(receiveCompletion:receiveValue:)` and `assign(to:on:)` both implement the Cancellable protocol, which provides a `cancel()` method:

```swift
sub?.cancel()
```
    
If you create a custom Subscriber, the publisher sends a Subscription object when you first subscribe to it. Store this subscription, and then call its `cancel()` method when you want to cancel publishing. When you create a custom subscriber, you should implement the `Cancellable` protocol, and have your `cancel()` implementation forward the call to the stored subscription.

### Example - Weather App 

Let's say we have a Weather app and you need to get the forecast by calling an external API that needs a URL with a city name. 

#### Option #1 - Add City by Typing in Textfield

You want the user to type the city into an alert textfield so they can choose it.

This will involve asynchronous code because of 
(1) capturing the textfield value as part of the completion for the alert action 
(2) the HTTP request. This may be done by using multiple delegate callbacks or nested completion handler closures to capture and pass on the value of the textfield and then the data from the HTTP request. Both are used widely in Swift and iOS, however there can be downsides including code readability and maintainability.

Using Combine, you could simply subscribe to updates from a text field’s publisher, and have business logic in place to use that text whenever it is updated to perform URL requests. A second publisher can process the HTTP request's response. The value produced could be used by a subscriber to update some data displayed on the UI.

#### Option #2 - Filter by Typing City in Textfield

Consider an app that needs to filter a table or collection view based on the contents of a text field. 

In AppKit, each keystroke in the text field produces a Notification that you can subscribe to with Combine. 

After receiving the notification, you can use operators to change the content and timing of event delivery, and use the final result to update your app’s User Interface.

## References and resources:
- https://developer.apple.com/documentation/combine
- https://developer.apple.com/documentation/combine/receiving-and-handling-events-with-combine
- https://github.com/ReactiveX/RxSwift
- https://navdeepsinghh.medium.com/marble-diagrams-an-overview-e04462c75175
- https://rxmarbles.com/#combineLatest
- https://www.raywenderlich.com/books/combine-asynchronous-programming-with-swift/v2.0
- https://www.raywenderlich.com/9461083-ios-concurrency-with-gcd-and-operations


## Nerds Only:
- [Functional reactive programming](https://en.wikipedia.org/wiki/Functional_reactive_programming)
- [Reactive programming](https://en.wikipedia.org/wiki/Reactive_programming)
- [Functional programming](https://en.wikipedia.org/wiki/Functional_programming)
- [Combine vs RxSwift cheat sheet](https://medium.com/gett-engineering/rxswift-to-apples-combine-cheat-sheet-e9ce32b14c5b)
- [Combine vs RxSwift performance comparision](https://quickbirdstudios.com/blog/combine-vs-rxswift/)
- [Using Combine - by Joseph Heck](https://heckj.github.io/swiftui-notes/#aboutthisbook) free online book
- [mathematical knowledge behind Functional Reactive programming](https://github.com/ReactiveX/RxSwift/blob/main/Documentation/MathBehindRx.md)
