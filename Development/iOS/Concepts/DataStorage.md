# Storage

This page will go through two different types of storage - `CoreData` and `UserDefaults`, which are often used within iOS applications.

## Table of contents
- [UserDefaults](#markdown-header-userdefaults)
- [CoreData](#markdown-header-coredata)
- [Using a Data Manager](#markdown-header-using-a-datamanager)
- [Fetching from CoreData](#markdown-header-fetching-from-coredata)
- [Saving to CoreData](#markdown-header-saving-to-coredata)
- [Deleting CoreData Objects](#markdown-header-deleting-coredata-objects)
- [Editing CoreData Objects](#markdown-header-editing-coredata-objects)
- [Versioning and Migration](#markdown-header-versioning-and-migration)
- [Predicates](#markdown-header-predicates)
- [Fetched Properties](#markdown-header-fetched-properties)
- [Reference and Resources](#markdown-header-references-and-resources)

## When to use which?

Use `UserDefaults` to store simple and small data, typically flags. 
For example, to store whether the user is logged in or not, which will determine whether the Login 
screen will be shown to the user. 
You should not be using `UserDefaults` to store large amounts of Data.

On the other hand, Core Data is a fully-fledged persistent framework that revolves 
around creating entities (similar to other databases, like SQLite).

## UserDefaults
[UserDefaults documention](https://developer.apple.com/documentation/foundation/userdefaults)

All iOS apps have a built in data dictionary that stores small amounts of user settings 
for as long as the app is installed. This system, called `UserDefaults` can save 
integers, booleans, strings, arrays, dictionaries, dates and more, but you should be careful 
not to save too much data because it will slow the launch of your app.

Firstly, create an instance of the shared `UserDefaults` object within your project and assign it to an instance variable.

```swift
let defaults = UserDefaults.standard
```

Then set values within it using a key.

```swift
defaults.set(true, forKey: "userLoggedIn")
```

Later on, when you want to access these values simply use the same `defaults` variable you have used above.

```swift
let loggedIn = defaults.bool(forKey: "userLoggedIn")
```

Make sure you have used the same key you set initially to pull the data out of `UserDefaults`. 
Also specify the **data type** of what you are extracting from `UserDefaults`. 
Here we are extracting a boolean value of whether the user is logged in or not. 

If we were to extract a integer it would look like 

```swift
let intExample = defaults.integer(forKey: "key")
```

## CoreData

Core data is used to store application data be it for future or offline use.
Core Data abstracts the details of mapping your objects to a store, 
making it easy to save data from Swift without administering a database directly.

The easiest way to set up Core Data would be to check the "Use Core Data" checkbox 
as you are creating the new project in Xcode. 

If you have not done this, you can follow [these steps](https://welcm.uk/blog/adding-core-data-to-an-existing-project-in-xcode-10-swift-4) to get Core Data setup on an existing project.

The four classes in the Core Data Stack

1. `NSManagedObjectModel` - represents each object type in your app’s data model, the properties they can have, and the relationships between them

1. `NSPersistentStore` - reads and writes data to whichever storage method you’ve decided to use, in our case here, Core Data

1. `NSPersistentStoreCoordinator` - is the bridge between the managed object model and the persistent store. It’s responsible for using the model and the persistent stores to do most of the hard work

1. `NSManagedObjectContext` - where you do all of the work with your Core Data objects, cannot exist without an associated context


To create your entities follow these steps

1. Open your `.xcdatamodeled` file
1. Click the "Add Entity" button on the bottom
1. And simply add attributes by clicking on the '+' button on your entity


To create custom managed object subclasses, go to Editor ▸ Create NSManagedObject Subclass… and choose your project, ensuring all your entities are selected.

### Using a DataManager

 Data Manager is a class that will be in charge of the CoreData Stack in your application. 
 Keeping the implementation of this manager simple, this a good place for [singletons](./Singletons.md) to be used. 
 Here you could create only one instance of the `CoreDataManager`. To use the CoreDataManager, 
 classes would need to call on this instance using `CoreDataManager.shared`. Read the markdown on 
 [singletons](./Singletons.md) to find out more. 

```swift
import CoreData

final class CoreDataManager {
    static let shared = CoreDataManager()
    
    private init() {
        // any further configurations can be done here
    }
}
```

```swift 
private let coreDataManager = CoreDataManager.shared
```
 
### Fetching from CoreData
 
 Do this within a do-catch block since this method throws (is failable, can lead to errors).

```swift
do {
    let objects = try context.fetch(Entity.fetchRequest())
}
catch {
    // handle errors
}
```
 
 This fetches all objects from Core Data.

 To fetch specific objects take a look at the code snippets [here](https://www.advancedswift.com/fetch-requests-core-data-swift/#fetch-all-objects-of-one-entity).
 
### Saving to CoreData 

To save objects/changes onto CoreData's persistent store, you will need to have an instance of the CoreData's context (of type `NSManagedObjectContext`).
To create an instance of this context, call on the CoreDataManager's `.persistentContainer.viewContext` method.

```swift    
private let coreDataManager = CoreDataManager()
private let context = coreDataManager.persistentContainer.viewContext
```

Once we have context, we can save data through CoreData's `.save()` method. However this method throws, and so we need to perform it within a
do-try-catch block. 

```swift
do {
    context.save()
}
catch {
    // handle errors
}
```

### Deleting CoreData Objects
 
 To edit objects, edit the fetched objects directly and save this to context. 
 To delete objects, use CoreData's `context.delete()` method and save this to context. 
 Once this is done make sure you do `context.save()`(within a try-catch block). Context here is the context of your Core Data Stack, 
 this line of code will save change made to the entity object.
 
 Deleting CoreData Object - Here the object deleted on `context()` is a `NSManagedObjectContext` which is associated with your CoreData database, 
 once an object has been deleted from the context, call `context.save()` to write these changes permanently to the persistent store.

```swift 
context.delete(user)
    
do {
    try context.save()
}
catch {
    // Handle Error
}
```

### Editing CoreData Objects

To edit CoreData objects, update properties of your fetched objects as needed. 
For example, if you fetched a user from CoreData through a fetch request, 
you can update this user's age as show below. 
Once you have finished updating you objects, be sure to call `context.save()` to write these
changes permanently in your CoreData store. 

```swift
// update properties as needed
user.age = 33

// To save new entity updates to the persistent store,
// call save on the context
do {
    try context.save()
}
catch {
    // Handle Error
}
```

### Versioning and Migration

Version and Migration happens when you need to make changes to the data model.

Versioning refers to the different versions of your CoreData stack because you need to make changes to the entity models. 
For example, you have a transport app that now needs to be updated because of the new light-rail transport system. 
Your application's CoreData stack now will need a light-rail entity and this will create a new version of CoreData stack, to do so you
will need to go through the migration process. 

Migrations happen in three steps:

1. Core Data copies over all the objects from one data store to the next.
1. Core Data connects and relates all the objects according to the relationship mapping.
1. Enforce any data validations in the destination model (CoreData disables destination model validations during the data copy)

Migration can happen in two ways [lightweight migration](https://medium.com/@maddy.lucky4u/swift-4-core-data-part-5-core-data-migration-3fc32483a5f2) or manual migration. However in most cases lightweight migration will do the trick.

### Predicates

[Predicate documentation](https://developer.apple.com/documentation/foundation/nspredicate)

 A predicate is a filter. By specifying the criteria you want to match, CoreData will ensure 
 that only matching objects get returned.

```swift
let fetchRequest = NSFetchRequest(entityName: "Entity")

let filter = "I fixed a bug in Swift"
fetchRequest.predicate = NSPredicate(format: "message == %@", filter)

do {
  let filteredData = try context.fetch(fetchRequest)
}
catch {
  // do some error handling
}
```

First create a `NSFetchRequest` object and simply fill its predicate attribute.

The predicate above with the symbol `%@` is Objective-C for "place the contents of the variable here". 
Here in this example we are putting the value of the variable `filter` in place of `%@`. 
Filtering core data for our entity objects that are like the filter sentence.


Another good operator to know is the `CONTAINS[c]` which is similar to ==


```swift
NSPredicate(format: "message CONTAINS[c] 'fix'")
```

This allows us to filter for all strings that have the word 'fix' in it. [c] represents being case-insensitive.

Other operators that are good to know include

- BEGINSWITH = similar to contains, and matches text at the start of the string
- NOT = flips the match around

### Fetched properties
 Fetched properties are properties that return an array value from a predicate. It is a query that evaluates to an array of results. There is great example of this being used [here](https://www.advancedswift.com/fetched-property-core-data/).
 

### References and Resources
 
 - [UserDefaults documention- https://developer.apple.com/documentation/foundation/userdefaults](https://developer.apple.com/documentation/foundation/userdefaults)
 
 - [Fetched Property Core Data- https://www.advancedswift.com/fetched-property-core-data/](https://www.advancedswift.com/fetched-property-core-data/)
 
 - [Predicate documentation- https://developer.apple.com/documentation/foundation/nspredicate](https://developer.apple.com/documentation/foundation/nspredicate)
 
 - [Lightweight migration- https://medium.com/@maddy.lucky4u/swift-4-core-data-part-5-core-data-migration-3fc32483a5f2](https://medium.com/@maddy.lucky4u/swift-4-core-data-part-5-core-data-migration-3fc32483a5f2)
 
 - [Fetch all Objects- https://www.advancedswift.com/fetch-requests-core-data-swift/#fetch-all-objects-of-one-entity](https://www.advancedswift.com/fetch-requests-core-data-swift/#fetch-all-objects-of-one-entity)
 
 - [Adding core data to an existing project- https://welcm.uk/blog/adding-core-data-to-an-existing-project-in-xcode-10-swift-4](https://welcm.uk/blog/adding-core-data-to-an-existing-project-in-xcode-10-swift-4)
