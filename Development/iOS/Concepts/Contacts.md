# Contacts / Address Book

## Table of Contents

- [CNContacts](#markdown-header-cncontacts)
- [Creating a New Contact](#markdown-header-creating-a-new-contact)
- [Modifying a Contact](#markdown-header-modifying-a-contact)
- [Contact properties with multiple values](#markdown-header-contact-properties-with-multiple-values)
- [ABAddressBook](#markdown-header-abaddressbook)
- [References and Resources](#markdown-header-references-and-resources)

## CNContacts

[Contacts documentation](https://developer.apple.com/documentation/contacts)

The Contacts framework provides Swift and Objective-C API to access the user’s contact information. 

Because most apps read contact information without making any changes, this framework is optimized for thread-safe, read-only usage. 

The contact class CNContact is a thread-safe, immutable value object (cannot be changed) of contact properties, such as the contact’s name, image, or phone numbers. The contact class is like NSDictionary in that it has a mutable subclass `CNMutableContact` you can use to modify contact properties. 

### Creating a New Contact

See example below of creating a new contact using CNMutableContact:

```swift
import UIKit
import Contacts

// Create a new contact
let newContact = CNMutableContact()
newContact.givenName = "John"
newContact.familyName = "Appleseed"

// Save the contact
let store = CNContactStore()
let saveRequest = CNSaveRequest()
saveRequest.add(newContact, toContainerWithIdentifier: nil)

do {
    try store.execute(saveRequest)
} catch {
    print("Saving contact failed, error: \(error)")
    // Handle the error
}
```

1. Create a new `CNMutableContact()` object
2. Set the `CNMutableContact`'s properties e.g. `givenName`, `familyName`, `phoneNumbers` (view the different properties to set [here](https://developer.apple.com/documentation/contacts/cnmutablecontact))
3. Create an instance of `CNContactStore()` which represents the user's contacts store database, you will need to use this to fetch information and save changes back to it
4. Create a `CNSaveRequest()` object that collects the changes you want to save to the user's contacts database. (You will need to create a new `CNSaveRequest` object for each save operation you want to make.)
5. Then when saving the contact, you call on CNSaveRequest's `.add(_ CNMutableContact, toContainerWithIdentifier: String?)` 
6. Finally, to execute requests you have added to `CNSaveRequest` call on `CSContactStore`'s `execute()` method and pass in your `saveRequest` as an argument (since this method throws, wrap it in a do-catch block)

### Modifying a Contact

Modifying the previously created contact above: 

```swift
// Update the home email address for John Appleseed
guard let mutableContact = contact.mutableCopy() as? CNMutableContact else { return }

let newEmail = CNLabeledValue(label: CNLabelHome, value: "john@example.com" as NSString)
mutableContact.emailAddresses.append(newEmail)

let saveRequest = CNSaveRequest()
saveRequest.update(mutableContact)
do {
    try store.execute(saveRequest)
} catch {
    print("Saving contact failed, error: \(error)")
    // Handle the error
}
```
    
In the code example above, since we are modifying/ updating the contact object, we call on CNSaveRequest's `.update` method. 

### Contact properties with multiple values

For contact properties that can have multiple values, such as phone numbers or email addresses, an array of `CNLabeledValue` objects is used, as in the modified `newEmail` variable in the example above.
`CNLabeledValue` are just labels that describe the property, for example, in modifying the email in the code above, we change the "Home" email, we could also add an email with a "Work" label by
using `CNLabelWork`. For a full list of different `CNLabeledValue` look [here](https://developer.apple.com/documentation/contacts/cnlabeledvalue).

For more information see Apple developer reference below.

## ABAddressBook

AddressBook was deprecated in iOS 9 and replaced with the new and improved Contacts framework (see above).

The ABAddressBook class provides a programming interface to the Address Book—a centralised database used by multiple applications to store contact and other personal information about people. 

```swift
class ABAddressBook : NSObject
```

## References and resources:
- https://developer.apple.com/documentation/contacts
- https://developer.apple.com/documentation/addressbook/abaddressbook
- https://developer.apple.com/documentation/contacts/cnlabeledvalue
- https://developer.apple.com/documentation/contacts/cncontactstore
- https://developer.apple.com/documentation/contacts/cnsaverequest
