#  Extension
 [Extension Swift Documentation](https://docs.swift.org/swift-book/LanguageGuide/Extensions.html)
 
## Table of Contents
 
 - [Introduction](#markdown-header-introduction)
 - [Uses for Extensions](#markdown-header-uses-for-extensions)
 - [Use of 'where' in extensions](#markdown-header-use-of-where-in-extensions)
 - [Resources](#markdown-header-resources)

## Introduction

Sometimes we want to add functionality to an existing class, structure, enumeration, or protocol. 

This can be done in any file by adding `extension className { add functionality here } `.

They are especially useful if you do not have access to an existing class. For example, 
adding a helper function to the String class (which we do not have access too) to check if a string contains only digits.

```swift
extension String {
    var containsOnlyDigits: Bool {
    let notDigits = NSCharacterSet.decimalDigits.inverted
    return rangeOfCharacter(from: notDigits, options: String.CompareOptions.literal, range: nil) == nil
}
```

To use this helper function we have on the String class we just need to call it as a function on a String type variable.

```swift
let a1 = "12345".containsOnlyDigits // true
let a2 = "a12345".containsOnlyDigits // false
```

**Note** Extensions can add functionality but they cannot override existing functionality or change the fundamental structure.
For example if you wanted to add a property to a class you must do this via subclassing, not via an extension.

E.g. If you had an Airplane class,

```swift
class Airplane {
    var altitude: Double = 0
}
```

You cannot change it's fundamental structure using extensions like so (by adding a new variable - changing structure).

```swift
extension Airplane {
    var speed: Int = 0
} 
```
    
Instead for things like this use subclassing, which creates a new class with the properties of it's parent class with some changes.

## Uses for Extensions

- Separating and grouping code

- Provide new intialisers (i.e constructor functions)

- Using [protocol](./Delegates.md) conformance

- Extensions, computed properties and helpers

- Making an existing type conform to a protocol (to group the protocol's conformance functions together)

- Protocol extensions (to add default implementations for the protocol)

## Use of 'where' in extensions

The key word `where` can be used when creating an extension so that the extension only applies in certain circumstances.

The below code is an example of how you might use `where` in creating an extension for the type Arrays.

```swift
extension Array where Element == String {
    func reverseAll() -> [String] {
        return self.map { String($0.reversed()) }
    }
}

let names = ["Arthur", "Ford", "Zaphod", "Trillian"]
print(names.reverseAll())
// Output: ["ruhtrA", "droF", "dohpaZ", "naillirT"]
```

Here an extension is defined for the type `Array` but it will only apply to arrays where the elements inside the arrays are of type `String`.

In this code `Element` is a generic placeholder, which is a "stand in" for the actual type of the Array elements.

Since Arrays can be of various types (e.g. [Int], [String], [Double]), by using the `Element` placeholder and the `where` keyword, the 
compiler checks the array type to ensure it is of type `String` ([String]) before allowing it to use the `.reverseAll()` method. 

## Resources

- [Understanding Swift Extensions](https://learnappmaking.com/swift-extensions-how-to/)

- [Using 'where' in Extensions](https://learnappmaking.com/swift-where-how-to/#generics-protocols-extensions-and-where)

- [Uses of Extensions in Swift](https://www.swiftbysundell.com/articles/the-power-of-extensions-in-swift/)

