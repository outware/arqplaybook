#  Generics

## Table of contents
- [Introduction](#markdown-header-introduction)
- [Generic Function Parameters](#markdown-header-generic-function-parameters)
- [Using Generics to Define New Sets of Types](#markdown-header-using-generics-to-define-new-sets-of-types)
- [Type Constraints](#markdown-header-type-constraints)
- [Type Constraints on Extensions](#markdown-header-type-constraints-on-extensions)
- [Associated Types](#markdown-header-associated-types)
- [References and Resources](#markdown-header-references-and-resources)


## Introduction

Generics enable you to write flexible, reusable functions and types that can work with any type, subject to the requirements that you define.

Without probably knowing it, you have in fact been using generics, as it is built into the Swift standard library. 

For example, Swift's Array and Dictionary are both generic collections, when you create an array that holds `Int` values you are indicating that the generic type placeholder of the array as an Int. Initialising an `Int` array like this `Array<Int>` is similar to `[Int]`.

## Generic Function Parameters

It helps us write **flexible and reusable code**. 

For example, if you needed a function that swaps two values, but the values passed into this function can be anything, Ints, Doubles, Strings etc. 

Instead of writing 3 different functions 

    func swapTwoInts(a: Int, b: Int) {}
    func swaptTwoDoubles(a: Double, b: Double) {}
    func swapTwoStrings(a: String, b: String) {} 
    
It is much better to write a single function that accepts a flexible type as long as both parameters are of the same type (e.g. 2 Ints passed in, instead of one Int and another Double)

    func swapValues(a: T, b: T) {
        let temporary = a
        a = b 
        b = temporary
    }

In the code above `T` (which stands for Type) is the typical placeholder symbol used for generics. The function arguments `(a: T, b: T)` is saying that it will accept any type as long as both `a` and `b` are of the same type.

To indicate different types being expected by the function, you can use different placeholders e.g. `(a: T, b: U)` this will for example allow us to pass in something like `swapped(a: 1, b: "Hello")`.

    func swapped<T, U>(a: T, b: U) -> (U, T) {
      return (b, a)
    }


It is important to note here that the placeholder does not neccesarily need to be `T`, it can be anything that make sense. 


## Using Generics to Define New Sets of Types

We can have a model that works by varying the value of types. A model which enforces a relationship where one range of possible values determine another range of values.

Consider owning a zoo with different animals as highlighted below.

    class Lion {}
    class Bear {}
    class Monkey {}
    class Dolphin {}
    
You can have another `ZooKeeper` model whose relationship is strongly connected to the animals in your zoo, where one range of possible values (`Animal`) determine another range of values (`ZooKeeper`) 

For the example above you can define a generic type of zoo keepers like so

    class ZooKeeper<Animal> {}

    var lionKeeper = ZooKeeper<Lion>()
    
    var generalKeeper = ZooKeeper() // ERROR! “generic parameter ‘Animal’ could not be inferred”
     
So a generic is a lot like a recipe for making real concrete types. Simply trying to instantiate `ZooKeeper()` will lead to an error.

The `Animal` placeholder inside `<>` is the **type parameter** that specifies the kind of animal the employee is taking care of. 

Once again the placeholder does not have to be `Animal`, it can be `T` or `U` etc., but it is good to use well-defined names when placeholders have a well-defined role like `Animal` here. 

These resulting concrete types are called **specialisations** of the generic type. `ZooKeeper<Bear>`, `ZooKeeper<Monkey>`

## Type Constraints 

Continuing the example above, the identifier `Animal` is a generic type parameter named placeholder (which allows any type to replace it), but when calling on this, you can simply pass any argument. 

You can pass in a String/ Int/ Double. However, when it comes to a zoo, `ZooKeeper<Int>` does not make sense!

Because of this, you will need a feature that lets you restrict what kind of types are allowed in the type parameter, for this we use type constraints.

    class ZooKeeper<Animal: ZooAnimal>
    
Here the constraint `: ZooAnimal` requires that the type assigned to the generic `Animal` parameter must be either
 
- a subclass of `ZooAnimal`
- or implement a `ZooAnimal` protocol

```
    protocol ZooAnimal {
        var name: String { get }
    }

    class Lion: ZooAnimal {}
    class Bear: ZooAnimal {}
    class Monkey: ZooAnimal {}
    class Dolphin: ZooAnimal {}

    let lionKeeper = ZooKeeper<Lion>()
    let faultyKeeper = ZooKeeper<String>() // ERROR!
```
    
### Type Constraints on Extensions 

Type constraints on extensions can also be powerful. For example, when you want all `Lion` arrays to support a `roar()` method, you can use an extension on the `Array` class
and specify that this method only be available for a specific `Element` (Lions).

    extension Array where Element: Lion {
        func roar() {
            forEach { print("(\$0.name) says ROAR! ") }
        }
    }

You can even specify that a type should conform to a protocol only if it meets certain constraints. 

In the example below, you are stating that every `Array` is `Roarable` if it's elements are `Roarable`

    protocol Roarable {
      func roar()
    }

    extension Lion: Roarable { 
      func roar() {
        print("\(self.name) says roar!")
      }
    }

    extension Array: Roarable where Element: Roarable {
      func roar() {
        forEach { $0.roar() }
      }
    } 

## Associated Types

Associated types work closely with protocols. They are fundamentally an associated type of a protocol. 

An associated type is a placeholder name of a type to use until the protocol is adopted and the exact type is specified. 

For example, if we want a protocol for collections we could create one like so 

    protocol Collection {
        associatedtype Item 
        var count: Int { get }
        var valueAtIndex(index: Int) -> Item { get }
        func append(_ item: Item)
    }

By defining an associated type `Item` with the keyword `associatedtype` we can make use of this protocol's logic for different types. 


    class StringCollection: Collection {
        
        var array: [String] = []
        var count: Int { array.count }
        
        func append(_ item: String) {
            guard !array.contains(item) else { return }
            array.append(item.uppercased())
        }
        
        subscript(index: Int) -> String {
            return array[index]
        }
    }

In the code example above where the `Collection` protocol is used, the `associatedType` is defined as a String, and so everywhere else on the protocol which was marked with the associatedType `Item` becomes a String.

### Limitations of `associatedtype`

While using associated types, you may encounter the following error:

> protocol can only be used as a generic constraint because it has Self or associated type requirements

This cryptic error arises out of a situation similar to the following: Say we have a bunch of different types which implement the `Collection` protocol, e.g: `StringCollection`, `IntCollection`, `WhopdeewhoozitCollection`, and we want to collect these collections in an array, call it `var collectionCollection: [Collection]`. Now, what does `collectionCollection.first.first` return? As we can see, we can't know (at compile time) what the value should be, because we have only constrained the variable via the protocol, it is only defined at runtime. This wouldn't be a problem if we didn't have the associated type becuase all of the functions and variables in the protocol relate to the same types and the compiler would be happy.

"But what if I just want to access the count?", you ask. "That's always going to be an int", you blather. Then you should define a protocol containing only the `count` method and define your variable instead via that protocol, e.g:

```
protocol Countable {
    var count: Int { get }
}

protocol Collection: Countable {
    associatedtype Item 
    ...
}

var collectionCollection: [Countable]
```

The key takeaway is that you can't define a variable as a protocol type if that protocol has associated type constraints.


## References and Resources

- [Generics Guide](https://docs.swift.org/swift-book/LanguageGuide/Generics.html)
- [What Are Generics](https://www.hackingwithswift.com/example-code/language/what-are-generics)
- [Understanding Associated Types](https://www.avanderlee.com/swift/associated-types-protocols/)
- [A Getting Started Generics Tutorial](https://www.raywenderlich.com/3535703-swift-generics-tutorial-getting-started)
