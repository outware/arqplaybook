# View Controllers and Views

## Table of Contents
- [Overview](#markdown-header-overview)
- [Is it a View or a Controller?!](#markdown-header-is-it-a-view-or-a-controller)
- [Does it act like a web page or a router?](#markdown-header-does-it-act-like-a-web-page-or-a-router)
- [Types of View Controllers](#markdown-header-types-of-view-controllers)
- [Container View Controllers](#markdown-header-container-view-controllers)
- [Content View Controllers](#markdown-header-content-view-controllers)
- [Views and Sub-Views](#markdown-header-views-and-sub-views)
- [Lifecycle](#markdown-header-lifecycle)
- [Referenences and Resources](#markdown-header-references-and-resources)

## Overview

[Apple documentation - ViewControllers](https://developer.apple.com/documentation/uikit/view_controllers)

An iOS application User Interface (UI) has 'scenes' (see your app's automatically generated AppDelegate and SceneDelegate files). Each scene contains a window object and one or more view objects, which in turn have associated view controllers. 

The window is an invisible container for the rest of your UI and a top-level container for your views, and it routes events to them through the view controller:

![UIWindowScene](https://docs-assets.developer.apple.com/published/84548660ef/view-controller-architecture~dark@2x.png)

A view controller manages a single set of views and keeps the information in them up-to-date. The views are managed in a view hierarchy, the root view of which is stored in the `view` property of the `UIViewController` class. The root view acts primarily as a container for the rest of the view hierarchy. The size and position of the root view is determined by the object that owns it, which is either a parent view controller or the app’s window. 

The view controller that is owned by the window is the app’s **root view controller**. It specifies the window's initial set of views and is sized to fill the window. The root view controller can be set using the window property [`rootViewController`](https://developer.apple.com/documentation/uikit/uiwindow/1621581-rootviewcontroller):

```swift
// UIWindow instance variable 'window' declared
let window = UIWindow(windowScene: windowScene)

// MainViewController (e.g. your custom subclass of UIViewController) instantiated with MainViewModel
let mainViewController = MainViewController.instantiate(with: MainViewModel())

// set window rootViewController property to 'mainViewController'
window.rootViewController = mainViewController
```

[UIKit](./UIKit.md) provides standard view controllers for navigation and managing specific type of contents (see 'Different Types of View Controllers' below). 

View controllers are not a feature of SwiftUI, although they can work together - e.g. you can place UIKit views and view controllers inside SwiftUI views and vice versa. See this Apple tutorial ['Interfacing with UIKit'](https://developer.apple.com/tutorials/swiftui/interfacing-with-uikit).

### Is it a View or a Controller?!

It is a ViewController - which is a Controller intrinsically tied to a View.

In the Model-View-Controller software engineering [design pattern](./DesignPatterns.md), Controllers are used to communicate between the View (or UI) and the Model (data models). In iOS, these Controllers are based on view controllers and are tied closely to their views. This can make them hard to test because they contain implementation of the view as well as business logic. 

In the Model-View-ViewModel (MVVM) [design pattern](./DesignPatterns.md), ViewControllers are treated as if they are part of the View. They interact directly with the View (or views), such as Storyboard or XIB, to receive events and update UI. Business logic is moved to the ViewModel, which is easier to test.

### Does it act like a web page or a router?

Kind of...

In iOS, the View (UI) is created through the ViewController. Every app therefore needs to have at least one ViewController in order to have any interaction with the user. 

A single ViewController has one "root view", which may itself contain many sub-views. Apps tend to have more than one ViewController, as most have more than one View. 

A user will interact with the app, triggering the creation of different ViewControllers. Behind the scenes, these are stored in an array, and the user can generally see the most recent View/ViewController. 

While this is different to pages and routers in web applications, it looks similar to a user - i.e. a new page or 'view' will appear after some action is taken or event is triggered, such as a user clicking a 'log-in' button inside a log-in View, and after successful user / password authentication being taken to the main View of the app.

### Types of View Controllers ###
[Apple documentation - UIViewController](https://developer.apple.com/documentation/uikit/uiviewcontroller)

The most common types of view controllers in UIKit are known as 'Containers' or 'Content' view controllers.

(1) Containers:

These can be used to "navigate" between different content view controllers.  

 - `UINavigationController`
 - `UITabBarController`
 - `UISplitViewController`

(2) Content:

These own all their views and manage interactions with them. New Xcode projects automatically generate a content view controller which you can modify. You can also add more. 

 - `UIViewController`
 - `UITableViewController`
 - `UICollectionViewController`


## View Controllers

### Container View Controllers

A container view controller manages presentation of content associated with view controllers it owns
('child view controllers'). A child's view can be presented as-is or with views owned by the container view controller.


[**UINavigationViewController**](https://developer.apple.com/documentation/uikit/uinavigationcontroller)

- Top navigation bar with cookie-crumb / back option.
- Manages child view controllers (the views the user can navigate to) using an ordered array called **navigation stack**.

For example, `UINavigationController` has a [`pushViewController`](https://developer.apple.com/documentation/uikit/uinavigationcontroller/1621887-pushviewcontroller) method that will display the latest ViewController in the array:

    navigation = UINavigationController()
    navigation.pushViewController(secondViewController, animated: true) 

This can be removed using the [`popViewController`](https://developer.apple.com/documentation/uikit/uinavigationcontroller/1621886-popviewcontroller) method, or another view controller pushed in front of it.

____________________________________________________________________________________________
SIDEBAR: 
Alternatively, the `UIViewController` has a [present](https://developer.apple.com/documentation/uikit/uiviewcontroller/1621380-present) method that, when called on the instance, modally presents that instance within the current container:

    viewController.present(secondViewController, animated: true, completion: nil) 

If presented, the view controller can modally disappear by dismissing itself using the [`dismiss`](https://developer.apple.com/documentation/uikit/uiviewcontroller/1621505-dismiss) method:

    viewController.dismiss(animated: true, completion: nil) 

While `UINavigationController`'s method `pushViewController` puts the view controller in the **navigation stack**, the `UIViewController`'s `present` simply replaces the root view controller and its views (modal presentation style however may intentionally leave some of the previous view visible. See [Present a View Controller Modally](https://developer.apple.com/documentation/uikit/view_controllers/showing_and_hiding_view_controllers)).
_______________________________________________________________________________________


There are two ways of setting up your **navigation view controller**:

-  **Through interface builder** : In the storyboard that you want to appear first, make sure ViewController is currently selected in Xcode as the 'initial' ViewController. Select that ViewController in the storyboard and then choose editor(top bar) > embed in > navigation controller (the navigationController will now be 'initial' ViewController)

-  **Programmatically** : Within the SceneDelegate's `scene()` function, initialise the `UINavigationController` with the ViewController you want as the first View. Note that this parameter is also called "rootViewController". Set the instance of `UINavigationController` as the window's `rootViewController`:

```swift
guard let windowScene = scene as? UIWindowScene else { return }

let window = UIWindow(windowScene: windowScene)
let mainViewController = MainViewController.instantiate(with: MainViewModel())
let navigationViewController = UINavigationController(rootViewController: mainViewController)
window.rootViewController = navigationViewController
self.window = window
window.makeKeyAndVisible()
```

[**UITabBarController**](https://developer.apple.com/documentation/uikit/uitabbarcontroller)

- Displays tabs at the bottom of the window to "navigate" between views. 
- Typically organises 3-5 view controllers.
- Can be combined with `UINavigationViewController`.

[See this tutorial on TabBar](https://medium.com/@vialyx/ios-dev-course-uitabbarcontroller-5df45ca78188)


[**UISplitViewController**](https://developer.apple.com/documentation/uikit/uisplitviewcontroller)

- Allows two view controllers to be placed side by side: Master view (smaller) and Detail view (bigger). 
- Mostly used on larger screens e.g. iPad.

[See this tutorial on SplitView](https://www.raywenderlich.com/4613809-uisplitviewcontroller-tutorial-getting-started)


### Content View Controllers

[**UIViewController**](https://developer.apple.com/documentation/uikit/uiviewcontroller)

The `UIViewController` class defines the shared behaviour that is common to all view controllers. 

Instances of `UIViewController` are rarely created directly. Instead, subclass it and customise it to manage view hierarchy:

```swift
class ViewController: UIViewController {
    override func viewDidLoad() {
         super.viewDidLoad()
     }
    // custom code
} 
```

A custom `UIViewController` subclass can also act as a container view controller.

UIKit provides various subclasses of `UIViewController` with extended functionality. 


[**UITableViewController** ](https://developer.apple.com/documentation/uikit/uitableviewcontroller)

- A subclass of UIViewController that specialises in managing a [table view](https://developer.apple.com/documentation/uikit/uitableview) and so facilitates UI display in row/ table form.

See:

- [Table View Basics](https://code.tutsplus.com/tutorials/ios-from-scratch-with-swift-table-view-basics--cms-25160)
- [Filling a Table with Data](https://developer.apple.com/documentation/uikit/views_and_controls/table_views/filling_a_table_with_data)
- [Creating Custom Table View Cells](https://programmingwithswift.com/create-a-custom-uitableviewcell-with-swift/)


[**UICollectionViewController**](https://developer.apple.com/documentation/uikit/uicollectionviewcontroller)

- A subclass of UIViewController that specialises in managing a [collection view](https://developer.apple.com/documentation/uikit/uicollectionview), and so facilitates UI display in a more flexible form including grid. You can also still configure data into a table view.
- UICollectionViewController is very similar to UITableViewController in other ways. For example, subclasses conforming to the relevant protocols require similar methods. Where the word 'row' would appear, you can instead write 'item'.

See further:

- [Paging](https://medium.com/@shaibalassiano/tutorial-horizontal-uicollectionview-with-paging-9421b479ee94): CollectionViews can be handy for Onboarding functions in your app, as they allow the user to swipe through views as if they are pages.
- [Smooth Scrolling](https://www.raywenderlich.com/7341-uicollectionview-tutorial-prefetching-apis#toc-anchor-004): Both TableViews and CollectionViews can be paired with pre-fetching and lazy loading to improve user experience.


____________________________________________________________________________________________
**EXAMPLE** 

If your interface includes a table view or collection view, you can replace the implementation of `UIViewController` with `UITableViewController` or `UICollectionViewController` - i.e. subclass it. 

You will need to add a `TableView` or `CollectionView` in the related View (e.g. Storyboard).

You can subclass `UIViewController` directly, but if you do this in addition to the above Storyboard changes you will need to make your subclass conform to the relevant data source and delegate protocols. 

For the table view these are `UITableViewDataSource` and `UITableViewDelegate`. They are placed behind the class that is being subclassed and before the curly braces:

```swift
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  override func viewDidLoad() {
       super.viewDidLoad()
   }
   // custom code
}
```

If you are subclassing `UITableViewController` or `UICollectionViewController`, this functionality is included so you don't need to add additional protocols:

```swift
class ViewController: UITableViewController {
  override func viewDidLoad() {
       super.viewDidLoad()
   }
   // custom code
}
```

Xcode will prompt you to add any methods required by the protocols to which your class conforms (e.g. cellForRowAt, or cellForItemAt). Choosing 'fix' will add these.

In both cases you need to connect the ViewController as the table view data source and delegate:

```swift
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

  override func viewDidLoad() {
       super.viewDidLoad()
       tableView.delegate = self
       tableView.dataSource = self
   }
   // custom code
}
```

If you don't, your data won't show up in the UI - even if the app is compiling and showing no errors.
____________________________________________________________________________________________


## Views and Sub-views

View controllers load their views lazily. Accessing the view property for the first time loads or creates the view controller’s views. There are several ways to specify the views for a view controller:

- *Preferred*: Specify the view controller and its views in your app’s Storyboard. To load a view controller from a storyboard, call the `instantiateViewController(withIdentifier:)` method of the appropriate `UIStoryboard` object. The storyboard object creates the view controller and returns it to your code.
- *Useful for custom cells and sub-views*: Specify the views for a view controller using a Nib file. To initialise a view controller object using a nib file, create your view controller class programmatically and initialise it using `init(nibName:bundle:)`. When its views are requested, the view controller loads them from the nib file.
- *Other*: Specify the views for a view controller using the `loadView()` method. In that method, create your view hierarchy programmatically and assign the root view of that hierarchy to the view controller’s view property.

All of these techniques have the same end result, which is to create the appropriate set of views and expose them through the view property.

For Views, the simplest way in UIKit is to add a new Storyboard. This will contain a View by default. 

For Sub-views, the easiest way is to create a stand-alone XIB file, and use a `UIView` to display it where you want it to appear in your project (it can be re-used, such as a loading view).

See:

- [Custom View from XIB Tutorial](https://betterprogramming.pub/swift-3-creating-a-custom-view-from-a-xib-ecdfe5b3a960)


## Lifecycle

### General

- `init()`
  
### Using XIBs (for views/ subviews)

- `loadView()` - used when view controller is created from code
- `awakeFromNib()`
  
### View Controller

- `viewDidLoad()` - only called once as the app runs, when the current view is nil
- `viewWillAppear()` - called every time before the view is visible
- `viewWillLayoutSubviews()` - this adjust the position of the view when bounds change
- `viewDidLayoutSubviews()` - called after view controller has been adjusted after a change in bounds
- `viewDidAppear()` - called after the view is presented on the screen
- `viewWillDisappear()` - called before the view is removed from the view hierarchy
- `viewDidDisappear()` - called after the view controller's view has been removed from view hierarchy

A view controller is the sole owner of its view and any subviews it creates. It is responsible for creating those views and for relinquishing ownership of them at the appropriate times such as when the view controller itself is released.


## References and Resources
- https://developer.apple.com/documentation/uikit/view_controllers
- https://developer.apple.com/documentation/uikit/view_controllers/managing_content_in_your_app_s_windows
- https://developer.apple.com/documentation/uikit/view_controllers/showing_and_hiding_view_controllers
- https://developer.apple.com/documentation/uikit/uiwindow/1621581-rootviewcontroller
- https://developer.apple.com/tutorials/swiftui/interfacing-with-uikit
- https://developer.apple.com/documentation/uikit/uiviewcontroller
- https://developer.apple.com/documentation/uikit/uiviewcontroller/1621380-present
- https://developer.apple.com/documentation/uikit/uiviewcontroller/1621505-dismiss
- https://developer.apple.com/documentation/uikit/uinavigationcontroller
- https://developer.apple.com/documentation/uikit/uinavigationcontroller/1621887-pushviewcontroller
- https://developer.apple.com/documentation/uikit/uinavigationcontroller/1621886-popviewcontroller
- https://developer.apple.com/documentation/uikit/uitabbarcontroller
- https://developer.apple.com/documentation/uikit/uisplitviewcontroller
- https://developer.apple.com/documentation/uikit/uitableviewcontroller
- https://developer.apple.com/documentation/uikit/uitableview
- https://developer.apple.com/documentation/uikit/uicollectionviewcontroller
- https://developer.apple.com/documentation/uikit/uicollectionview
- https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/index.html#//apple_ref/doc/uid/TP40007457
- https://medium.com/@vialyx/ios-dev-course-uitabbarcontroller-5df45ca78188
- https://www.raywenderlich.com/4613809-uisplitviewcontroller-tutorial-getting-started
- https://code.tutsplus.com/tutorials/ios-from-scratch-with-swift-table-view-basics--cms-25160
- https://programmingwithswift.com/create-a-custom-uitableviewcell-with-swift/
- https://medium.com/@shaibalassiano/tutorial-horizontal-uicollectionview-with-paging-9421b479ee94
- https://www.raywenderlich.com/7341-uicollectionview-tutorial-prefetching-apis#toc-anchor-004
- https://betterprogramming.pub/swift-3-creating-a-custom-view-from-a-xib-ecdfe5b3a960
- https://docs-assets.developer.apple.com/published/84548660ef/view-controller-architecture~dark@2x.png (UIWindowScene diagram)

