# NSNotificationCenter

[Apple NSNotificationCenter documentation](https://developer.apple.com/documentation/foundation/nsnotificationcenter)

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [How does NotificationCenter work?](#markdown-header-how-does-notificationcenter-work)
- [How to Register Notification Observers](#markdown-header-how-to-register-notification-observers)
- [`Notification.Name`](#markdown-header-notification-name)
- [Posting Notifications](#markdown-header-posting-notifications)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

`NotificationCenter` is a tool for communicating information within your app. This is
different from using push or local notifications, where you notify a user of content you want them to receive. 
`NotificationCenter` instead allows us to send and receive information between classes and/or
structs based on an action that has occurred in your application.

A good way of thinking about `NsNotficationCenter` is as a broadcaster. Other parts 
of your code can tune in to the different channels and listen for any changes. 

## How does NotificationCenter work?

It has three components:

1. A **"listener"** that listens for notifications, called an **observer**
1. A **"sender"** that sends notifications when something happens
1. The notification center itself, that keeps track of observers and notifications

We can get the the default "notification center" via `NotificationCenter.default`. This is where all notifications
are posted to and observed from. 

Each notification must have a unique way to identify themselves (like the different channels of a broadcaster).
In the same way, if we want to observe or listen to specific notifications (channels), we would call on the name of
the notification on `NotificationCenter.default`

## How to Register Notification Observers

To set up a NotificationCenter and be alerted on changes, you need to create an observer. 

```swift
NotificationCenter.default.addObserver(
    self,
    selector: #selector(somefunction(notification:)),
    name: Notification.Name.uniqueName,
    object: nil)
```

Let's break down this block of code 

1. The first parameter, `observer` is unnamed and refers to the object that observes the notification
2. The second parameter, `selector` is the function you call when the notification occurs. Because this is marked with #selector, make sure to use `@objc` on your function declarations
3. The third parameter `name` is the name of the notification you want to listen for (it is of type Notfication.Name, more on this below)
4. The fourth parameter, `object` is an optional object whose notifications you want to receive, so if this was set, you would only receive notifications from that "sender"

## Notification. Name

This code adds an **observer** (the listener object that listens for notifications) to the notification center, telling it that `self`
wants to observe for notifications with name .uniqueName (Notification.Name can be custom names aligned to listeners you create or one of the many listeners already provided by Apple e.g. `.titleChanged`, `.didReceiveData` for more look [here](https://learnappmaking.com/notification-center-how-to-swift/) )

You can create custom Notification.Names easily 

```swift
let name = Notification.Name("uniqueName")
```
    
It is a good practice to add your notification names as static constants to an extension of `Notification.Name` like so

```swift
extension Notification.Name {
    static let uniqueName = Notification.Name("uniqueName")
    static let anotherListener = Notification.Name("anotherListener")
    static let textFieldFilled = Notification.Name("textFieldFilled")
}
```

Now with the names available as static constants, you can use this 

```swift
NotificationCenter.default.addObserver(self, selector: selector, name: .uniqueName, object: nil)
```

## Posting Notifications 

When wanting to post notification/ notify notification center observers of any changes, simply use 
the NotificationCenter's `post` method, and placing the observer's name within the `name` parameter. 

```swift
NotificationCenter.default.post(name: .uniqueName, object: nil)
```

This will then alert `.uniqueName`'s observer and run the function set in the `selector` parameter.

## References and Resources

- [Simple example of using NotficationCenter](https://medium.com/@JoyceMatos/using-nsnotificationcenter-in-swift-eb70cf0b60fc)
- [More on NotficationCenter](https://learnappmaking.com/notification-center-how-to-swift/)
- [Apple NotficationCenter's addObserver method](https://developer.apple.com/documentation/foundation/nsnotificationcenter/1415360-addobserver)
- [Understanding Notification.Name](https://cocoacasts.com/what-is-notification-name-and-how-to-use-it)

