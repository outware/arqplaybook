#  Audio Playback

[Apple documentation - AVAudioPlayer](https://developer.apple.com/documentation/avfaudio/avaudioplayer)

## Table of Contents
- [Introducation](#markdown-header-introduction)
- [Play Audio from Local Storage](#markdown-header-play-audio-from-local-storage)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

To play an audio file within your application first import `AVFoundation`:

```swift
import AVFoundation

class MyClass {
    //
}
```

This framework combines six major technology areas that together encompass a wide range of tasks for capturing, processing, synthesising, controlling, importing and exporting audiovisual media on Apple platforms.

These will give you access to the tools to make audio play such as `AVAudioPlayer` and `AVAudioSession`.

## Play Audio from Local Storage

First load the sound file into your project 

- To add the sound file start by clicking on the top most folder of your project

![Project Top Folder](../Assets/Project Top Level Folder.png)

- This will open a menu in the main screen, click on the tab `Build Phases` and find the section called `Copy Bundle Resources`

![Build Phases](../Assets/Build Phases.png)

![Copy Bundle Resources](../Assets/Copy Bundle Resources.png)

- Press the `+` button. A modal menu will appear, click on `Add Other` to find the desired sound file on your computer.

![Add Resource](../Assets/Add Resource Menu.png)

- Once you have found the sound file, click `open` and in the next menu select `finish`. You will have now added the sound file into the project.

In this example we are going to add a button to the storyboard to trigger the sound file to start and stop playing. After importing `AVFoundation` into your View Controller file declare an optional instance of `AVAudioPlayer`

- `var soundPlayer: AVAudioPlayer?`

Then create an `@IBAction` function for when the button is pressed. 

Add an `if let` statement inside this function to check if the sound file is already playing or not. 

If the sound is playing, `stop()` should be called on the instance of `AVAudioPlayer`. If the sound is not playing the function will execute the `do/try` block. 

- Within this block, first declare where the sound file is with `Bundle.main.path(forResource: , ofType: )`.

- Then `try` to create an instance of `AVAudioSesion`. 

- Unwrap the string we created (`soundPath`) to declare where the sound file is. 

- Use the `soundPath` to instantiate an `AVAudioPlayer`.

- Write a guard statement to unwrap the player to ensure it has been created properly.

- Then call the `play()` function on the audio player `soundPlayer`.

- The `catch` part of the block prints out an error if something has failed.

```swift
@IBAction func playAudioButtonPressed(sender: UIButton) {
    print("playing audio")
    if let soundPlayer = soundPlayer, soundPlayer.isPlaying {
        //stop playback
        soundPlayer.stop()
    }
    else {
        //set up player and play
        let soundPath = Bundle.main.path(forResource: "Antigravity Guitar copy", ofType: "caf" )
    
        do {
            try AVAudioSession.sharedInstance().setMode(.default)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
        
            guard let soundPath = soundPath else {
                return
            }
        
            soundPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundPath))
            guard let soundPlayer = soundPlayer else {
                return
            }
            soundPlayer.play()
        
        } catch {
            print("sound file failed to play")
        }
    }
}
```

## References and Resources
- https://developer.apple.com/documentation/avfaudio/avaudioplayer
