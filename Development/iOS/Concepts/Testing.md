# Testing

## Table of Contents
- [Overview](#markdown-header-overview)
- [Development Processes](#markdown-header-development-processes)
- [Test Driven Development](#markdown-header-test-driven-development)
- [Behaviour Driven Development](#markdown-header-behaviour-driven-development)
- [Other Testing Types](#markdown-header-other-testing-types)

## Overview

Tests can be categorised in two main categories: automated and manual. Proper automated test suits can run quickly and report failures accurately which makes them the preferred way. Unit, behaviour and UI tests are some of the automated tests.

A test asserts a condition. The code under the test can be a function, a class or a module, often referred to as _the subject_. The subject's implementation is hidden from the test and is only visible to the test by its public interfaces.

Every test has 3 main steps:

- Context
    * A familiar state before an action
- Action
    * The action to be performed
- Outcome
    * The outcome of the action to be verified

Test steps may be expressed in different ways. Most unit test frameworks use _describe_, _context_ and _it_ to define the test steps. Behaviour tests, on the other hand, commonly use Gherkin syntax as the test format in the form of _given_, _when_, _then_.

Test Driver Development (TDD) and Behaviour Driven Development (BDD) are two popular test-first software development processes that are commonly practiced. We encourage the use of TDD, BDD, or the combination of both in any new or existing development opportunities.

To enforce inclusion of tests in the development process, tests should be added as a criteria in the [Definition of Done (DoD)](https://www.agilealliance.org/glossary/definition-of-done/). Running the test suits must be an integral step in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) build pipelines ([Continuous Integration / Continuous Delivery (or Deployment)](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment)).

## Development Processes

### Test Driven Development
Unit tests are commonly automated tests that are created before the code is written [Test Driven Development (TDD)](https://en.wikipedia.org/wiki/Test-driven_development).

#### Frameworks
- [XCTest](https://developer.apple.com/documentation/xctest)

#### Setup
By ticking the checkbox "Include tests" when creating a new XCode project, tests using XCTest will be included. The tests then can be added to or expanded as the development progresses. To add unit tests to an existing project where the project has not been configured for unit testing, follow [these steps](https://medium.com/@CoderLyn/setting-up-a-new-test-folder-205aec029455) instead.

For more, see [Test Driven Development](./TestDrivenDevelopment) in this playbook and check out the links below:

#### References
- [The Three Rules of TDD](http://www.butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)
- [Test Driven Development Tutorial for iOS: Getting Started](https://www.raywenderlich.com/5522-test-driven-development-tutorial-for-ios-getting-started)
- [Example of XCTesting](https://www.raywenderlich.com/21020457-ios-unit-testing-and-ui-testing-tutorial)
- [Test Driven Development: By Example](https://www.amazon.com/gp/product/0321146530/ref=as_li_ss_tl?ie=UTF8&linkCode=sl1&tag=&linkId=6d76ed79b7f498d7930b370ad4c059ac)

### Behaviour Driven Development

#### Frameworks
- [Quick](https://github.com/Quick/Quick) The BDD framework
- [Nimble](https://github.com/Quick/Nimble) Expressive matcher framework

#### References
- [Introducing BDD](https://dannorth.net/introducing-bdd/)
- [The Truth about BDD](https://sites.google.com/site/unclebobconsultingllc/the-truth-about-bdd)
- [Behavior-Driven Testing Tutorial for iOS with Quick & Nimble](https://www.raywenderlich.com/135-behavior-driven-testing-tutorial-for-ios-with-quick-nimble)

## Other Testing Types
### Progression

When projects have had discrete changes made and merged in, this is usually in response to a 'ticket' that defines the scope of the changes needed (e.g., add feature 'top-up balance').

Progression testing may then be done by Quality Engineers (QEs) to test the changes against their scope as defined in the ticket.

The QE may incorporate new automated testing in order to ensure the new build succeeds.

Manual testing may also be done by Quality Assurers (QAs) at this point.

### Regression

Conversely, this happens in the period directly before release and deployment - for example, on the Apple Store.

It involves end-to-end testing to make sure the changes have not broken anything else.

For a large application, this could take days of manual effort per platform. Automated testing is included to reduce the manual overhead.

As part of this, the performance of the new version may also be compared to previous versions.

**Frameworks used** - Appium

ARQ emphasises automation and scalability especially for large applications.

[Appium](http://appium.io/docs/en/writing-running-appium/running-tests/) is the preferred framework for performance testing.
