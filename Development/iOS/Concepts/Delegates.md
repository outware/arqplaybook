#   Delegates

## Table of Contents
- [What is a Delegate?](#markdown-header-what-is-a-delegate)
- [Implementation](#markdown-header-implementation)
- [Connecting ViewModels A -> B](#markdown-header-connecting-viewmodels-a-b)
- [Handlers](#markdown-header-handlers)
- [References and Resources](#markdown-header-references-and-resources)

## What is a Delegate?

Delegates are a means of passing information to different view controllers or view models (different files within your project) while maintaining separation of concerns.

Separation of concerns is a principle of good code design that makes code more testable and maintainable. It is essentially organising and writing code in such that when changes are made in one component, minimal changes need to be made in other parts of the code.

In iOS development, delegates are one means of implementing the Model View ViewModel (MVVM) [design pattern](./DesignPatterns.md).

You will often see the terms 'protocols' and 'delegates' used interchangeably, because delegates are created using custom swift [Protocols](https://docs.swift.org/swift-book/LanguageGuide/Protocols.html).

In MVVM, the View Controller ["owns"](https://www.raywenderlich.com/34-design-patterns-by-tutorials-mvvm) and controls the View Model and so it has a strong connection to it. In order to avoid strong coupling, the View Model should therefore never directly interact with the View Controller. Strong coupling can cause issues for code maintainability and testing. Avoiding it is a major reason for using design patterns such as MVVM to maintain separation of concerns.

In MVVM with delegates, instead of interacting directly, delegates can be used to act on behalf of the View Model:

![MVVM Diagram](../Assets/MVVMDelegates.png)

## Implementation

In this relationship, the View Model is referred to as the "Instructor" or "Answerer", and the View Controller as the "Listener" or "Asker".

You will need to implement the following steps in your code:

- a delegate protocol is created, and the View Model is made to conform to that protocol
- a weak instance of that protocol is declared in the View Controller, allowing it to access the delegate methods implemented in the View Model indirectly. 

When creating delegates remember to

**Create** - the two things you are trying to connect need to exist

**Initialise** - depending on what you are trying to connect, you need to make sure that you are declaring that the connection exists or making sure the connection complies with the protocol requirements.

**Connect** - how are these two things communicating

**Access** - once the two things know about each other how does one ask for information from the other?


### 1. Create 

**Protocols can be created in their own file - or at the top of a file for ease of reference**

A protocol may have one or many functions declared inside it, however each function declaration must be used within the class that conforms to the protocol.

Protocol names should be capitalised like classes and structs:

```swift
protocol ModelDelegate: AnyObject { 
    modelDelegateFunction(_ name: String)
}
```

**Assign** Protocol Delegate to ViewModel:

```swift
class ViewModel: ModelDelegate { }
```

### 2. Initialise

**Assign View Model inside View Controller**

Don't forget to set the View Model in the View Controller.

When using UIKit and Storyboards, this is often done by creating a custom 'instantiate' function within the View Controller and providing the `.viewModel` value using the declared viewModel variable (see '3. Connect' below).

- `viewController.viewModel = viewModel`

**Set Delegate Methods/Stubs**

The class that conforms to the delegate protocol must implement every method required by the protocol, for example:

```swift
protocol ModelDelegate: AnyObject { 
    modelDelegateFunction(_ name: String)
}

class ViewModel: ModelDelegate { 

    func modelDelegateFunction(_ name: String){
        // contents of function defined here
    }
}
```
    
### 3. Connect 

**View Model to View Controller**

(1) Declare viewModel variable in Answerer/Instructor class (View Controller):

- `var viewModel: NameViewModel!` 

(2) Declare weak delegate variable in Asker/Listener class (View Model):

- `weak var modelDelegate: ModelDelegate`

Make sure to use the modifier 'weak' before the variable declaration, so that strong coupling is not created between the ViewModel and ViewController (if you don't, it will undermine the MVVM design architecture by creating a strong connection/ coupling).


### 4. Access

**Delegate properties and methods of the delegate protocol** 

(1) From the View Controller

Access these through the View Model's instance since this is already declared within the Answerer/Instructor class (View Controller):

- `var variable = viewModel.viewModelVariableName`

(2) From the View Model

Access these through the delegate's instance, which is we just declared within the Asker/Listener class (View Model). 

You could access/invoke methods using dot notation on the delegate instance and, for example, place this inside an action associated with a UI event such as a pressed button:

```swift
    func buttonPressed(_ name: String) {
    
        modelDelegate?.modelDelegateFunction 
        
    }
```

This code above looks for classes that conform to the view model delegate (via variable `modelDelegate`) and, if so, uses it to execute `modelDelegateFunction`.


## Connecting ViewModels A -> B

In a more complex application, you may have more than one View Model.

You can use delegates to communication **between** view models as well as from view controller to view model.

**Connect** in ViewModel A

```swift
func bViewModel(for indexPath: IndexPath) -> BViewModel? {
    let bViewModel = BViewModel()
    self.modelDelegate = bViewModel
    return bViewModel
}
```


**Access** in ViewModel B

```swift
class BViewModel: AViewModelDelegate {
    func modelDelegateFunction( _ name: String ) {
        variableToChange = name  
    }
} 
```

## Handlers

**Create** in Handler file (Instructor/Answerer):

```swift
 Protocol Handling: AnyObject {
    func handlingFunction(_ name: String)
 }

 class Handler {
    weak var handlerDelegate: Handling
    func callHandler( ) {
        handlerDelegate?.handlingFunction(name: "hello world")
    }
 }
```

 
**Initialise** Class must conform to delegate protocols - add protocol stub ( function ):


```swift
func handlingFunction( _ name: String) { }
```    
    
**Connect** in viewModel file (Asker/Listener):

```swift
 class ViewModel : Handling {
    var handler: Handler
    
    init( ) {
        handler = Handler
        handler.handlerDelegate = self 
    } 
}
```
    
**Access** Call handler in function from ViewController that is triggered when a button is pressed:

```swift
func buttonPressed( _ name: String ) {
    handler.callHandler( ) 
}
```
    
## References and Resources
- [WWDC2015: Protocol Oriented Programming in Swift - https://developer.apple.com/videos/play/wwdc2015/408/](https://developer.apple.com/videos/play/wwdc2015/408/)
- https://docs.swift.org/swift-book/LanguageGuide/Protocols.html
- https://www.raywenderlich.com/34-design-patterns-by-tutorials-mvvm
- https://www.raywenderlich.com/6742901-protocol-oriented-programming-tutorial-in-swift-5-1-getting-started
- https://www.codementor.io/@nishadhshrestha/mvvm-in-swift-4-using-delegates-ikdflt1cb
- https://www.swiftbysundell.com/articles/separation-of-concerns-using-protocols-in-swift/  
- https://learnappmaking.com/delegation-swift-how-to/  
- https://stackoverflow.com/questions/5732745/updating-a-viewmodel-from-another-viewmodel
- https://stackoverflow.com/questions/38451129/how-to-use-get-and-set-in-swift/38452377
- https://pavlepesic.medium.com/flow-coordination-pattern-5eb60cd220d5 

