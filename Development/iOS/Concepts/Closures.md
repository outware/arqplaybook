# Closures

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Implicit Returns from Single-Expression Closures](#markdown-header-implicit-returns-from-single-expression-closures )
- [Shorthand Argument Names ](#markdown-header-shorthand-argument-names )
- [Trailing Closures](#markdown-header-trailing-closures)
- [Completion Handlers](#markdown-header-completion-handlers)
- [Escaping Closures](#markdown-header-escaping-closures)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

Closures are  self-contained blocks of code containing functionality that you can assign to a variable.
This can then be passed around in your code. 

A simple closure syntax looks like this:

```swift
let exampleClosure = { (parameters) -> returnType in
    //statements
}
```

`returnType` here can be changed to any return type e.g. String, Int, Double.

Closures can also be written like this:

```swift
let voldemort: (String) -> { spell in 
    print("Voldemort says \(spell)!")
}

voldemort("AVADA KEDAVRA")
```

The three things that matter here:

- The closure type `(String) -> ()` (takes in a String and returns void)
- The closure expression `{ spell in  }` (make sure to use the keyword `in` as it is in place to seperate the closure's parameters from it's code body)
- The closure call `voldemort()`

### Implicit Returns from Single-Expression Closures 

Single-expression closures implicitly return the result of their single expression.
"Single-expression" means that there is only one line of code/expression within the closure body.

e.g. The closure below will return 8

```swift
var multiplyClosure = { (a: Int, b: Int) -> Int in
  a * b
}

let result = multiplyClosure(4, 2)
```

### Shorthand Argument Names 

Swift automatically provides shorthand argument names to inline closures (closures whose values are used immediately,
without first being assigned to an intermediate variable).

Here `1` is assigned to an intermediate variable then printed

```swift
let number = 1
print(number)
```
    
On the other hand, below `1` is an inline value, it is printed directly

```swift
print(1)
```
    
To refer to the values of a closure's arguments use the names `$0`, `$1`, `$2` and so on.

`$0` refers to the first argument, `$1` the second argument etc.

For example this is useful with many of the functional programming methods such as `.filter`

```swift
let squadGoals = ["Harry", "Ron", "Hermoine"]
squadGoals.filter{ $0.count < 4 }
```

The code snippet above iterates over the `squadGoals` array and filters out names that have less than 4 characters. 
The `filter` function will return `["Ron"]`

### Trailing Closures 

If you need to pass a closure expression to a function as the function's **final** argument and the closure is long,
it can be good to use a trailing closure. 

You write a trailing closure after the function call's parantheses:

```swift
someFunctionThatTakesAClosure(parameter: String){
    // trailing closure's body goes in here
}
```
    
Without the trailing closure it would look like this:

```swift
someFunctionThatTakesAClosure(parameter: String, closure: {
    // closure's body goes in here
})
```

### Completion Handlers 

A completion handler is just a closure that is used to complete a certain action that is only called once
you have finished doing something. 

The reason why we would use one is because we want to be notified when something (that takes time) is completed.
Completion handlers are especially used in [asynchronous](./SynchronousAsynchronous.md) work.

A good example of this is when we do a network request. Network requests are asynchronous. 
This means that the code will not wait for the request to return before it runs the rest of the code.

So in order to get around this we need to pass a function in and once the request is complete, 
it will call that function which will perform the required action.

An example of completion handlers being used in an asynchronous HTTP request is shown below under the ["Escaping Closures"](#escaping-closures) segment. 

When we have received data from the request, we call on the completion handler, which we named as the `completion` parameter. Because of this we will call on the completion handler by doing `completion()`.

As we indicated that the closure had a parameter (a String; we expect to receive a string of data from the HTTP request) 
in the declaration of the completion handler ( `(String) -> Void` ),
we must pass the data string successfully received:

`completion(data)`

or pass in an error (as a string) when calling the completion handler: 

`completion(error.localizedDescription)`

Note: `.localizedDescription` returns a string of the error's decscription, we use this to match our closure's parameter type `(String)`.


### Escaping Closures

A closure is said to "escape" a function when the closure is passed as an argument to the function, 
but it is called after the function already returns.

When this happens, you can write `@escaping` before the closure's parameter's type to indicate that it is allowed to escape. ` completion @escaping (String) -> Void ) { ... }`

An example of a closure that can escape is one that is stored in a variable defined outside the function. e.g. When making a HTTP
request (see code snippet below), the `URLSession` dataTask usually `resumes()` and returns (ends the function),
moving on to execute other code blocks within your application. 

However, after a few moments, the data from the request will finally be ready, at this point, the completion handler (the closure)  will be called by doing `completion()`. 

This completion handler is being called after the function already returns and so we add `@escaping` before the closure's parameters.

```swift
func fetchData(with url: URL, completion: @escaping (String) -> Void) {
        // perform data task
        URLSession.shared.dataTask(with: url!) { data, response, error in
        if let error = error {
            completion(error.localizedDescription)
        }
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            print("Error : HTTP Response Code Error")
            // handle bad response
        }
        
        guard let data = data else {
            // handle errors
        }
        
        // when code reaches here, it means data task returned successfully
        DispatchQueue.main.async {
            completion(data)
        }
        
    }.resume()
}
```
    
Escaping closures tend to be used for **tasks that takes time**. This is because tasks that take time tend to be run in a background thread (not on
the main thread, so that the User Interface is not frozen while the function takes the time to complete). 

Tasks that take a longer time to complete can be handled through a few options 

1. Using completion handlers - to pass back result or notify calling object that the function has completed
1. [Async/ Await](./SynchronousAsynchronous.md) - making it clear that the function is performing asynchronous work and awaiting a response
1. [Protocols and Delegation](./Delegates.md) - alerting the calling object of that the function has completed and passing back results where requireds

## References and Resources

- [Closures in Swift](https://docs.swift.org/swift-book/LanguageGuide/Closures.html)
- [Closures explained with Code Examples](https://www.avanderlee.com/swift/trailing-escaping-closures/)
- [The Ultimate Guide to Closures](https://learnappmaking.com/closures-swift-how-to/)
- [Understanding Completion Handlers](https://programmingwithswift.com/understanding-completion-handlers-in-swift/)
