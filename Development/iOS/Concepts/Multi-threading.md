# Multithreading

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [Grand Central Dispatch (GCD)](#markdown-header-grand-central-dispatch-gcd)
- [Main Queue](#markdown-header-main-queue)
- [Global Queues](#markdown-header-global-queues)
- [Custom Queues](#markdown-header-custom-queues)
- [Thread Communication](#markdown-header-thread-communication)
- [Dispatch Groups](#markdown-header-dispatch-groups)
- [Dispatch After](#markdown-header-dispatch-after)
- [Dispatch Semaphore](#markdown-header-dispatch-semaphore)
- [GCD Synchronous and Asynchronous Methods](#markdown-header-gcd-synchronous-and-asynchronous-methods)
- [The Readers-Writers problem](#markdown-header-the-readers-writers-problem)
- [Dispatch Barriers](#markdown-header-dispatch-barriers)
- [Combinations of Queues](#markdown-header-combinations-of-queues)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

Multithreading is simply the process of putting closures in queues

```swift
queue.async {
  // closure here
}
```

`queue` in the above code represents the queue you want the closure to run on
e.g. you can have `DispatchQueue.main.async {}` or `DispatchQueue.global.async {}`

Multithreading is mostly about “queues” in iOS.
Functions (usually closures) are simply lined up in a queue.
Then those functions are pulled off the queue and executed on an associated thread(s). Queues can be “serial” (one closure a time) or “concurrent” (multiple threads servicing it).



## Grand Central Dispatch (GCD)

[Dispatch Apple documentation](https://developer.apple.com/documentation/DISPATCH)

GCD makes it easier to write multithreaded code. Grand Central Dispatch is a technology that is designed to make the execution of tasks on multicore hardware performant and straightforward. 

It can do this because it operates at the system level. Your application operates in a sandbox, which means that it is unaware of other processes running on the system at the same time. 

Because Grand Central Dispatch operates at the system level, it has an accurate view of the processes that are running and the resources that are available. 

An application interacts with Grand Central Dispatch through dispatch queues. Dispatch queues is a place where work can be scheduled for execution.

Examples of these dispatch queues include the `main` and `global` queue.

However, you should be aware that GCD is very eager to bring up threads whenever we kick off work on queues. 

When a queue blocks its thread, a new thread will be spawned to handle work, meaning the system can overcommit with more threads than there are CPU cores.

This can lead to thread explosion, which results in memory and performance issues. 

Furthermore, with lots of scheduling overhead while using threads, there will be a lot of context switching, which in turn will make the CPU run less efficiently.

Swift's concurrency model using [async/await](./SynchronousAsynchronous.md) is designed to be more efficient than GCD, as `await` does not block a thread like GCD does.

## Main Queue

There is a very special serial queue called the “main queue”.
All User Interface activity MUST occur on this queue and this queue only.
And, conversely, non-User Interface activity that is time consuming (e.g. API calls) must NOT occur on that queue. We do this because we want our UI to be highly responsive and want things that happen in the UI to happen predictably (serially)! If memory/time expensive actions were called on the main queue, the main thread would freeze as the action is being completed.


Example of getting an instance of the main queue

```swift
DispatchQueue.main.async {
  doSomething()
}
```

## Global Queues
For non-main queue work, you will usually use a shared, global, concurrent queue.

Global queues execute one or more tasks concurrently. As the name implies, however, it still keeps its FIFO order of execution. 

Instantiating the global queue requires us to specify the `qos` (QoS - Quality of Service) as a parameter.

The parameter for this function of getting the 
global queue must be filled with a type of QoS, highlighting their priority to the system.

Example of getting a global, shared concurrent queue:

```swift
DispatchQueue.global(qos: .userInitiated).async {
  doSomething()
}
```

There are 4 different QoS types:

1. `.userInteractive` - system sees it as high priority, only for doing something short and quick, should run on the main thread
2. `.userInitiated` - system sees it as high priority, but it might take a little bit of time (e.g. making API calls to display data takes time)
3. `.background` - not directly initiated by user and so it can run as slow as needed, has lowest priority
4. `.utility` - long-running background processes, low priority

## Custom Queues

Custom queues can be created to be either serial or concurrent. Requests in these queues actually end up in one of the global queues.

Custom queues are created using `DispatchQueue`'s `init(label:qos:attributes:autoreleaseFrequency:target:)`.

Some reasons for why we would use custom queues include:

1. To use dispatch barriers (which are unavailable in global queues) to ensure a certain value is thread-safe (see below on more details on dispatch barriers)

2. Debugging your app, it can be useful to use your own queues with meaningful names, so that you can more easily identify the individual threads in the debugger

```swift
    DispatchQueue(label: "specificName.queue").async {
      doSomething()
    }
```

Parameters associated with creating a custom queue include: 

- `label`- a string attached to the queue to uniquely identify it in debugging tools
- `qos` - Quality-of-service level associated with the queue which determines the prioritiy of the task to the systems
- `attributes` - add on `.concurrent` to specify a queue should execute tasks concurrently, if omitted, the dispatch queue will execute tasks serially
- `autoreleaseFrequency` - frequency with which to autorelease objects created by the blocks 
- `target` - the target queue on which to execute blocks 

## Thread Communication

Sometimes threads will need to communicate with each other, letting each other know when a task has been completed.

For example, if the application needed to download and display a large image, we would use a background thread to start a URL request and download that image (so that it does not block any UI operations) and then let the image be passed back onto the main thread to display the image (because UI changes can only be done on the main thread).

To enable this communication between threads we can call to dispatch and push new tasks onto the next thread when tasks are ready. 

Look at the code example below that highlights how the background thread downloading the image communicates to the main thread when it has the image ready to display.
Here `BackgroundThread` is the background thread you have used to create a HTTP request for the image. 

```swift
BackgroundThread.async {
    //download image via a URL request
    
    DispatchQueue.main.async {
        // Render the image and update the UI to display image
    }
}
```

## Dispatch Groups 

[Dispatch Groups Apple Documentation](https://developer.apple.com/documentation/dispatch/dispatchgroup)

Sometimes we need to perform certain tasks (more than one) before executing the next task.
 
For example imagine a scenario when you need to make a couple of API calls to retrieve information before processing this. 

Instead of observing these tasks individually we can use dispatch groups.

Dispatch groups allow us to monitor a group of tasks as a single unit, it will aggregate a set of tasks and synchronise behaviours in the group. 

We can add tasks to a group by making calls to both `enter()` and `leave()`, then when all the tasks finish executing, the group executes its completion handler. 

There are four essential methods to dispatch groups: 

1. `enter()` - indicate that a block of code has entered the group
2. `leave()` - indicate that a block of code has finished executing and left the group
3. `wait()` - wait asynchronously for the submitted work to finish, this will block the calling thread until the work is finished
4. `notify()` - notifies the required thread that the submitted code block is done excecuting

To implement we first create an instance of `DispatchGroup()` and then we call on `enter()` before any tasks. 

In the code example below, because we are making a HTTP request, we add the `group.leave()` on a defer statement, which will run when we exit the completion closure of the `dataTask`. 
Then if we still have more tasks to complete within the group we indicate a new task entering the group by calling `group.enter()` and also calling `group.leave()` when it is completed.
`group.wait()` will block the called thread until all the operations are complete, it will determine completeion by equating the number of `enter()` and `leave()` calls. 
Once an equal number of `leave()` has been executed the calling thread will be unblocked.

```swift 
func getData() {
    let group = DispatchGroup()
    let url = URL(string: "https://someUrlLink.com")
    group.enter()
    URLSession.shared.dataTask(with: url) { (data, response, error) in 
        defer { group.leave() }
        // handle response, data and error
        if let response = response as? HTTPURLResponse, response.statusCode == 200 {
            group.enter()
            self.displayData() { 
                // update the UI to display received data
                group.leave()
            }
        }
    }.resume()
    group.wait()
}

```

It is important to note that it is highly recommended to avoid `wait()` whenever possible because it essentially blocks the calling thread, which in the example above is our main thread. 
Because of this we can instead use `notify()`, which does not block the calling thread and instead notifies the main thread when all the `group.leave()` methods have been executed.
In the code example below, once the main thread is notified in will print out "work done: ..."

```swift
func getData() {
    let group = DispatchGroup()
    let url = URL(string: "https://someUrlLink.com")
    group.enter()
    URLSession.shared.dataTask(with: url) { (data, response, error) in 
        defer { group.leave() }
        // handle response, data and error
        if let response = response as? HTTPURLResponse, response.statusCode == 200 {
            group.enter()
            self.displayData() { 
                // update the UI to display received data
                group.leave()
            }
            group.notify(queue: .main) {
                print("work done: \(Thread.current)")
            }
        }
    }.resume()
}
```

## Dispatch After 

[Dispatch After Documentation](https://developer.apple.com/documentation/dispatch/1452876-dispatch_after)

The `asynAfter()` fucntion invokes the block or closure on the dispatch queue that is passed into the function adter a specified delay.

The function declaration is shown below:

```swift
func asyncAfter(deadline: DispatchTime, qos: DispatchQoS = .unspecified, flags: DispatchWorkItemFlags = [], execute work: @escaping () -> Void)
```

Because `qos` and `DispatchQoS` have sensible default values we can simply use `asyncAfter()` like so 

```swift
DispatchQueue.asyncAfter(deadline: DispatchTime.now() + 2.0) {
    // code within this closure will run after 2 seconds 
}
```

In the code example above, we created an instance of `DispatchTime` and added 2 seconds (we used '2.0' because `Dispatch.now()` returns a double). 
After 2 seconds, the code in the execute closure will be run. 

## Dispatch Semaphore

[Dispatch Semaphore Documentation](https://developer.apple.com/documentation/dispatch/dispatchsemaphore)

Firstly what is a semaphore? 

Semaphores provide synchronised access to a shared resource by containing a: 

- **Counter** - lets the semaphore know how many threads can use the resource at once
- **Queue** - to store the list of threads requiring access to the shared resource
- **Wait()** - a method to request access to the shared resource
- **Signal()** - a method to release access to a shared resource

When a semaphore gets a `wait()` request, it will check the value of the counter. 
If the counter is zero, then it push the thread into the queue. 
If not, it provides access to the shared resource and reduces the counter by 1. 

On the other hand, when a semaphore gets a `signal()` request, it will check if it's queue has any threads in it.
If the queue has any threads, it will dequeue it and increment the counter by 1.

In the code example below, we will use the example of a small grocery store who has two checkout lanes, but only EFTPOS machine. This means that this shared resource (the EFTPOS machine) is a share resource. 
The three checkout lanes will be using dispatch semaphore to request access to the shared resource and bill customers. 

```swift
let concurrentQueue = DispatchQueue(label: "concurrent", attributes: .concurrent)
let semaphore = DispatchSemaphore(value: 1)
var eftpos = {(billingAmount: Int, laneNumber: Int) in 
    print("billed $\(billingAmount) at lane \(laneNumber)")
}

concurrentQueue.aync {
    print("lane 1 is currently scanning customer 1 products")
    print("lane 1 now needs to finalise payment using the eftpos")
    semaphore.wait()
    eftpos(25,1)
    semaphore.signal()
    print("lane 1 finished billing")
}

concurrentQueue.async {
    print("lane 2 is currently scanning customer 1 products")
    print("lane 2 now needs to finalise payment using the eftpos")
    semaphore.wait()
    eftpos(85,1)
    semaphore.signal()
    print("lane 2 finished billing")
}

```

What is happening in the code above ?

- We first create a concurrent queue which is an instance of `DispatchQueue`
- Then we create an instance of a `DispatchSemaphore` with a value of 1, meaning that only one thread can access this shared resource (eftpos machine) at a time 
- `semaphore.wait()` is added before calling `eftpos` and processing payments, because we are requesting access to the shared resource
- `semaphore.signal()` is called after billing has been made to increment the semaphore counter and letting the shared resource be used by other lanes

The resulting output of the code block above will be 

```
lane 1 is currently scanning customer 1 products
lane 2 is currently scanning customer 2 products
lane 1 now needs to finalise payment using the eftpos
lane 2 now needs to finalise payment using the eftpos
billed $25 at lane 1 
lane 1 finished billing
billed $85 at lane 2
lane 2 finished billing
```

## GCD Synchronous and Asynchronous Methods 

### .sync

A synchronous function only proceeds to the next function after the task completes. 
We can schedule a block of work synchronously by calling `DispatchQueue.sync(execute:)`

### .async 

An asynchronous functions orders the task from start and return immediately. 
Because it does not wait for the task to complete, it therefore does not block execution in the current thread from moving on the next function. 
We can schedule a block of work asynchronously by calling `DispatchQueue.async(execute:)`

## The Readers-Writers problem

The readers-writers problem is a common problem in concurrency. 

When constant are declared with the `let` keyword, they are read only no matter what thread they are in and so they are **thread-safe**.

However, variables that are declared as `var` are mutable. They are not thread-safe because the value of this variable can be modified in one thread while being read in another. 

## Dispatch Barriers

[Apple Dispatch Barrier documentation](https://developer.apple.com/documentation/dispatch/dispatch_barrier)

GCD uses dispatch barriers as a solution to the readers-writers problem. 

Dispatch Barriers are used to synchronise the excution of one or more tasks in your dispatch queue.

When you add a barrier to a concurrent dispatch queue, the queue will delay the execution of any task submitted after the barrier until all previous tasks have finished executing. 

To add a barrier, add `.barrier` onto the execute method under the parameter `flag`, e.g. `myQueue.async(flag: .barrier)`.

```swift
final class Messenger {

    private var messages: [String] = []

    private var queue = DispatchQueue(label: "messages.queue", attributes: .concurrent)

    var lastMessage: String? {
        return queue.async {
            messages.last
        }
    }

    func postMessage(_ newMessage: String) {
        queue.async(flags: .barrier) {
            messages.append(newMessage)
        }
    }
}

let messenger = Messenger()
// Executed on Thread #1
messenger.postMessage("Hello Arqadian!")
// Executed on Thread #2
print(messenger.lastMessage) // Prints: Hello Arqadian!
```

In the example above, the barrier flag will stop any read/write access to the value it is changing (in this case `messages`).
It will get in the way of parallel tasks and makes the concurrent queue a serial queue for a moment.

The barrier will place a "lock" on reading the `lastMessage` variable until the write activity `postMessage` is completed. 
After all tasks before the barrier flag is completed, the queue will execute the barrier block and resumes it's normal execution behaviour.

In the new Swift concurreny [async/await](./AsynchronousSynchronous.md) the use of dispatch barriers is replaced by Actors. 

## Combinations of Queues 

|  | Serial | Concurrency | Main |
| --- | ----------- | ----------- | ------ |
| Sync |  No new threads, execute serially  | Multiple threads running, tasks are executed serially on each thread | No new thread, executed serially, large tasks may block UI |
| Async | No new threads, tasks are executed one at a time | Multiple threads running, tasks running asynchronously unless blocked | Serial execution of tasks on the main thread, large tasks may block UI |


## References and Resources 

- [Swift concurreny comparison with the threading model](https://www.donnywals.com/wwdc-notes-swift-concurrency-behind-the-scenes/)
- [Swift GCD](https://kentakodashima.medium.com/swift-grand-central-dispatch-gcd-80bcb16a147f)
- [Concurrent vs Serial DispatchQueue](https://www.avanderlee.com/swift/concurrent-serial-dispatchqueue/)
- [Dispatch Groups in Swift](https://betterprogramming.pub/a-deep-dive-into-dispatch-groups-8251bbb8b001)
- [Sequencing with Dispatch Groups](https://betterprogramming.pub/a-deep-dive-into-dispatch-groups-8251bbb8b001)
- [Understanding Dispatch Semaphore](https://medium.com/@kiransep21/semaphores-in-swift-e493ac853072)

