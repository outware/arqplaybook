#  UIKit
[Apple documentation - UIKit](https://developer.apple.com/documentation/uikit)

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [UIKit vs SwiftUI](#markdown-header-uikit-vs-swiftui)
- [How to Use](#markdown-header-how-to-use)
- [Interface Builder](#markdown-header-interface-builder)
- [Constraints](#markdown-header-constraints)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

UIKit is an Apple framework that provides the required infrastructure for your iOS application, including a library of objects.

It provides the window and view architecture for implementing an interface, the event handling for app inputs, and the main run loop needed to manage interactions between the system, app and user:

![Core app objects](../Assets/CoreObjects.png)

It also has animation support, document support, drawing and printing support, information about the current device, text management and display, search support, accessibility support, app extension support, and resource management.

Use UIKit classes from the main thread or main dispatch queue only, unless otherwise indicated - particularly where the work involves the User Interface (UI) or classes derived from UIResponder (see [Multi-threading](./Multi-threading.md]). 

## UIKit vs SwiftUI

When creating an application in Xcode, you will be prompted to select a framework - either UIKit, or SwiftUI.

Once a framework is selected, you will also need to select the language that you will use. UIKit supports both Objective-C and Swift languages, while SwiftUI supports only Swift.

While SwiftUI is the newer framework, most applications are still being made or are using UIKit.

## How to use

[Apple documentation - About App Development with UIKit](https://developer.apple.com/documentation/uikit/about_app_development_with_uikit)

To implement UIKit in any file within your project, simply import the kit at the top of your required file `import UIKit` to have access to the framework.

The full range of objects contained within the UIKit library is then programatically accessible from whichever file UIKit has been imported; for example, UIViewController (and all the subclasses of this), UIView (and all the subclasses of this) etc.

## Interface Builder

The interface builder is a way of placing components in your app visually via Storyboards.

![StoryBoard](../Assets/StoryBoards.jpeg)

Add components to the storyboard by accessing the object library provided in Xcode:

- click the "+" button at the top right-hand corner of Xcode (or press command + shift + L)

- Select the object (e.g Label) you wish to add from the library, and drag it onto the storyboard. 

![Object Library](../Assets/Object-library.jpeg)


To **Connect** components via storyboard

- name component in the Identity inspector tab - under Identity > Restoration ID

- split your Xcode view in two

- click on the component in the document outline and drag it into your viewController file

- this will make and `@IBOutlet` in your viewController file which you will have to name

To **Connect Programmatically** (recommended method)

- name component in the Identity inspector tab - under Identity > Restoration ID

- in your associated view Controller create an outlet for your component ( you will have to name) e.g `@IBOutlet var welcomeLabel: UILabel!` - you know that the label exists so it is safe to force unwrap it with the "!"

- Go back to the storyboard file and right click on the viewController file in the document outline on the left side of the screen

![Document Outline](../Assets/Document-outline.png)

- A Popup will appear find `welcomeLabel` and click the dot beside it and drag to connect it to the label component either on the storyboard itself or within the document outline.

To **Access** Components

- In the viewController you now have the variable that you named when connecting the component via the outlet e.g `welcomeLabel`

- Now we need to assign text to this label. `welcomeLabel.text = "Hello World"`

## Constraints

Within the storyboard any component you place needs to clearly understand where it is placed on the storyboard, to do this we give it constraints. Whether telling it to be aligned both horizontally and vertically in the container (centre, centre), or how far it should be away from other components or the edge of the viewing area.

To apply a constraint to any component click on the component and then go to the constraints menu in the bottom right corner of XCode or you can go to the **Size Inspector** in the toolbar at the right hand side of XCode. Once applied these constraints can be seen in the document outline panel on the left side of your screen.

![Constraint Menu](../Assets/Constraints.jpeg)

**NOTE** - While applying constraints is a fairly simple process, making them work for you has its challenges. The best way to understand them is to use them and do not be afraid to delete constraints and recreate them if something isn't working right.

## References and Resources
- https://developer.apple.com/documentation/uikit
- https://developer.apple.com/documentation/uikit/about_app_development_with_uikit



