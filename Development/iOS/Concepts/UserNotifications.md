# User Notifications

[Apple documentation - User Notifications](https://developer.apple.com/documentation/usernotifications/)

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [Local Notifications](#markdown-header-local-notifications)
- [Permission from User](#markdown-header-permission-from-user)
- [Critical Notifications](#markdown-header-critical-notifications)
- [Creating Notification Content and Scheduling Notifications](#markdown-header-creating-notification-content-and-scheduling-notifications)
- [Cancel Scheduled Notification Request](#markdown-header-cancel-scheduled-notification-request)
- [Remote (push) Notifications](#markdown-header-remote-push-notifications)
- [How do Push Notifications Work](#markdown-header-how-do-push-notifications-work)
- [Configuring Application to Enable Push Notifications](#markdown-header-configuring-application-to-enable-push-notifications)
- [Registering App with APNs](#markdown-header-registering-app-with-apns)
- [Sending Simulated Push Notifications](#markdown-header-sending-simulated-push-notifications)
- [A Basic Push Notification](#markdown-header-a-basic-push-notification)
- [Handling Push Notifications](#markdown-header-handling-push-notifications)
- [Server Side Setup For Push Notifications](#markdown-header-server-side-setup-for-push-notifications)
- [References and Resources](#markdown-header-references-and-resources)


## Introduction

User-facing notifications communicate important information to users of your app, regardless of whether your app is running on the users device. 

Notifications can also tell your app to download information and update its interface. Notifications can display an alert, play a sound, or badge the app’s icon (see [options](https://developer.apple.com/documentation/usernotifications/unauthorizationoptions)).

You can generate notifications locally from your app or remotely from a server that you manage. 

## Local Notifications

The app creates the notification content and specifies the conditions (e.g. time or location) that trigger the delivery of the notification. To create and manage local notifications, you need the following steps:

1. Authorisation
1. Content creation
1. Scheduling the notifications
1. Managing the notifications
1. Handling actions

For an in-depth explanation with an example of how these steps are done look [here](https://www.raywenderlich.com/21458686-local-notifications-getting-started)

### Permission from User

[Apple documentation - UNUserNotificationCenter](https://developer.apple.com/documentation/usernotifications/unusernotificationcenter)

[Apple documentation - User Notification Permissions](https://developer.apple.com/documentation/usernotifications/asking_permission_to_use_notifications)

You'll need to ask the user for permission to access certain things on their device and to send them notifications.

`UNUserNotificationCenter` handles all local notification-related behaviour in your app, including requesting authorisation, scheduling delivery and handling actions. 

Its method `.requestAuthorization` will prompt the user to grant or deny authorisation and records the user's response:

```swift
let notificationCenter = UNUserNotificationCenter.current()
notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
    if let error = error {
    // Handle the error here.
    }
    // other code
}
```
    
`UNUserNotificationCenter` is a singleton, so you cannot create instances of it directly in this way:

```swift
let notificationCenter = UNUserNotificationCenter() // ERROR ERROR ERROR - DO NOT DO THIS!!
```

Instead, you will need to `.current` gets the reference:

```swift
let notificationCenter = UNUserNotificationCenter.current()
```

**Check Authorisation Status**

Always check you app's authorisation status before scheduling local notifications, as users can change it at any time.

To request the app's authorised notifications settings (get details on what the app is authorised to do in terms of notifications), call on the`UNUserNotificationCenter`'s method `getNotificationSettings(completionHandler:)`. This takes a completion handler, which are typically escaping [closures](./Closures.md).


    func getNotificationSettings(completionHandler: @escaping (UNNotificationSettings) -> Void)

All this means that work in the `getNotificationSettings` method may be performed on a background thread while other work continues on the main thread. 

Before Async/Await in Swift 5.5, the [Grand Central Dispatch](./Multi-threading.md) (GCD) Apple API was used to manage asynchronous operations. Using GCD's `DispatchQueue` and its `.async` method, we can bring back the task to the main thread once any background work is complete:

```swift
func someFunction() {
  UNUserNotificationCenter.current().getNotificationSettings { settings in
    DispatchQueue.main.async {
      self.settings = settings
    }
  }
}
```

If the user has authorised notifications, the value `.authorized` will be passed as a parameter to the completion handler, which takes arguments of type `UNNotificationSettings`. 

### Critical Notifications

Critical alerts are those that bypass the do-not disturb and ringer switch and play a sound. They are quite disruptive and so not all apps are allowed to have one. 

To have one, you can apply for entitlement on the Apple Developer Portal. These are usually critical alerts e.g. for health, medical, security or public safety reasons. 

See this [tutorial](https://medium.com/@shashidharyamsani/implementing-ios-critical-alerts-7d82b4bb5026) for information on how to implement critical notifications if you need them.

### Creating Notification Content and Scheduling Notifications

[Creating and Scheduling Apple Notifications](https://developer.apple.com/documentation/usernotifications/scheduling_a_notification_locally_from_your_app)

You can create and schedule local notifications using any of the following triggers

- Time Interval (`UNTimeIntervalNotificationTrigger`)
- Calendar (`UNCalendarNotificationTrigger`)
- Location (`UNLocationNotificationTrigger`)

1. fill in the properties of a `UNMutableNotificationContent` object with the details of the notification. `UNMutableNotificationContent` 
contains many fields such as `title`, `body`, `sound` etc. The fields you fill in define how the system delivers your notifications. The Apple developer documentation contains a  [list](https://developer.apple.com/documentation/usernotifications/unmutablenotificationcontent) of the different fields you can use.

```swift
let content = UNMutableNotificationContent()
content.title = "Weekly Staff Meeting"
content.body = "Every Tuesday at 2pm"
```
    
2. Specify the trigger for delivery - one of the 3 ways to create and schedule notifications shown above

e.g. with `UNCalendarNotificationTrigger`

```swift
var dateComponents = DateComponents() // Configure the recurring date
dateComponents.calendar = Calendar.current

dateComponents.weekday = 3  // Tuesday
dateComponents.hour = 14    // 14:00 hours
```
               
let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true) // Create the trigger as a repeating event

3. Create and register a notification request by creating a `UNNotificationRequest` object that includes your content and trigger condition, then call the `add(_:withCompletionHandler:)` method to schedule the request:

```swift
let uuidString = UUID().uuidString 
let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
let notificationCenter = UNUserNotificationCenter.current() // Schedule the request with the system

notificationCenter.add(request) { (error) in
    if error != nil {
    // Handle any errors.
    }
}
```
### Cancel Scheduled Notification Request

Once a notification request is triggered, a notification request remains active until the trigger condition is met, or it is explicitly cancelled. 

To cancel an active notification request, call the `removePendingNotificationRequests(withIdentifiers:)`method within `UNUserNotificationCenter`:

```swift
UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifier)
```
    
`identifier` here would be the same identifier used in creating a `UNNotificationRequest`.


## Remote (push) Notifications 

When applications are closed, push notifications allow developers to reach users and perform small tasks - even when the user is
not actively using the app. They are essentially messages sent to your app through the Apple Push Notification service (APNs).

Push notifications can be used for: 

- Setting a badge number on the app icon
- Playing a notification sound 
- Displaying a short message to draw attention to the app 

To send and receive push notifications, you must perform three tasks:

- Configure your app and register it with APNs
- Send a push notification from a server to specific target devices via APNs
- Use callbacks in the app to receive and handle push notifications

Sending push notifications is the responsibility of the app's server component.

### How Do Push Notifications Work

[Setting up a Remote Notification Server Apple Documentation](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server)

![Remote Notifications diagram](../Assets/PushNotificationsDiagram.png)

Remote notifications begins within your app's server. You decide which notifications to send to users and when to send them. 

When it is time to send a notification, you can generate a request that contains the notification data and a unique identifier for
the user's device. 

You then forward your request to the APNs (Apple's server), which handles the delivery of the notification to the user's device. When the 
user's device receives the notification, their operating system handles any user interactions and delivers the notification to your app. 

### Configuring Application to Enable Push Notifications

[Xcode enabling Push Notification Capability](https://help.apple.com/xcode/mac/current/#/devdfd3d04a1)

Go to the project navigator (top-most project file on the left panel) and change the **Bundle Identifier** to something unique, so the APNs server
can send direct pushes to your app. 

Then go create an AppID in your developer account to enable push notifications.

Next click the **Signing and Capabilities** tab on the project navigator and click "+ Capability" and type "push" in the
filter field and press enter. 

![Enable Push Notifications Capability on Xcode](../Assets/enablePushNotifications.png)

### Registering App with APNs

1. Get permission to show notifications (a similar way permission for local permissions was done) 
2. Register device to receive push notifications 
 
In the function where you get notification settings (e.g. getNotificationSettings()) within **AppDelegate.swift** after checking the user hasas authorised notifications on `settings.authorizationStatus`, call on the  `registerForRemoteNotifications()` method.

```swift
func getNotificationSettings() {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      print("Notification settings: \(settings)")
      guard settings.authorizationStatus == .authorized else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
}
```

3. Receive a **device token** (like an "address" of your app on the current device)

If registration was successful, you will receive the token in your App Delegate's `application(_:didRegisterForRemoteNotificationsWithDeviceToken:)` method:

```swift
func application(_ application: UIApplication,
                didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
  let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
  let token = tokenParts.joined()
  print("Device Token: \(token)")
}
```

This method will be called whenever `registerForRemoteNotifications()` succeeds.

It is taking the received `deviceToken` and converting it into a string. 

The `deviceToken` is the one that uniquely identifies this app on this particular device.

In addition to handling successful APNs registrations, prepare to handle unsuccessful registrations by implementing the `application(_:didFailToRegisterForRemoteNotificationsWithError:)` method.

```swift
func application(
  _ application: UIApplication,
  didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register: \(error)")
      // proper error handling
}
```

Upon receiving a device token, open a network connection from your app to your server. 

Securely forward the device token and the information you need to identify the user (e.g. username ) - something that connects them to your service. 

On your server, store the tokens securely, and access them to send notifications. 


### Sending Simulated Push Notifications 

Use a text editor to create a `.apn` file filled with your JSON text of a push notification.

Use Xcode's **simctl** utility (a command-line tool that provides you the interface to use and configure your iOS simulator in a programmatic way) to simulate a push notification. 

For more instructions take a look [here](https://medium.com/swlh/simulating-push-notifications-in-ios-simulator-9e5198bed4a4).

### A Basic Push Notification

The payload for a push notification is a JSON dictionary that contains at least one **aps** item (a dictionary holding the notification payload to be sent to the APN server). 

Here `aps` contains the fields `alerts` (The alert headline text), `sound` (the sound the push notification makes when delivered) and other custom fields that get delivered
to the application where you can handle/ use it. 

There are eight built in keys to add to the `aps` dictionary in Apple's developer [ documentation](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/generating_a_remote_notification#2943360)
```json
{
  "aps": {
    "alert": "Breaking News!",
    "sound": "default",
  }
}
```

### Handling Push Notifications

When your app receives a push notification, a method in `UIApplicationDelegate` is called.

Depending on the state of your app, you will need to handle notifications differently. 

- If your app was not running and the user launches it by taping on the push notifications, the notifications will be passed into the `launchOptions` of
 `application(_:didFinishLaunchingWithOptions:)` (within the AppDelegate.swift file):

```swift
// Check if launched from notification
let notificationOption = launchOptions?[.remoteNotification] 
    if let notification = notificationOption as? [String: AnyObject], 
        let aps = notification["aps"] as? [String: AnyObject] {
    // launch the controller that displays the action etc. 
}
```

- If your app was running in either the foreground or background, the system notifies your app by calling `application(_:didReceiveRemoteNotification:fetchCompletionHandler:)`

### Server Side Setup For Push Notifications

As more customers register to receive notifications, you will need to somehow store their device tokens to send them notifications.

To add push notifications to your app, it is also important to consider the server side setup. 
This can be done through a Vapor server side setup or through third party services.

Some popular third party services that can be used include:

- Amazon Simple Notification Service (SNS)
- Braze
- Firebase Cloud Messaging
- Kumulos
- OneSignal
- Urban Airship

This will not be discussed here, but the following resources below can help you get started

1. [Learn how to send push notifications through a Swift Server API - Vapor](https://www.raywenderlich.com/11238593-sending-push-notifications-with-vapor)
2. [Learn how to set up Firebase as a third party server for push notifications](https://medium.com/@fatih.aktepe92/how-to-implement-server-side-event-driven-push-notifications-in-ios-3d757bea398c)

## References and Resources

1. Local Notifications

- [Getting Started with Local Notifications](https://www.raywenderlich.com/21458686-local-notifications-getting-started)
- [Implementing iOS Critical Alerts](https://medium.com/@shashidharyamsani/implementing-ios-critical-alerts-7d82b4bb5026)
- [Scheduling Notification Locally](https://developer.apple.com/documentation/usernotifications/scheduling_a_notification_locally_from_your_app#2980225)
- [Scheduling TimeInterval Notification Example](https://www.hackingwithswift.com/books/ios-swiftui/scheduling-local-notifications)

2. Push Notifications 

- [Push Notification Tutorial](https://www.raywenderlich.com/11395893-push-notifications-tutorial-getting-started#toc-anchor-008)
- [Sending Notification Requests to APNs](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/sending_notification_requests_to_apns/)
- [Simulating Push Notifications in iOS Simulator](https://medium.com/swlh/simulating-push-notifications-in-ios-simulator-9e5198bed4a4)
