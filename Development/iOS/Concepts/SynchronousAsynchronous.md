# Synchronous and Asynchronous

## Table of Contents
- [Procedural Programming vs Concurrency](#markdown-header-procedural-programming-vs-concurrency)
- [Procedural](#markdown-header-procedural)
- [Concurrency](#markdown-header-concurrency)
- [How was Concurrency Handled Before Async/Await?](#markdown-header-how-was-concurrency-handled-before-async-await)
- [How is the New Async/Await Concurrency Model different?](#markdown-header-how-is-the-new-async-await-concurrency-model-different)
- [Async and Await](#markdown-header-async-and-await)
- [Async](#markdown-header-async)
- [Await](#markdown-header-await)
- [A Different Approach: Calling Asynchronous Functions and Running it in Parallel with Other Code](#markdown-header-a-different-approach-calling-asynchronous-functions-and-running-it-in-parallel-with-other-code)
- [Implementation](#markdown-header-implementation)
- [Actors in Async/Await](#markdown-header-actors-in-async-await)
- [How do Actors Prevent Data Races](#markdown-header-how-do-actors-prevent-data-races)
- [Preventing Unneeded suspensions](#markdown-header-preventing-unneeded-suspensions)
- [Nonisolated access within Actors](#markdown-header-nonisolated-access-within-actors)
- [Replacing completion handlers](#markdown-header-replacing-completion-handlers)
- [Common errors in implementing async/await](#markdown-header-common-errors-in-implementing-async-await)
- [References and Resources](#markdown-header-references-and-resources)

## Procedural Programming vs Concurrency

Procedural code runs from top to bottom. Control is given back to the caller through return.

Callback-based concurrency will create asynchronous tasks, but it will continue executing the current thread without an issue, even if those tasks are running. Control is given back to the caller through completion handlers.

### Procedural

This is code that simply executes in the order that is written, line by line.

When you write any normal program with no exceptional needs (e.g. networking, I/O etc), your program executes procedurally. 

This means functions and methods that return a value can return that value on the last line, as all the lines above it have already executed and performed whatever tasks they needed to.

### Concurrency

Where you have code that may run in parallel with other code ('concurrently') it gets more complicated.

Often 'call-backs' are used to perform some further action only after the rest of the code has executed. These are functions that are invoked inside a function, and passed into the parent function as a parameter. Often they appear in swift as 'closures' and/or 'completion handlers'.

These are asynchronous - meaning, the code may execute **after** code that is physically on a line below it.

### How was Concurrency Handled Before Async/ Await? 

Before the release of Async/Await in Swift 5.5, the [Grand Central Dispatch](./Multi-threading.md) (GCD) Apple API was used to manage asynchronous operations. 

This is accomplished through `DispatchQueues` and [multithreading](./Multi-threading.md), where completion handlers (closures) are called once asynchronous tasks are completed and results are returned.

`DispatchQueue` executes tasks asynchronoulsy using the built-in `async(execute: DispatchWorkItem)` method found in the `DispatchQueue` class.
 
The `DispatchQueue` class has other methods such as ` sync(execute: DispatchWorkItem)` which is used when you need to wait for something done on a different thread and only continue executing once that is completed.
 
On the other hand, the `async` label used in function declarations with the new Swift concurrency framework is a keyword that indicates the function performs an asynchronous task. 

### How is the New Async/ Await Concurrency Model different? 

The new async/await model is built on top of threads (GCD), but you do not interact with them directly. 

Because Swift runs on preemptive threads, where the operating system manages and assigns tasks to the different threads available, Swift can give up a thread it's running on,
and instead let another asynchronous function run on that thread (e.g. when the first task is taking too long). 

## Async and Await

Async/await is like a hybrid between procedural programming and callback-based closures.

### Async 

Async is short for 'asynchronous'. 

It is a method attribute with two uses:

- to tell the compiler when a piece of code is asynchronous.
- to spawn asynchronous tasks in parallel. 

It is placed after the method's round brackets and before the arrow, like this:

```swift
func fetchImages() async -> [UIImage] {
    // .. perform data request
}
```

or: 

```swift    
func fetchImages() async throws -> [UIImage] {
    // .. perform data request
}
```

async code can only run in **concurrent** contexts (within other async functions, or when manually dispatched via Task {}).

### Await 

The async and await keywords must always be used together.

Await is used for calling methods that have been marked as asynchronous. 

To do this, the keyword 'await' is placed before the invocation of the method:

```swift
do {
    let images = try await fetchImages()
    print("Fetched \(images.count) images.")
} catch {
    print("Fetching images failed with error /(error)")
    // statements
}
```
 

Whenever the program finds the await keywords, it has the option of suspending the function. It may or may not do so, and the decision is up to the system. If the system does suspend the function, await will return control, not to the caller, but to the system. The statements below await will not be executed until it has finished. 

When the thread gets blocked on an await, it can still be used to execute other tasks whilst waiting, e.g. for the main thread this usually means processing more user input, but the background thread can be reused in other tasks if they stop on the `await` as well.

In the above example, we are telling our program to await a result from `fetchImages()`and only continue once the result has been returned. 

The system decides what’s important to execute, and at some point, it will return control back to you after it sees the awaited function has finished.

### A Different Approach: Calling Asynchronous Functions and Running it in Parallel with Other Code

Another approach to calling asynchronous functions while letting it run in parallel with other code around it is to write `async` in 
front of `let` when you define a constant. After doing this, you then write `await` each time you use the constant.

In the example below, `downloadPhoto()` is an asynchronous function. 

```swift
async let firstPhoto = downloadPhoto(named: photoNames[0])
async let secondPhoto = downloadPhoto(named: photoNames[1])
async let thirdPhoto = downloadPhoto(named: photoNames[2])

// other code ... 

let photos = await [firstPhoto, secondPhoto, thirdPhoto]
show(photos)
```
    
In the code above, none of the three calls to `downloadPhoto()` were marked with `await`, so the code does not suspend and wait for the function's result. 

Instead if there are enough resources (e.g. preemptive threads available), the program can run the `downloadPhoto()` methods in parallel with the next lines of code. 

In the example above, execution will continue until the line where `photos` is defined. Here the program needs the results from these asynchronous calls, so you write the 
keyword `await` to pause execution until all three photos are finish downloading before continuing the execution of the code after this.  

Here is how you can think about the differences of the 2 approaches:
 
- Call asynchronous functions with `await` when the code on the following lines depends on that function’s result. This creates work that is carried out sequentially. (The next line of code waits for the previous line before continuing)
- Call asynchronous functions with `async-let` when you don’t need the result until later in your code. This will then create a task and carry out the task in parallel with other code if there are preemptive threads available (resources available to do so). 
- Both `await` and `async-let` allow other code to run while they’re suspended.
- In both cases, you mark the possible suspension point with `await` to indicate that execution will pause, if needed, until an asynchronous function has returned.

### Implementation

There are some straightforward, specific rules about the way async functions work:

- Synchronous functions cannot simply call async functions directly – it wouldn’t make sense, so Swift will throw an error. This means you will need to chain `async` function declarations.

```swift
func viewFirstImage() async -> UIImage {
        let images = await fetchImages()
        let firstImage = images[0]
        displayImage(firstImage) // synchronous function
}
```

Async functions need to be called from functions that themselves are markes as `async` or from within a Task. 

Because we want to call on the asynchronous function `fetchImages()` we must label the current function `viewFirstImage()` as an async function as it calling an asynchronous function. 
Declaring the `viewFirstImage()` function as such `func viewFirstImage() -> UIImage {...}` will not build successfully. 

- Async functions can call other async functions, but they can also call regular synchronous functions if they need to. (In the example above, the synchronous `displayImage()` function is called in an async function)
- If you have async and synchronous functions that can be called in the same way, Swift will prefer whichever one matches your current context – if the call site is currently async then Swift will call the async function, otherwise it will call the synchronous function.

Async functions and initialisers can throw errors if needed, so they are often used with `do/catch`. 

Functions that may throw an error should also be marked with the 'throw' keyword. After the round brackets, an async function that can throw an error should read 'async throws' (and then the return type):

```swift
func save(users: [String]) async throws -> String {
    let savedUsers = users.joined(separator: ",")

    if savedUsers.count > 32 {
        throw UserError.dataTooLong
    } else {
        // Actual saving code would go here
        return "Saved \(savedUsers)!"
    }
}
```

When calling the method 'try await' is in front of the method invocation:

```swift
func updateUsers() async {
    do {
        let users = try await fetchUsers(count: 3)
        let result = try await save(users: users)
        print(result)
    } catch {
        print("Oops!")
    }
}
```

## Actors in Async/ Await 

Actors are used to avoid issues with [data races](https://www.avanderlee.com/swift/thread-sanitizer-data-races/#what-are-data-races), which was a common issue developers run into when handling asynchronous code. 

```swift
private var name: String = ""

func udpateName() {
    DispatchQueue.global().async {
        self.name.append("Potter")
    }
    
    // Executed on the main thread
    print(self.name)
}
```

In the example above, two pieces of code, on two different threads are accessing the same `String` property.
 
As the background thread `global()` is writing the name, the behaviour is unpredictable, the print statement could either
return the empty string or the udpated name string. This causes potential issues in code that might be hard to detect because it will behave differently everytime the code is run. 

**Actors** help protect the code from data races by enforcing limitations on the data, preventing concurrent access to mutable data (editable data, e.g. variables declared with `var`).

You can define an Actor using the `actor` keyword, similar to how you would with a `class` or a `struct`

```swift
actor Gryffindor {
    let mascot = "Lion"
    var numberOfWizards: Int = 80
}
``` 

Unlike structs, classes or actors are reference types, where an instance (a referenced copy) may be available from many different places in your code. 

When you pass a class or actor as a reference (the memory address), the original reference is being passed and shared in multiple places.

Actors are like other swift types, they can have intialisers, methods, properties etc. Their names are also capitalised, similar to class/ struct names.

Furthermore, unlike structs, an Actor also requires you to define a custom intialiser when you need to set instance variables from outside the actor at initialisation, however you can still declare variables with a default value and not define an initialiser within it.

Moreover, unlike classes, Actors do not support inheritance, meaning there is no need for things like convenience or required initialisers, `final` statements, overriding statements etc.
  
### How do Actors Prevent Data Races

Actors prevent data races by providing **synchronous access** to its isolated data (variables and functions). 
In other words, when one execution of code is using the `numberOfWizards` variable we had in our example above, it will "lock" the entire actor, including all functions that may or may not use any of the instance variables. 

Before Actors, Concurrent dispatch queues and barriers were used to handle access. 

```swift
func sortingHatSorts() {
        /// Using a barrier to stop reads while writing
        queue.sync(flags: .barrier) {
            numberOfWizards += 1
        }
    }
```

The code above would require a lot of maintainence, and the developer would need to carefully place these barriers as the compiler does not enfore it.

Actors use the same locking mechanism, but this is hidden under implementation details. By using actors, the Swift compiler can enforce synchronised access,
preventing data races most of the time. 

Implementing the function above into an Actor class can be done this way:

```swift
actor Gryffindor {
    let mascot = "Lion"
    var numberOfWizards: Int = 80
    
    func sortingHatSorts() {
        numberOfWizards += 1
    }
} 
```

When trying to use or read the mutable properties and methods:

```swift
let gryffindor = Gryffindor()
gryffindor.sortingHatSorts() // This will be caught by the compiler
```
    
The mutable property `sortingHatSorts()` which mutates `numberOfWizards` can only be accessed **from within the Actor** without the `await` keyword. 

However, when wanting to call `sortingHatSorts()` (or any function even if they do not mutate) outside the actor, you must use an `await`, because you are locking the actor for exclusive access to one thread whilst `sortingHatSorts()` is being called.

However, we are allowed to access immutable properties from the Actor, such as `mascot`

```swift
print(gryffindor.mascot)
```

To access data/mutable properties, use async/await. 

```swift
let gryffindor = Gryffindor()
await gryffindor.sortingHatSorts()
print(await gryffindor.numberOfWizards)
```
    
### Preventing Unneeded suspensions

Take a look at the code below where we have added a new function `notifyObserver()` into our Actor class. 

```swift
actor Gryffindor {
    let mascot = "Lion"
    var numberOfWizards: Int = 80
    
    func sortingHatSorts() {
        numberOfWizards += 1
    }
    
    func notifyObservers() {
        print("New member added to Gryffindor!")
    }
} 
```
    
We could run this code:

```swift
let gryffindor = Gryffindor()
await gryffindor.sortingHatSorts()
await gryffindor.notifyObservers
```
    
However, we could also optimise this code by instead calling `notifyObservers()` from within `sortingHatSorts()` as such: 

```swift
actor Gryffindor {
    // ...
    
    func sortingHatSorts() {
        numberOfWizards += 1
        notifyObservers()
    }
    
    // ...
}
```
    
This is because we are within the Actor and have synchronised access, we don't need another `await`.

### Nonisolated access within Actors

Sometimes our Actor's properties/functions do not require the `await` keyword such as the function `notifyObservers()` in the example above.
 
When you are accessing isolated properties from within the Actor itself, e.g. accessing `notifyObserver()` from the `sortingHatSorts()`
method inside the `Gryffindor` Actor, you are basically reusing your given access, so make the most out of it!

There are also cases where it is not required to have isolated access. For example, if we add a `revealMascot()` method: 

```swift
actor Gryffindor {
    // ...
    
    func revealMascot() {
        print("Gryffindor's mascot: \(mascot)")
    }
    
    // ...
}
```

The `revealMascot()` method is only accessing the immutable `mascot` property within our Actor.
 
Despite this, in using this function, the compiler expects an `await` keyword 

```swift
let gryffindor = Gryffindor()
await gryffindor.revealMascot()
``` 

This does not make sense. Instead, we can use the keyword `nonisolated` to mark our method and tell Swift that it is not accessing isolated data and avoid using the `await` keyword:

```swift
actor Gryffindor {
    // ...
    
    nonisolated func revealMascot() {
        print("Gryffindor's mascot: \(mascot)")
    }
    
    // ...
}
    
let gryffindor = Gryffindor()
gryffindor.revealMascot() // notice how the keyword "await" is no longer used
```

**Note** that `nonisolated` keyword can be used for computed properties as well, but is not needed on immutable properties (`let` variables ).

### Replacing completion handlers

Async methods can replace closure completion callbacks, which are also commonly used in Swift to complete an asynchronous task. You may want to do this because completion handlers have some downsides:

- The completion closures must be repeated in each method exit (or the app could be endlessly waiting for a result.). 
- Closures are hard to read, as the flow can move back and forth between other methods and code can be heavily nested.
- Closure order of execution can be hard to reason about.
- Retain cycles have to be avoided by using [weak references](https://vinodhswamy.medium.com/strong-cycle-retain-cycle-in-swift-f452f07518b2) (e.g. [weak self]).

Async and await, which was introduced as part of the structured concurrency changes in Swift 5.5, make it easier to see and manage the order of execution of the code. Methods are linearly executed without going back and forth like you would with closures and completion handlers.

This can be beneficial in tasks where you would otherwise have to chain a series of events to pass a value between objects, such as making an HTTP request from an API and returning some data:

[WWDC2021 - Use async/await with URLSession](https://developer.apple.com/videos/play/wwdc2021/10095/)

Apple provides [specific instructions](https://developer.apple.com/documentation/swift/swift_standard_library/concurrency/updating_an_app_to_use_swift_concurrency) for refactoring completion handlers to async await methods.

### Common errors in implementing async/ await

There are two common errors you might run into when implementing async/await for the first time.

1. "'async' call in a function that does not support concurrency" error

    - solve this error by using a `Task.init` method to call the async method from a new task that does support concurrency

```swift
func fetchData() {
    Task.init {
        do {
            self.images = try await fetchImages()
        } catch {
            // .. handle error
        }
    }
}

```
    
2. "Reference to capture parameter 'self' in concurrently-executing code" error
 
    - We are trying to reference an immutable instance of `self` 
    - In this case, you are likely referencing either a property or an instance that is immutable
    - Fix this issue by making your properties mutable or by changing the struct into a reference type, like a class


## References and Resources 
- https://developer.apple.com/documentation/swift/swift_standard_library/concurrency/updating_an_app_to_use_swift_concurrency
- https://www.andyibanez.com/posts/understanding-async-await-in-swift/
- [Concurrency with Async/Await - https://docs.swift.org/swift-book/LanguageGuide/Concurrency.html](https://docs.swift.org/swift-book/LanguageGuide/Concurrency.html)
- https://www.avanderlee.com/swift/async-await/
- https://www.hackingwithswift.com/articles/233/whats-new-in-swift-5-5
- https://developer.apple.com/videos/play/wwdc2021/10095/
- https://www.avanderlee.com/swift/actors/
- https://www.avanderlee.com/swift/thread-sanitizer-data-races/#what-are-data-races

