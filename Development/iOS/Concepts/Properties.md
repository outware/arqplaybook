# Properties

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Declaring Properties](#markdown-header-declaring-properties)
- [Stored vs Computed Properties](#markdown-header-stored-vs-computed-properties)
- [Computed Property](#markdown-header-computed-property)
- [Common Modifiers](#markdown-header-common-modifiers)
- [Static](#markdown-header-static)
- [Strong / weak](#markdown-header-strong-weak)
- [Unowned](#markdown-header-unowned)
- [Final](#markdown-header-final)
- [Private / file-private / internal / public](#markdown-header-private-file-private-internal-public)
- [Property Observers](#markdown-header-property-observers)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

In Swift, properties associate values with a particular class, structure, or enumeration.

They are usually associated with an instance of that object, and can store or compute values. 

They can also be associated with the type itself (see 'Common Modifiers - Static' below).

**NOTE: Objective-C property attribute**

The Objective-C language was used extensively in iOS prior to Swift and is still the basis for many objects in iOS. Note that in this language, the @property is required to configure how an object can be exposed if it is intended to be used outside the class. It is basically an accessor method. 

While Objective-C provides two ways to store values and references as part of a class instance (in addition to properties, instance variables could be used as a backing store for the values stored in a property), Swift unified these concepts into a single property declaration.

## Declaring Properties

Swift properties are constants (`let`) or variables (`var`) declared in the context of a class, structure or enumeration.

They can be declared with the data type explicitly defined but without a value, or with a value using type inference. 

e.g. `let name: String` or `let name = "Sebastian"`

Below is an example of declaring, within a struct, variable properties that have a type but no value yet:

```swift
struct SomeStruct {
    var aString: String
    var topSpeed: Double
}
```
    
Property values can be accessed and set (if accessible variables) using dot notation:

```swift
var myStruct: SomeStruct
myStruct.aString = "The top speed is"
myStruct.topSpeed = 2.0
```

Below is an example of instead declaring variable properties that have a value using type inference:

```swift
struct SomeStruct {
  var topSpeed = 2.0
  var aString = "The top speed is"
}
```

In both examples, the following code would print "The top speed is 2.0":

```swift
print(myStruct.aString, myStruct.topSpeed)
```

Access modifiers (see below) can modify the ability to access such properties.


## Stored vs Computed Properties

### Stored Property

Stored properties are variables and constants in classes and structures. 

They simply store a value, usually accessed or set via the instance of the class or struct.

Stored values can be read and updated by default - that is, they have 'setters' and 'getters' that do not need to be explicitly defined.

See the example code above which contains two stored properties: 'aString' and 'topSpeed'.

### Computed Property

Computed properties are provided by classes, structures, and enumerations. They do not simply store a value, but calculate or 'compute' the value based on other properties.

Computed properties are not able to be set by default. They have a getter (to read values) and an optional setter (to set values), so the setter needs to be defined explicitly.

This is an example of a computed property with a getter and a setter:

```swift
struct ContentViewModel {
    private var content: Content
    init(_ content: Content) {
        self.content = content
    }
    
    // computed property below
    var name: String {
        get {
            content.name
        }
        set {
            content.name = newValue
        }
    }
}

var content = Content(name: "arq", fileExtension: "png")
var viewModel = ContentViewModel(content)
viewModel.name = "ARQ"
print(viewModel.name) // Prints: "ARQ"
```
    
**When to use?**

You should use computed properties when the property:

- depends on other properties
- is defined inside an extension
- is an access point to another object, without disclosing the object fully

If the logic needs parameters, or involves heavy calculation that may take longer to execute, you may need to use a method instead.


NOTE: With subclasses, you can override read-only properties with computed properties. This can be useful in UIKit:

```swift
final class ViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
```

## Common Modifiers

### Lazy

A lazy stored property is a property whose initial value isn’t calculated until the first time it’s used.

```swift
class DataManager {
        lazy var importer = DataImporter()
        var data: [String] = []
    }

class DataImporter {
        var filename = "data.txt"
    }

let manager = DataManager()
```

In this example,the DataImporter instance for the importer property will only be created when the importer property is first accessed, such as when its filename property is queried ("data.txt" will print):

```swift
print(manager.importer.filename)
```

If a property marked with the lazy modifier is accessed by multiple threads simultaneously and the property hasn’t yet been initialised, there’s no guarantee that the property will be initialised only once

### Static

Swift allows us to use a "static" prefix on methods and properties to associate them with the type that they’re declared on rather than the instance. 

A very common use case for static properties is configuration, particularly in the UIKit framework. This can reduce code and make certain configuration elements immutable; for example, by defining matters of formatting or style using static properties within a configuration function.

```swift
enum Colors {
    static let mainColor = UIColor(red: 1, green: 0.2, blue: 0.2, alpha: 1)
    static let darkAccent = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
  }
```

Another use of static properties is to use them as a cache when the object created is expensive e.g. a date formatter:

```swift
struct Blogpost {
  private static var dateFormatter = ISO8601DateFormatter()

  let publishDate: Date?
  // other properties

  init(_ publishDateString: String /* other properties */) {
    self.publishDate = Blogpost.dateFormatter.date(from: publishDateString)
  }
}
```

### Strong / weak 

The default reference to a property will be strong. 

In order to prevent tight coupling, for example when referencing a delegate in the ViewController, use the property attribute 'weak' so that after the view controller has gone out of the heap or stack, that reference is not retained in memory:

```swift
final class myClass {
    var weak displayDelegate: DisplayDelegate
}
```

If `weak` is not used, it will hold onto the view controller strongly, not allowing both the reference to the view controller and the delegate to be deallocated. 

Instead, through the use of `weak`, if no other objects have a strong reference to the instance, it will leave the heap or stack.

There is some debate over the use of 'weak' for IB outlets (e.g. for buttons, images, views etc in the UI). Apple uses both.

ARQ's current practice is to use strong IB outlets (select this option when creating the outlet).

### Unowned

Unowned variables are similar to weak variables in that they provide a reference to data without having ownership, they also have the same purpose as `weak` references - to prevent memory retain cycles. 

However, weak variables can be `nil` - they are basically optionals. When the instance they reference is deallocated, they are set to `nil`. 

On the other hand, unowned variables must never be set to nil once they are initialised, and so you do not need to worry about unwrapping.

The most common place you will see `unowned` is within closures, they are used to declare `[unowned self]` - meaning "I want to reference `self` inside this closure, but do not want to own it".

Why would we use `unowned` rather than `weak` here? Because if `self` is `nil` within a closure, we must have done something wrong!

Nevertheless, `unowned` must be used carefully, because it is not set to `nil` when the referenced instance is deallocated, and can result in a fatal error if the unowned reference is acessed.

```swift
final class myClass {
    // function with a completion closure
    viewModel.someFunction(for: arguments) { [unowned self] in
        self.doSomething()
    }
}
```

### Final

Best practice is to mark all classes and structs 'final' to prevent direct modification:

```swift
final class Animal {
    private var name: String
    private var noise: String
    
    // methods 
}
```

### Private / file-private / internal / public

Swift provides five different access levels for entities within your code:

1. Private (most restrictive):  restricts the use of an entity to the enclosing declaration, and to extensions of that declaration that are in the same file
2. File-private: restricts the use of an entity to its own defining source file
3. Internal (default): enables entities to be used within any source file from their defining module, but not in any source file outside of that module. Typtically used when defining an app’s or a framework’s internal structure
4. Public: enable entities to be used within any source file from their defining module, and also in a source file from another module that imports the defining module
5. Open (least restrictive): Same as public above, but applies only to classes and class members, and it differs from public access by allowing code outside the module to subclass and override. Marking a class as open explicitly indicates that you’ve considered the impact of code from other modules using that class as a superclass, and that you’ve designed your class’s code accordingly.

Properties will be internal by default if not marked specifically. They should rarely (if ever) be made public.

ARQ practice is to mark as 'private' to prevent direct modification:

```swift    
final class Animal {

    private var name = "Bobo"
    private var noise = "roowr"

    func makeNoise(){
        print("\(self.name) says \(self.noise)")
    }
}
```

E.g. Using the above:

```swift
var dog = Animal()
dog.name = "Sebastian"
dog.noise = "woof woof"

dog.makeNoise()
```

would generate an error. 

If you removed (or commented out) the lines "dog.name = "Sebastian"" and "dog.noise = "woof woof"", the application would print "Bobo says roowr." 

However, if you instead removed 'private' from the Animal class name and noise properties and left those two lines, they  would modify the Animal name and noise properties. If the function 'makeNoise' was invoked on the class instance after this, it would now print "Sebastian says woof woof."

## Property Observers

Being able to observe changes in various values is essential for many different kinds of programming styles and techniques, whether you use delegates, functions, or reactive programming. 

Swift comes with a way to observe any kind of non-lazy, stored property:

- `willSet`
- `didSet`

which helps you easily run a block of code either before or after a property was assigned.

In the below example, `willSet` and `didSet` are used to notify the delegate anytime the 'tool' variable is changed:

```swift
class ToolboxViewController: UIViewController {
    weak var delegate: ToolboxViewControllerDelegate?

    var tool = Tool.boxedSelection {
        willSet {
            // Property observers are called even if the new
            // value is the same as the old one, so we need to
            // perform a simple equality check here.
            guard tool != newValue else {
                return
            }

            delegate?.toolboxViewController(self,
                willChangeToolTo: newValue
            )
        }

        didSet {
            guard tool != oldValue else {
                return
            }

            delegate?.toolboxViewController(self,
                didChangeToolFrom: oldValue
            )
        }
    }
}
```

This means that regardless of where and how we change `tool`, the delegate will always be notified correctly and the ViewController’s API can be kept simple.

Note: While Swift’s built-in property observers only enable us to observe synchronous property changes, they can also be used to build asynchronous abstractions [such as Futures & Promises](https://www.swiftbysundell.com/articles/under-the-hood-of-futures-and-promises-in-swift/).

## References and Resources:
 - https://docs.swift.org/swift-book/LanguageGuide/Properties.html
 - https://www.avanderlee.com/swift/computed-property/
 - https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html
 - https://www.donnywals.com/effectively-using-static-and-class-methods-and-properties/
 - https://www.hackingwithswift.com/example-code/language/what-does-unowned-mean
 - https://krakendev.io/blog/weak-and-unowned-references-in-swift
 - https://www.swiftbysundell.com/articles/property-observers-in-swift/
 - https://www.swiftbysundell.com/articles/under-the-hood-of-futures-and-promises-in-swift/

