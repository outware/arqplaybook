# iOS Design Patterns

## Table of Contents
- [What is a design pattern?](#markdown-header-what-is-a-design-pattern)
- [Common design patterns - MVC versus MVVM](#markdown-header-common-design-patterns-mvc-versus-mvvm)
- [Model View Controller (MVC)](#markdown-header-model-view-controller-mvc)
- [Model View ViewModel (MVVM)](#markdown-header-model-view-viewmodel-mvvm)
- [Protocols and delegates in MVVM](#markdown-header-protocols-and-delegates-in-mvvm)
- [Closures](#markdown-header-closures)
- [Combine](#markdown-header-combine)
- [References and Resources](#markdown-header-references-and-resources)

## What is a design pattern?

A design patten is a software architectural pattern -  a template or roadmap for organising a codebase. This can help to make the development process faster and the code more robust, readable and maintainable. 

Design patterns are general, reusable best practice approaches for solving commonly occurring problems for particular applications or systems. While not mandatory, one or more is likely to be adhered to in a well-maintained codebase.

## Common design patterns - MVC versus MVVM

Swift and Objective-C are both object-oriented languages. Object-oriented design patterns typically show relationships and interactions between classes or objects. 

In iOS development, the Model View Controller (MVC) and Model View ViewModel (MVVM) design patterns are both popular.

Each component is written as a class or object. 

### Model View Controller (MVC)

![MVC Design Pattern](https://www.freecodecamp.org/news/content/images/2021/04/MVC3.png)

MVC is used extensively in iOS and is the default design pattern. It contains the following components:

- Model: contains the business logic. It is responsible for managing the app state. This may include reading and writing data, networking and data validation.
- View: handles the user interaction with the UI (for example, pressing a button) and is also responsible for presenting data to the user via the UI. The View should not "understand" the data (i.e. Model) being presented.
- Controller ("ViewController" in iOS): the View and Model interact through one or more Controller. 

The Controller should contain the logic for both interacting with the Model based on events in the View, as well as instructions to the View to update the UI. For example, in a simple online shop, the Model might contain a class setting out the type of product and its properties, and methods for how the product can be counted or removed from a digital "warehouse". The View would contain UI elements showing the results of interactions with the data model, for example, if a product of the Model's product type was added to a shopping cart icon.

The Controller knows about both the Model and the View, and owns views and models it interacts with. MVC can therefore cause tight coupling (this is when components are overly reliant on each other, so that you cannot update one without making changes in the other), which undermines or excludes re-use. 

It can also be hard to test the Controllers, which often end up housing the code that doesn't fit neatly into either the Model or the View.

### Model View ViewModel (MVVM)

![MVVM Design Pattern](https://cdn.evbuc.com/eventlogos/170379860/mvvmpattern.png)

MVVM has become more popular in iOS in recent years and is used by the ARQ iOS team. 

The MVVM pattern has a Model, a View (in practice the "ViewController" plus storyboard or XIB file) and a ViewModel. 

As such it really has four components:

- Model: data that the app operates on.
- View: UI visual elements (i.e. represented in Xcode in the storyboard or xib)
- Controller ("ViewController"): In iOS, this is part of the 'View', but it is the functionality written in Swift/Objective-C and not the storyboard or XIB.
- ViewModel: updates the Model from View inputs and updates Views from Model outputs.

A StackOverflow user has summarised current observed MVVM implementations as follows:

1. MVVM that uses Delegation / Protocols to update the ViewController / View
2. MVVM with Combine using the Publisher/ Subscriber model to receive and react to value changes over time.
    - Note: Replaced [Key-Value Observing](https://developer.apple.com/documentation/swift/cocoa_design_patterns/using_key-value_observing_in_swift) (KVO), which relied on third party frameworks like [ReactiveCocoa](https://medium.com/@felicity.johnson.mail/an-intro-to-reactivecocoa-26c49eba06f9). Combine is Apple's own framework with Swift API. It also has a built-in Publisher for any KVO-compliant property.
3. MVVM as a simple helper / wrapper class for the model (no protocols, no observers etc)

The ViewModel is an abstraction of the view exposing public properties and commands. It may have a subscriber or observer data binding (as in 2. above) that automates interaction between the View and bound properties in the ViewModel. 

Here it can be thought of like a cog operating between Model and View/ViewController. Events in the View should trigger behaviour in the ViewModel that not only interacts with the Model, but causes behaviour in the ViewModel that the View can immediately use to update the UI. This is more efficient, and more closely adheres to separation of concerns. 

Another advantage of MVVM is that it is easier to test than MVC. This is because the ViewModel can be tested on business logic alone, without worrying about view implementations which are in the (View)Controller.

## Protocols and delegates in MVVM

### Delegate pattern 

Another way of implementing MVVM is using [delegate](./Delegates.md) protocols to update the ViewController/View (as in 1. above). This is done by including a weak reference (i.e. via a variable) to a protocol containing the desired behaviour in the ViewController (for example, functions to send and/or receive events and data). The weak reference prevents strong coupling.

Instead of using an existing protocol, the custom protocol is created by the programmer. It should include functions and properties to be used to implement the desired behaviour. Make it available using 'AnyObject':

```swift
protocol MyProtocol: AnyObject {
    myFunction()
}
```

The desired behaviour is implemented in the relevant component - for example, a ViewModel or other Model - which must conform to the protocol in order to do so:

```swift
class ViewModel: MyProtocol {
    // content of class
    
    myFunction(){
    // does something
    }
}
```

Make the delegate available elsewhere by creating the weak reference, and accessing the functions and/or properties: 

```swift
class ViewController {
    var myProtocol: MyProtocol?
    
    myProtocol?.myFunction() // does the thing
}
```


### Closures 

[Closures](./Closures.md) are essentially functions that can be passed into another function as a parameter. 

They can be used inside it to access that function’s scope, and this can be used to promote separation of concerns. 

For example, closures can be used to capture Model self instances produced within the ViewController without the Model knowing what those instances are. 

There is some debate about the relative merits of using closures compared to delegates, though in practice they often complement each other and appear together in the same project.

Closures take one of three forms:

- Global functions: closures that have a name and don’t capture any values.
- Nested functions: closures that have a name and can capture values from their enclosing function.
- Closure expressions: unnamed closures written in a lightweight syntax that can capture values from their surrounding context.

Closures in Swift are similar to "blocks" in C and Objective-C, and to "lambdas" in other programming languages.

###  Combine

[Apple documentation on combine](https://developer.apple.com/documentation/combine)

[Combine](./Combine.md) is an Apple framework that provides a declarative Swift API for processing events, or changes in values over time. 

It uses a `Publisher` /  `Subscriber` model, where the Publishers broadcast values over time and Subscribers receive and activate updates (e.g. to the UI) based on those changes.

It can be used to implement MVVM in a reactive way instead of using delegates or closures, similar to third-party frameworks supporting Key-Value Observing behaviour (e.g. CocoaReact) which it has largely replaced. 

## References and Resources:
 
- https://github.com/tailec/ios-architecture/
- https://www.hackingwithswift.com/example-code/language/what-is-mvvm
- https://cocoacasts.com/swift-and-model-view-viewmodel-in-practice
- https://www.raywenderlich.com/6733535-ios-mvvm-tutorial-refactoring-from-mvc
- https://developer.apple.com/documentation/swift/cocoa_design_patterns/using_key-value_observing_in_swift
- https://medium.com/@felicity.johnson.mail/an-intro-to-reactivecocoa-26c49eba06f9
- https://www.codementor.io/@nishadhshrestha/mvvm-in-swift-4-using-delegates-ikdflt1cb
- https://stackoverflow.com/questions/41916819/ios-mvvm-is-delegation-pattern-a-requirement
- https://pavlepesic.medium.com/how-to-implement-delegation-pattern-using-mvvm-and-flow-coordinators-d1f6c3fcbe6
- https://docs.swift.org/swift-book/LanguageGuide/Closures.html
- https://www.freecodecamp.org/ (for MVC image)
- https://www.developer-experts.net/en/ (for MVVM image)
