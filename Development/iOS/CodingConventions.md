# General Coding Principles

## Table of Contents

- [Introduction](#markdown-header-introduction)
- [Style Guide and Conventions](#markdown-header-style-guide-and-conventions)
- [Swift Coding Conventions and Style Guidelines](#markdown-swift-swift-coding-conventions-and-style-guidelines)
- [General](#markdown-header-general)
- [Naming](#markdown-header-naming)
- [Classes, Structs and Protocols](#markdown-header-classes-structs-and-protocols)
- [Spacing](#markdown-header-spacing)
- [Properties/Variables](#markdown-header-properties-variables)
- [Functions and Closures](#markdown-header-functions-and-closures)
- [Pre-Processor Macros](#markdown-header-pre-processor-macros)
- [Access Control](#markdown-header-access-control)
- [Optionals](#markdown-header-optionals)
- [Control Flow](#markdown-header-control-flow)
- [Clarity](#markdown-header-clarity)
- [Enumerations](#markdown-header-enumerations)
- [Memory Management](#markdown-header-memory-management)
- [References and Resources](#markdown-header-references-and-resources)

## Introduction

* There should be NO warnings
    * Solve warnings before they accumulate into a long list. 
    * If there is a warning, it should be very visible, and addressed straight away.
* Do not leave TODO notes in the code
    * TODO's should be completed or removed. 
    * If there is an idea for a future improvement, label it explicitly with FutureWork. If it is really long, it shouldn't be in code, but in our project management system (such as Pivotal Tracker, as a chore).
* Sensible variable names
    * No extra long or 'joke' variable names that should be changed or removed. 
    * Always use an appropriate name, and improve when possible.
* Dead code
    * If code is not used, then remove it (it will still be available in version control).
    * This can include code that is:
        * commented out 
        * removed via macros (e.g. #if 0) (it is not visibly dead code, but it is dead nonetheless)
* Keep folders on disk organised (as well as in the project)
    * e.g. libraries in a Libs folder etc.
* Version control
    * Make sure your commit doesn't break other functionality.
    * Write clear and concise comments.
    * Commit log should start with wireframe and story number.
    * Every commit has a code review, with initials of code reviewer.
    * Don't commit unused code and resources.

* Adhere to coding style guidelines, below

## Style Guide and Conventions

* Code readability over brevity
    * Complicated one line compound statements can be clever, but are hard to debug and maintain.
    * Meaningful descriptive naming and commenting.
* Code organisation
    * Keep code neat
        * indentation
        * formatting
        * alignment
        * superfluous line breaks
    * Keep your functions as small as possible, with a discrete purposes.
* Variable and function names
    * Give clear, short, descriptive names
* Conditional Statements
    * Always use braces for `if`, even if it is a one line block.
    * If you have a compound/complicated condition statement, then break out to descriptively named variables and use the variables in the condition statement except where the efficiency payoff is not worth it. 
        * i.e. computing all the conditions beforehand is computationally expensive, and otherwise may not occur because the condition could have been resolved with the first component such as when it is TRUE, and it is a compound OR
    * If there are more than 2 components, use line breaks between the components
    * Use indentation to align components logically
* Align columns
* If there are multiple subsequent lines that contain a feature such as equality operator, indent so that the equals line up.
* All code and projects should be well documented. Adhere to the Arq Documentation Conventions


# Swift Coding Conventions and Style Guidelines

It's always good to start with the [general language standards and practices](https://docs.swift.org/swift-book/LanguageGuide/TheBasics.html).

## General

* No semicolons unless required.
* Declare each variable (immutable and mutable) or function on a separate line.
* Use native Swift types where possible (`String` over `NSString`).
* When creating `struct` objects use Swift's `struct` initializers instead of the legacy `CGGeometry` factory functions.

## Naming

* Follow the naming conventions in the [Swift API Design Guidelines](https://swift.org/documentation/api-design-guidelines/#naming).

### Classes, Structs and Protocols

* Use Pascal case (start with a capital letter) for `class`, `struct` and `protocol` names.
* Don't use a prefix for `class`, `struct` or `protocol` names.
    * Unlike Objective-C, Swift has modules, which also means it has namespaces (but only at the module level).
    * Swift can distinguish between a `class`, `struct` and `enum` by module name.
    
## Spacing

* Indent using 2 spaces.
    * It preserves line spacing and prevents line wrapping in the case when a long statement is unavoidable.
    * Arq has converged on 2 spaces across platforms as our standard.
    * ...so you may as well stick with it so our code is consistent. You don't want to be the person that causes this:

        ``` swift
        var y = 3
        if user.isHappy {
          y = y + 2
        } else {
            y = y + 4
          print(y)
        }
        ```
    
* Method braces and other braces (if/else/switch/while etc.) always open on the same line as the statement but close on a new line.

    ``` swift
    if user.isHappy {
      // Do something
    } else {
      // Do something else
    }
    ```

* There should be exactly one blank line between methods.
   
    ``` swift
    func hello() {
    }
    
    func goodbye() {
    }
    ```

## Properties/Variables

* Use immutable variables (`let`) unless mutability is required.
    * This prevents unintentional access/manipulation by external entities. 
    * It allows the compiler to perform additional optimisations.
        * ...yes, you could argue that compiler is already intelligent enough, but let's help it and, most of all, ourselves by making things explicit.
* Iterator variable names should be descriptive (not `i` or `x`).
* Don't add superfluous suffixes to variable names (i.e. `name` not `nameString`).
* Swift does not require `self` to refer to instance variables.
    * Only use `self` where it: 
        * Is required, such as referencing `self` in a closure. 
        * **or** improves readability such as an in initialiser.

    ``` swift
    // Initialiser
    func init(number: Int, word: String) {
      self.number = number
      self.word = word
    }    
    
    // Closure
    func doSomething(completion: () -> Void) {
    }
    
    func doSomethingAwesome() {
      doSomething { [unowned self] in
        self.updateUI()
      }
    }
    ```

*  Use type inference wherever possible unless explicit types improve readability.

    Do:
    
    ``` swift
    let string = "I am a string"
    
    // Type needed for optionality
    let string: String? = "I am a string"
    ```
    
    ***Don't***:
    
    ``` swift
    // Type is superflous
    let string: String = "I am a string"
    ```

## Functions and Closures

* Use trailing closure syntax wherever possible:

    ``` swift
    func doSomething(string: String, completion: () -> Void) {
    }
    
    func doSomethingAwesome() {
      doSomething("a string") {
      ...
      }
    }
    ```

* Use an implicit `return` if there is only one line in a closure:

    ``` swift
    fooArray.map { $0 + 1 }
    ```

* Use named parameters for longer closures:

    ``` swift
    fooArray.map { foo in
      let bar = foo.toBar()
      bar = bar.doSomething()
      return bar
    }
    ```

## Pre-processor Macros

* Swift does not support macros, instead use global functions to define common behaviour.
* Use `// MARK: <Label>` instead of `#pragma mark`.
* Use `let` constants instead of `#define` constants.

## Access Control

* Follow the [Swift standards for Access Control](https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html).
* Private methods should always be marked as `private`.

    ``` swift
    private func doSomething() {
    }
    ```

* Read-only variables should use a private setter:

    ``` swift
    private(set) var numberOfRows = 3
    ```

* Internal helper classes should be `fileprivate` so they can be used within the source file while not being exposed to the rest of the module.

* All (well, most) Swift entities [default](https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html#ID7) to an `internal` access level, which means they can be only accessed within their module.'

* `open` and `public` should not normally be used. They are primarily needed only when creating a framework or library.

## Optionals

* Avoid force unwrapping optionals with `!` (this includes implicitly unwrapped optionals).
* The `nil` case should be handled with: 
    * [Optional chaining](https://docs.swift.org/swift-book/LanguageGuide/OptionalChaining.html)
    * `nil` coalescing
    * `if let` or `guard let` syntax
* Note: An exception to this rule is any property that is set after initialisation but is **required** to be non-nil for the type to function (e.g. IBOutlets, injected dependencies).
    * In these cases force unwrapping should fail **only** during development. Such as forgetting to connect an outlet in a `.xib` file.

Do:
    
``` swift
@IBOutlet weak var bigRedButton: UIButton!
    
let string: String?
    
// Optional chaining
let count = string?.characters.count
// Nil Coalescing
let defaultValue = string ?? "Sensible Default Value"
    
// if let
var count: Int
if let string = string {
  count = string.characters.count
} else {
  // Handle error
}
    
// guard let
func doSomething(toString: String?) -> String {
  guard let toString = toString else {
    return
  }
    
  // Rest of function
}
```

***Don't:***
    
``` swift
let string: String?
    
let count = string!.characters.count // This will crash if the string is nil!
```

## Control flow 

* Use a `guard` statement to transfer control out of scope if one or more conditions are not met.  
    * `guard` statements without `return`, `break` or `continue` produces a compile-time error, so exit is guaranteed.
    * `guard` statements make it clear where we are testing for conditions and requirements versus performing logic of some kind. 
* Use `defer` blocks to cleanup when execution leaves the current scope.
* Use a `return` statement only at the end of a method or closure.

***Do:***
    
``` swift
func resizeImage(url: NSURL) -> UIImage? {
  //...
  let dataSize: Int = ...
  let destData = UnsafeMutablePointer<UInt8>.alloc(dataSize)
  defer {
    destData.dealloc(dataSize)
  }
    
  var destBuffer = vImage_Buffer(data: destData, ...)
    
  // scale the image from sourceBuffer to destBuffer
  var error = vImageScale_ARGB8888(&sourceBuffer, &destBuffer, ...)
  guard error == kvImageNoError else { 
    return nil 
  }
  guard let destCGImage = vImageCreateCGImageFromBuffer(&destBuffer, &format, ...) else { 
    return nil 
  }
  //...
  return destCGImage
}
```
    
***Don't:***
    
``` swift
func resizeImage(url: NSURL) -> UIImage? {
  // ...
  let dataSize: Int = ...
  let destData = UnsafeMutablePointer<UInt8>.alloc(dataSize)
  var destBuffer = vImage_Buffer(data: destData, ...)
    
  // scale the image from sourceBuffer to destBuffer
  var error = vImageScale_ARGB8888(&sourceBuffer, &destBuffer, ...)
  if error != kvImageNoError {
    destData.dealloc(dataSize)  
    return nil
  }
    
  let destCGImage = vImageCreateCGImageFromBuffer(&destBuffer, &format, ...) 
  if destCGImage == nil {
    destData.dealloc(dataSize)  
    return nil
  }
  destData.dealloc(dataSize) 
  // ...
  return destCGImage
}
```

## Clarity

* Group semantically similar lines and use whitespace to separate semantically different lines 
    * e.g. Group properties (IBOutlets, primitive types/flags, related functionality).
* Always add an empty line after the last variable/property.
* Group semantically similar methods under a `// MARK: - <Label>` this also applies to protocol methods if they are not defined in an extension.

    ``` swift
    // MARK: - View Lifecycle (for a UIViewController)
    // MARK: - NSNotifications Handling
    // MARK: - UITextFieldDelegate
    ```
    
* Methods that are no longer maintained should be removed.
    * If absolutely necessary, document with `@deprecated`, and remove when no longer referenced by any projects.
    * Unused methods make the code harder to understand and confusing, as they add extra noise to the code base.

## Enumerations

* Use Swift `enum` for enumerations declared as `NS_ENUM` in Objective-C.
* Use `struct <OPTION_NAME>:RawOptionSetType` for options declared as `NS_OPTION` in Objective-C (e.g. `UIUserNotificationType`).

## Memory Management

* Read the official Swift documentation on [Automatic Reference Counting (ARC)](https://docs.swift.org/swift-book/LanguageGuide/AutomaticReferenceCounting.html) and [Memory Management](https://docs.swift.org/swift-book/LanguageGuide/MemorySafety.html).
* If you refer to `self` in a block, use a `weak` or `unowned` reference to avoid retain cycles.    
* Use a `weak` reference for variables that need not be retained, which could be `nil`.
* Use `unowned` for variables that do not need to be retained, and which must not be `nil`.
* Delegate properties should be `weak` references.
* Outlets should be `weak` references in most cases.

``` swift
class MyViewController: UIViewController {
    
  @IBOutlet weak var headerView: UIView!
    
  weak private var delegate: DoSomethingDelegate?
  unowned var someElseOwnsThis: String
    
  // MARK: - ViewController Lifecycle
  
  func viewDidLoad() {
    super.viewDidLoad()
  }
    
  // MARK : - doSomething Functions
  
  func doSomething(string: String, completion: () -> Void) {
  }
    
  func doSomethingAwesome() {
    doSomething("a string") { [weak self] in
      guard let weakSelf = self else {
        return
      }
      ...      
      delegate?.didSomethingAwesome()
    }
  }
}
```

## References and Resources

- [swift-language-standards](https://docs.swift.org/swift-book/LanguageGuide/TheBasics.html)
- [swift-api-design-guidelines](https://swift.org/documentation/api-design-guidelines/#naming)
- [swift-access-control](https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html)
- [swift-access-control-default](https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html#ID7)
- [swift-optional-chaining](https://docs.swift.org/swift-book/LanguageGuide/OptionalChaining.html)
- [swift-arc](https://docs.swift.org/swift-book/LanguageGuide/AutomaticReferenceCounting.html)
- [swift-memory-safety](https://docs.swift.org/swift-book/LanguageGuide/MemorySafety.html)
