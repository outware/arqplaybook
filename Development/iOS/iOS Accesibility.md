# iOS Accessibility

[Apple Developer Documentation](https://developer.apple.com/documentation/objectivec/nsobject/uiaccessibility)

## Table of Contents
 - [Introduction](#markdown-header-introduction)
 - [Levels of Conformance](#markdown-header-levels-of-conformance)
 - [Configuring Accessible Elements](#markdown-header-configuring-accessible-elements)
 - [UIAccessibility Programming Interface](#markdown-header-uiaccessibility-programming-interface)
 - [Making User Interface Elements Accessible](#markdown-header-making-user-interface-elements-accessible)
 - [Testing Accessibility](#markdown-header-testing-accessibility)
 - [References and Resources](#markdown-header-references-and-resources)
 


## Introduction
“Mobile accessibility” refers to making websites and applications more accessible to people with disabilities when they are using mobile phones and other devices. 

It also can give users more control in the UI of their device so that it operates more inline with their preferences e.g Dark mode

For simplicity, there are four accessibility domains to keep in mind when thinking about your users:

- Vision 
    - A person may be blind or color blind, or have a vision challenge that makes focusing difficult.

- Hearing 
    - A person may be deaf, have partial hearing loss, or have difficulty hearing sounds within a certain range.

- Mobility 
    - A person with reduced mobility may have difficulty holding a device or tapping the interface.

- Cognitive 
    - A person may have difficulty remembering a sequence of steps, or they may find an overly complex user interface difficult to process and manage.


Some of the ways that we can help to make applications more accessible to users are: 

- Large Text Size
    -   Allowing for if users increase the text size, how should the text be displayed so that it is still readable. How should the user interface alter to accomodate the larger text.

- VoiceOver
    - Will read out all text on your application, as well as indicating buttons, textfields or anything else the user may need to interact with. VoiceOver text may differ from the visual text. e.g FAQ should be read by VoiceOver as "Frequently Asked Questions"

- Display Customisation
    - iOS offers a range of features to customize the display, including Bold Text, High Contrast Cursors, Reduce Transparency, Dark Mode, and Reduce Motion. Use UIAccessibility APIs to detect when these settings are enabled so that your app behaves correctly.
    
## Levels of Conformance
When implementing accessiblity the project may conform to the User Agent Accessibility Guidelines 2.0 at one of three conformance levels: 
   
- Level A conformance:
    - The user agent complies with all applicable level A success criteria.
    - Can block some people with disabilities from getting information or accomplishing a task, and/or are relatively easy for developers to implement or are common in the existing user agents.
    - A developer must satisfy this checkpoint. Otherwise, one or more groups will find it impossible to access information. Satisfying this checkpoint is a basic requirement for some groups to be able to use mobile applications.

- Level AA conformance:
    - The user agent complies with all applicable level A and AA success criteria
    - Can cause difficulty for some people with disabilities in getting information or accomplishing a task (including tasks causing excessive fatigue), and/or may be more difficult for developers to implement.
    - A developer should satisfy this checkpoint. Otherwise, one or more groups will find it difficult to access information in the document. Satisfying this checkpoint will remove significant barriers to accessing mobile applications.

- Level AAA conformance:
    - The user agent complies with all applicable level A, AA, and AAA success criteria.
    - Improve accessibility or reduces fatigue for some people with disabilities, and/or, may be very difficult for developers to implement.
    - A developer may address this checkpoint. Otherwise, one or more groups will find it somewhat difficult to access information in the document. Satisfying this checkpoint will improve access to mobile applications.

## Configuring Accessible Elements
An accessible view can be configured by setting one or more of the below attributes. 

VoiceOver reads out a combination of the below attributes. Usually in the form of _<accessibilityLabel><accessibilityTraits><accessibilityHint>_.

- **accessibilityLabel**
    - A short, localized word or phrase that succinctly describes the control or view, but does not identify the element's type. Examples are "Add" or "Play".
- **accessibilityTraits**
    - A combination of one or more individual traits, each of which describes a single aspect of an element's state, behavior, or usage. For example, an element that behaves like a keyboard key and that is currently selected can be characterised by the combination of the Keyboard Key and Selected traits.
- **accessibilityHint**
    - A brief, localized phrase that describes the results of an action on an element. Examples are "Adds a title" or "Opens the shopping list".
    - Note that an element that does not perform an action doesn’t need to provide a hint.
- **accessibilityFrame**
    - The frame of the element in screen coordinates, which is given by the CGRect structure that specifies an element's screen location and size.
- **accessibilityValue**
    - The current value of an element, when the value is not represented by the label. For example, the label for a slider might be "Speed" but its current value might be "50%".

Notes: 

- Many of the standard preset attributes have very logical defaults derived from UIView, UIControl etc. So likely will not need to be altered

- Standard UIKit controls and views are automatically accessible. However, you should also make sure that their attributes make sense e.g. appropriate hints and labels.


## UIAccessibility Programming Interface
The UI Accessibility programming interface consists of two informal protocols, one class, a function, and a handful of constants:

- **UIAccessibility informal protocol**
    - Objects that implement the UIAccessibility protocol report their accessibility status (that is, whether they are accessible) and supply descriptive information about themselves. Standard UIKit controls and views implement the UIAccessibility protocol by default.
- **UIAccessibilityContainer informal protocol**
    - This protocol allows a subclass of UIView to make some or all of the objects it contains accessible as separate elements. This is particularly useful when the objects contained in such a view are not themselves subclasses of UIView and, for this reason, are not automatically accessible.
- **UIAccessibilityElement class**
    - This class defines an object that can be returned through the UIAccessibilityContainer protocol. You can create an instance of UIAccessibilityElement to represent an item that isn't automatically accessible, such as an object that does not inherit from UIView, or an object that does not exist.


## Making User Interface Elements Accessible
Every user interface element with which users can interact with, is accessible. This includes elements that merely supply information, such as static text, as well as controls that perform actions. All accessible elements supply accurate and helpful information.

From the perspective of Accessibility, custom view is either an individual view or a container view. An individual view does not contain any other views that need to be accessible. A container view, on the other hand, contains other elements with which users can interact. For example, a view which does custom drawing of shapes would be considered a container view, as those shapes do not inherit from UIView themselves.
    
### Container View
If your application displays a custom view that contains other elements with which users interact, you need to make the contained elements separately accessible. At the same time, you need to make sure that the container view itself is not accessible. The reason is that users interact with the contents of the container, not with the container itself.
To accomplish this, your custom container view should implement the UIAccessibilityContainer protocol. This protocol defines methods that make the contained elements available in an array.

## Testing Accessibility

Once you have implemented some accessibility elements, it is wise to test and see them functioning properly within the application. 

- XCode Accessibility Tool
    - Allows you to hover over each element to see their accesibility elements and how they might be read out with Voice Over

- Running on a Test Device
    - By turning on Voice Over on a test device you can see how a user would navigate around the device, and how each element is read out and interacted with.
    
    - *Highly recommend* that you set up Voice Over to be turned on and off via a shortcut button. This allows you to navigate quickly to the page you are testing and then turn on Voice Over.
    
    - Note that testing Voice Over is best done with a physical test device rather than a simulator.

## References and Resources
 - [Human Interface Guidelines](https://developer.apple.com/design/human-interface-guidelines/accessibility/overview/introduction/)
 - [Accesibility Programming Guide for iOS](https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/iPhoneAccessibility/Introduction/Introduction.html)
