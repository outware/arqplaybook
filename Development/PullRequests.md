# Pull Requests
_***This is merely an overview of creating Pull Requests check with your project manager/tech lead for department specific requirements***_

## Table of Contents
- [Introduction](#markdown-header-introduction)
- [Creating a Pull Request](#markdown-header-creating-a-pull-request)
- [Creating Pull Request Comments](#markdown-header-creating-pull-request-comments)
- [Example Pull Request Format](#markdown-header-example-pull-request-proposal-format)
- [Reviewing PRs](#markdown-header-reviewing-prs)


## Introduction

Pull requests, which are often referred to as "PRs" are a step in the development process we use to allow others to check our work.
A pull request is created on the branch you are working on when you are finished working on your code and ready to merge that branch into the main branch (often called develop).
Once you have created the request the people you have added to [review](#markdown-header-reviewing-prs) it, may write comments on your PR, to point out errors or give suggestions on alternatives that might be more concise for example.
At ARQ Group, we suggest a minimum of three people are added as reviewers, and once your PR is reviewed, perhaps updated and finally approved, it is able to be merged.


## Creating a Pull Request

When creating a PR, there is a few things to make sure of.

- Make sure your branch builds with no errors

- Make sure all your branch tests successfully complete.

- Check that you have fulfilled all the requirements of the task assigned to you

- Check you have only changed the things you needed to, remove any `print` statements or unnecessary comments

- Ensure the [comment](#markdown-header-creating-pull-request-comments) of the pull request is accurate

- Add your 3 reviewers

- Ensure your branch is pointing at develop (or equivalent "main branch")

- Check the box to delete branch once merged


## Creating Pull Request Comments

A Pull Request Comments section should consist of a few things;

- [PR Title](#markdown-header-pr-title)

- [Overview](#markdown-header-overview)

- [How to Test](#markdown-header-how-to-test)

- Notes (if needed)

#### PR Title

This should be an audience-focused statement of the PR goal.
Follow this process to work out your goal:

- Determine your own goal for the PR. eg. To minimise the time taken to resolve open PRs.
- Flip your goal to an audience focused goal. eg. A faster approach to getting through our PR review backlog.

#### Overview

- Add a link to the card that describes the initial issue, allow your reviewers to read it for themselves
- Provide and overview of what this issue was and how you have solved it
- The description field must contain the following detail:

A brief summary of the PR.

_What's the benefit to our clients/community/users_

If it affects our external consumers what is the benefit to them?


_What's the benefit to our business_

How does this benefit our business. Greater effectiveness, culture improvement etc.


_What's the benefit to our team_

If applicable how does this improve our team?


#### How to Test

- What did your branch change and how can we find that change
- Give step by step instructions to make it easy for your reviewers to find and prove that your code does what it is supposed to.
- Does your reviewer need to log in? Make sure to give them any credentials they may need
- Describe the scenario where your code occurs and what it's expected results are.

#### _Example Pull Request proposal format_

**PR title**

[NCG-2645]-Fixes display error when navigating from user details page

**PR Overview**

- Original request [NGC-2645] ( link to actual request )
- When navigating back from user details, displayed initial welcome page instead of returning to homepage
- Updated so now returns correctly to homepage

**How to Test**

Scenario 1

- log in as user

      - Username: User1

      - Password: Password

- navigate to user details page
- once on user details page press the back button and observe the app returning smoothly to the homepage


### Reviewing PRs

We practice code reviews as a form of feedback, typically this occurs through the use of BitBucket (or similar) Pull Request process.  It's an opportunity to receive solicited feedback and for the reviewer to learn about the changes to the system.  A side-affect of code reviews is that it means more than one person is aware of any new code being added.

Here's the gist for conducting a code review:

1. Branch off and run the app (on device if possible)
1. Follow listed testing steps and make sure they all line up with the requirements
1. Run all unit tests to ensure they are passing
1. Look at the code, see if it makes sense or perhaps could be improved
1. Add comments to the PRs if needed (you can attach these comments right next to the relevant code)
1. If it looks all good to you, then approve the branch for merging.

When your branch has completed code review it can move on to [QA Testing](./QATesting)


## Definition of Done

Coding complete? Not quite yet.

Before creating a pull request, make sure you check these things:

1. **READ:** Read the story thoroughly including any comments on the story card.
 Make sure you did not miss out on any user scenarios
 
2. **TEST:** Test the functionality of your story
    1. make sure it covers the full story description
    2. make sure unit tests are not failing
    
3. **VISUAL:** Show the completed screen/feature to a visual designer/ux designer **on device** to get clarification of the completed design.

4. **PERFORMANCE:** Check the performance of the story
    1. is the scrolling smooth (on the minimum supported phone)
    2. does the UI block when performing some tasks
    3. is the transition smooth (animation "from" and "to" your story related screen)
    4. is the loading and dismissal of the keyboard smooth and fast?
    
5. **VALIDATION:** If the story has input fields, make sure you validate the input
    1. Test the length of the accepted input value
    2. Test using invalid input values:
        1. invalid characters or emoji in text field
        2. letters where number is expected
        3. invalid email, etc.
        
6. **ERROR HANDLING:** If the story is presenting data from network calls, test the error scenarios
    1. Disable network connection
    2. What is the screen layout when there is no data
    3. Does the app display network error messages
    4. How is the app handling invalid response from server (stub the response to invalid json or change the json structure)
    5. Is the app handling fields that are null? App must not crash on receiving nulls in the response.
    6. How does the app behave in poor network conditions? Use network link conditioner on iOS to simulate poor network.
    
7. **INTERRUPTIONS:** How is your app behaving on external interruptions (other app pushed on top,
    and putting your app in background)
    1. Incoming call and resuming after call is done
    2. Push in background using home button and resuming afterwards
    3. Interacting with system notification and resuming afterwards
    4. **NOTE:** Android should use "Don't keep Activities" flag in order to simulate low memory scenario
    
8. **LOADING STATES:** How does the app behave in situations where a loading state is displayed?
    1. When loading is complete, does the loading spinner / progress indicator dismiss smoothly?
    2. Can loading be interrupted? Does the app behave as expected in this situation?
    3. What happens when the app is pushed to background while loading?
    4. If there is a loading error is it handled gracefully and as expected by design?

It takes time, and it might feel like a pain, but if QA are catching the easy bugs it means they're not catching the REAL bugs. As developers it's our job to ensure that we push the highest quality work through for testing. By sending it to QA we're saying proudly "This is the best work I can produce. I would be happy for a customer to use this code, and submit reviews based on their experience".
