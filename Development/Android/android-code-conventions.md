# Arq Android Coding Conventions and Style Guidelines

## Preface

External projects you go onto will likely have slightly different best practices and conventions. Nonetheless, this is a good starting point and will be largely relevant.

## Kotlin

We use [Android's Kotlin style guide](https://developer.android.com/kotlin/style-guide) as a baseline. Where possible, follow utilise Kotlin specific syntax to make code more concise, e.g. use expression body like 
```fun someFunction(): Boolean = true``` instead of ```fun someFunction(): Boolean {  return true }```

Additional rules are described below:

* Class naming
	* Class names extending from Android classes use suffixes describing the type of component being extended:
		* Application: ends with _Application_ (e.g. `MyApplication` or `MyApp`)
		* Activities: ends with _Activity_ (e.g. `MyActivity`)
		* Fragments: ends with _Fragment_ (e.g. `MyFragment`)
		* Views: ends with _View_ (e.g. `MyDetailsView`)
        * ViewModels: ends with _ViewModel_ (e.g. `MyDetailsViewModel`)
* Method naming
	* Getters that return boolean value can start with _is_ (e.g. `isEnabled()`). Depending on context, it may make more sense to start with _has_ (e.g. `hasCredit()`). This can be determined on a case-by-case basis.

## Java

We follow [AOSP's Java coding conventions](http://source.android.com/source/code-style.html) as a baseline. If something is not covered there, we fall back to the official [Sun/Oracle conventions](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).

## Code Formatting

In order to have a consistent coding style across a project's codebase, we strongly encourage the use of Android Studio coding style schemes. It is considered good practice to commit the style scheme in the project's repository so it can evolve with the project.

You can use [Android's scheme](https://android.googlesource.com/platform/development.git/+/master/ide/intellij/codestyles/AndroidStyle.xml) as a baseline and update it as necessary.

Some handy hotkeys for formatting code in Android Studio (For mac) include:
* Control + Alt + O (Optimise imports - removes unused imports)
* Command + Alt + L (Reformat code - removes whitespace)

## App Resources

We use prefixes and suffixes to organise resources in a sensible fashion. These should typically be paired with a name that is helpful in understanding what they are linked to, for example *btn_login*. 

### IDs

Here is a list of common prefixes we use for ID resources:

* Button: _btn\__
* Layouts (Linear, Relative): _layout\__
* ImageView: _img\__
* TextView: _txt\__
* EditText: _edit\__
* Spinner: _spinner\__
* ProgressBar: _progress\__
* Switch: _switch\__
* CheckBox: _check\__
* RadioButton: _radio\__
* ViewPager: _pager\__
* ScrollView: _scroll\__

### Layouts

Here is a list of common prefixes we use for layout resources:

* Activity layouts: _activity\__
* Fragment layouts: _fragment\__
* List item: _item\__ or _item\_header\__ or _item\_footer\__
* Menu: _menu\__
* Dialogs: _dialog\__
* Custom views: _view\__
* Other widgets: _layout\__

### Drawables

Here is a list of common prefixes and suffixes we use for drawable resources:

* Background: _bg\__
* Button: _btn\__
* Image: _img\__
* Icon: _ic\__
* Text: _txt\__

	Note: an icon differs from an image in that it will typically be a small, single colour image, typically used in an action bar, drawer, or things of that ilk.

* Drawable Selector
	* Selector: _\<prefix>\_\<name>.xml_
	* Selected/Pressed: _\<prefix>\_\<name>\_sel_
	* Selected/Focused: _\<prefix>\_\<name>\_focus_
	* UnSelected: _\<prefix>\_\<name>\_usel_

	E.g. `btn_save_usel.xml`, `btn_save_sel.xml`, `btn_save.xml`, etc.

### Styles and Themes

Even though they both use the `style` xml tag, styles and themes should be separated into `styles.xml` and `themes.xml` files respectively.

A theme is a style that is applied to an entire screen or multiple screens and generally extends from the AppCompat themes, e.g. `Theme.AppCompat.Light`.

A style applies to a specific element or widget
Styles should be named using the format _\<project\_prefix\>.\<name\_of\_parent (without the parent's prefix)\>.\<optional: key\_feature(s)\_of\_new_style>_, e.g. `<style name="MyApp.TextAppearance.Title.Condensed" parent="TextAppearance.AppCompat.Title">`.

You should use `<!-- Comments -->` to group similar items.

### Strings

Here is a list of rules we follow to manage string resources:

* All UI strings must be in one or more XML resource file(s) (by convention `strings.xml`)
* Java/Kotlin source files must not contain any hardcoded UI strings
* Strings should be named with a prefix specifying the context where they are going to be used
_\<context\_prefix>\_\<string\_identifier>_, e.g. `<string name="login_name">Name</string>`, `<string name="login_name_hint">Put your name here</string>`, etc.
* You should use `<!-- Comments -->` to group similar items

### Colours

Here is a list of rules we follow to manage colour resources:

* All UI colours should be in one or more resource files (by convention `colors.xml`)
* Selector colour should be in the `res/color` folder
* App should use a colour palette; contextual colours should then reference a palette colour
* Palette colours should be named in a human readable, non-contextual way, e.g. `ocean_blue or light_blue`
* Colours should be named with a prefix specifying the context where they are going to be used
_\<context\_prefix>\_\<string\_identifier>_, e.g. `apptheme_primary`, `actionbar_title_text`, etc.
* You should use `<!-- Comments -->` to group similar items

### Dimensions

Here is a list of rules we follow to manage dimension resources:

* All UI dimensions should be in one or more resource files (by convention `dimens.xml`)
* Dimensions unique to your view should be used, global units like `raw.unit_4` should be avoided as they are essentially hardcoding values.
* Dimensions should be named with a prefix specifying the context where they are going to be used
_\<context\_prefix>\_\<string\_identifier>_, e.g. `<dimen name="login_name">Name</string>`, etc.
* You should use `<!-- Comments -->` to group similar items