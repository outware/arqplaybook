
# Testing Accessibility

# Android Accessibility Scanner

When you've finished implementing accessibility on your app you can use the [Accessibility Scanner](https://play.google.com/store/apps/details?id=com.google.android.apps.accessibility.auditor&hl=en) provided by Google to verify if there are some areas of improvement you should focus on. The Accessibility Scanner analyses the screens of your app to identify potential accessibility issues. Just install the app, follow the onboarding tutorial to enable the scanner, launch the app you want to test and start the scanner.


# Using ADB to control Accessbility settings for easier testing with Android Emulators + Real Devices

It's a lot easier to test accessibility using ADB. Credit for these commands goes to [this](https://gist.github.com/mrk-han/67a98616e43f86f8482c5ee6dd3faabe) repo.

## Talkback

**disable**

`adb shell settings put secure enabled_accessibility_services com.android.talkback/com.google.android.marvin.talkback.TalkBackService`

**enable**

`adb shell settings put secure enabled_accessibility_services com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService`

## Font Scale (Font Size -- Testing Dynamic Text)

`adb shell settings put system font_scale 0.85` (small)

`adb shell settings put system font_scale 1.0` (default)

`adb shell settings put system font_scale 1.15` (large)

`adb shell settings put system font_scale 1.30` (largest)

`adb shell settings put system font_scale 2.0` (sizes like this can only be achieved with custom adb setting, not from ui)

## Color

**Properties:**
```
accessibility_display_daltonizer_enabled=1
accessibility_display_daltonizer=11
accessibility_display_daltonizer=12
accessibility_display_daltonizer=13
```

**ADB Commands:**

`adb shell settings put secure accessibility_display_daltonizer_enabled 1`

`adb shell settings put secure accessibility_display_daltonizer 0`

`adb shell settings put secure accessibility_display_daltonizer 11`

`adb shell settings put secure accessibility_display_daltonizer 12`

`adb shell settings put secure accessibility_display_daltonizer 13`

```
0 == Monochromatic
11 is for Deuteranomaly (red-green)
12 is for Protanomaly (red-green)
13 is for Tritanomaly (blue-yellow)
```

## Color Inversion

**Property:** `accessibility_display_inversion_enabled=1`

**ADB Command:** `adb shell settings put secure accessibility_display_inversion_enabled 1`

## High Contrast Text

**Property:** `high_text_contrast_enabled=1`

**ADB Command:** `adb shell settings put secure high_text_contrast_enabled 1`

## List Existing Properties

```
adb shell settings list system
adb shell settings list global
adb shell settings list secure
```

Also, `adb shell getprop` is a great way to expose a lot of other properties you can change with the `adb shell setprop` functionality


## Show Touches

`adb shell settings put system show_touches 1`

## Pointer Location

`adb shell settings put system pointer_location 1`