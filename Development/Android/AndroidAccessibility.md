# Android Accessibility

This document is a collection of best practices and suggestions to support accessibility while developing on Android.
For an introduction to accessibility on Android refer to [Developing Android Applications](https://developer.android.com/training/accessibility/accessible-app.html) on the Android Developers website. This talk on [Mobile accessibility](https://www.youtube.com/watch?v=CmGlYMjvMjc) by Arq's Simon Smiley-Andrews is also a great introduction to the topic more broadly.

## TalkBack

TalkBack is the default screen reader on Android devices, it can be enabled from Systems Settings > Accessibility > TalkBack. As a default the screen reading volume matches the device media volume. TalkBack is available as part of the Android Accessibility Suite on the [Google PlayStore](https://play.google.com/store/apps/details?id=com.google.android.marvin.talkback&hl=en), and may need to be installed if using an emulator.

TalkBack reads the content of the screen to the user and allows them to interact with it using gestures. Each element on the screen should have a label, and the user can explore the screen in two ways:

* Dragging their finger on the screen, as soon as the finger is sitting on an item with an attached label the screen reader reads it.

* Swiping right, the user can move element-to-element from the top left corner of the screen to the bottom right one. Swiping left allows the user to move in the opposite direction.

## Fragmentation

Fragmentation is one of the major pain-points of the Android ecosystem, when working on accessibility this problem is even more apparent. We're all aware of how different OS versions and different vendor customizations result in different behaviors, as an additional variable different TalkBack versions influence the outcome as well. Vendors can also provide customised Text-To-Speech engines that replace the default TalkBack TTS engine.

Just to make a few examples to consider:

* On some devices back navigation requires a double tap on the back button, on some others it doesn't.

* Some devices will read "WA" as "Western Australia", some others will read it as "W A".

* On some devices a floating action button is read in order considering its positioning on the screen, on some others it's considered to be at the very end of the screen.

## TalkBack Settings

One of the first things to do if you're working on accessibility is to explore the TalkBack settings.

A useful one is "Display speech output" under "Developer settings". This setting displays on screen all the text read from the TTS engine. You can turn it on and mute the media volume to view the text-to-speech on screen.

## Content Description

While the system does a good job of creating labels for most UI elements like TextViews and Buttons, there's not much it can do to automatically generate a label for an image. That's why the contentDescription attribute should always be provided as a best practice, and should describe the image well enough to convey to a non sighted user the same message that the image is conveying to a sighted user.

## importantForAccessibility

You may have some elements in your UI that are not adding any value to a non sighted user. Starting API 16 you can mark any View as not [important for accessibility](https://developer.android.com/reference/kotlin/android/view/View#setimportantforaccessibility), TalkBack will then completely ignore that View.

One thing to be mindful of when using the importantForAccessibility attribute (or method) is that this View is going to completely ignore any accessibility related events. This is evident on ListViews for example, where setting the ListView as not important for accessibility will prevent it from scrolling when the user reaches the end of the page by right-swiping.

## Dynamic UI Changes

Something to consider when working for accessibility is that all the dynamic UI changes like inline error messages, loading indicators or content refreshing are not useful to a non sighted user, and should be accompanied by voice announcements.

Announcements are triggered simply by calling [announceForAccessibility](https://developer.android.com/reference/kotlin/android/view/View#announceforaccessibility) on a View object.
