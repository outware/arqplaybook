# Getting started with Android

# Computer setup
Download and install the current version of [Android Studio](https://developer.android.com/studio/index.html)

# Getting started with Android development

Get access to bitbucket, and head to our [Android training repo](https://bitbucket.org/outware/arq-android-training/src/main/). This suggests using the Udacity video courses, however, if you prefer written resources, have a look at the courses [here](https://developer.android.com/courses). This project is written in Kotlin which is our preferred language for new projects. If you would like to brush up on Kotlin before starting, have a look [here](https://classroom.udacity.com/courses/ud9011).

# Android Resources
* [Android Developer Docs](https://developer.android.com/docs)
* [Android Developer Guides](https://developer.android.com/guide) - a "friendlier" version of the docs. Great for gaining in depth understanding for a concept.
* [Codelabs](https://codelabs.developers.google.com/?cat=android) - Guided tutorials, walking through small applications to help learn a new topic.
* [Kotlin style guide](https://developer.android.com/kotlin/style-guide)
* Kotlin Koans: To learn more about Kotlin you may like to complete the [Kotlin Koans online course](https://kotlinlang.org/docs/tutorials/koans.html).
* Clean Architecture:
    * [Uncle Bob - Clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
    * [Clean architecture with Android](https://medium.com/@dmilicic/a-detailed-guide-on-developing-android-apps-using-the-clean-architecture-pattern-d38d71e94029#.sowet1llu)
    * [Architecting Android the clean way](https://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)
    * https://medium.com/androiddevelopers/viewmodels-and-livedata-patterns-antipatterns-21efaef74a54
    * https://github.com/android/architecture-samples - Provides example code with different architectural approaches to solving the same problem.
* RX Java:
    * [RX java and Retrofit with Android](https://medium.com/@biratkirat/8-rxjava-rxandroid-in-kotlin-e599509753c8)
    * [RX Java Roadmap](https://medium.com/free-code-camp/a-complete-roadmap-for-learning-rxjava-9316ee6aeda7)

* Data binding:
    * https://proandroiddev.com/android-data-binding-under-the-hood-part-1-33b8c7adfb7c
    * https://proandroiddev.com/android-data-binding-under-the-hood-part-2-fdcbb0f54700

# Points to keep in mind when adding to documentation:

The documentation is not meant to replace official language docs, however, it may build on them or provide a different approach to explaining their usage.
In particular, it should be targeted to developers being onboarded into Arq, but may also act as a useful point of reference for more experienced devs.

# Suggested Topics to add (In no particular order):
* Null safety in Kotlin
* Coroutines
* Activity lifecycle
* MVVM
* Data Binding
* Livedata
* Clean Architecture
* Code style and best practices
* Testing
* Android studio tips and tricks
* ADB
* Anything else you might feel passionate about!