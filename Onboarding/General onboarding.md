# Onboarding #

Before we start the onboarding process let me be the first to welcome you to ARQ. We have a thriving team of talented individuals and it is fantastic to have you join our team. 

Below is a series of steps to help you navigate the first few weeks in the company. Specific documentation can be found for you, based on your speciality, inside the onboarding folder in the playbook, but for now, let's focus on getting you up and running. 

### Priorities in your first week

In your first week we want you to get comfortable with your new working setup, the digital team and who you might be working with on your upcoming projects. For this reason we try our best to never have people starting on a project within their first 3 days. This is not always possible so I am sorry if this is happening to you. 

The priorites for your first week are:

- Getting your laptop setup and access to all your necessary ARQ accounts
    - Email (Office365)
    - Bitbucket (shared coding repos)
    - ELMO (People and Culture portal)
    - ARForce (Timesheets and project allocation)
    - Your preferred IDE setup
       
